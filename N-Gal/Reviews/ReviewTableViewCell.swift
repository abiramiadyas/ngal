//
//  ReviewTableViewCell.swift
//  Exlcart
//
//  Created by Tech Basics on 29/12/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var reviewCommentLbl: UILabel!

    @IBOutlet var dateLbl: UILabel!
    @IBOutlet weak var reviewStarView: TPFloatRatingView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
