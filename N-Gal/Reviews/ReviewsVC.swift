//
//  ReviewsVC.swift
//  Exlcart
//
//  Created by Tech Basics on 28/12/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class ReviewsVC: ParentViewController,loginIntimation {

    var reviewsAry:NSArray = []
    var productName:String = ""
    var productID:String = ""

    @IBOutlet weak var reviewsTbl: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet var reviewTextView:UITextView!
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var viewReviewTxtBg: UIView!
    @IBOutlet var textviewReview: UITextView!
    @IBOutlet var floatRatingView: TPFloatRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Reviews"
        titleLbl.text = productName as String
        
        btnCancel.layer.borderColor = UIColor.darkGray.cgColor
        btnPost.layer.borderColor = UIColor.darkGray.cgColor
        self.reviewsTbl.tableFooterView = UIView()
        
        self.viewReviewTxtBg.layer.cornerRadius = 5.0;
        self.viewReviewTxtBg.layer.borderColor = UIColor.lightGray.cgColor
        self.viewReviewTxtBg.layer.borderWidth = 1.0;
        
        self.floatRatingView.editable = true

        self.floatRatingView.emptySelectedImage = UIImage(named: "str-gry") as UIImage!
        self.floatRatingView.fullSelectedImage = UIImage(named: "str-ylo") as UIImage!
        
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.halfRatings = true
        self.floatRatingView.floatRatings = false
        self.floatRatingView.rating = 0.0
        
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.textviewReview.resignFirstResponder();
    }
    @IBAction func buttonSubmitPressed(_ sender: AnyObject) {
        
        
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            self.postReview()
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
                //
            })
        }
    }
    
    @IBAction func buttonCancelPressed(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loginSuccess() {
        self.postReview()
    }
    
    
    func loginFailure() {
        //
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  reviewsAry.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:ReviewTableViewCell = self.reviewsTbl.dequeueReusableCell(withIdentifier: "cell") as! ReviewTableViewCell
         cell.titleLbl.text = (reviewsAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name")! as? String
        
        cell.nameLbl.text = "by " + ((reviewsAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "author")! as! String)
        cell.reviewCommentLbl.text = (reviewsAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "text")! as? String

        
        let font = UIFont.systemFont(ofSize: 16.0)
        cell.reviewCommentLbl.font = font
        cell.reviewCommentLbl.numberOfLines = 0;
       // let text = cell.reviewCommentLbl.text! as NSString
       // let size = text.sizeWithAttributes([NSFontAttributeName:font])
        let height = ReviewsVC.heightForView(cell.reviewCommentLbl.text!, font: font, width: cell.reviewCommentLbl.frame.width)
        
        cell.reviewCommentLbl.frame = CGRect(x: cell.reviewCommentLbl.frame.origin.x, y: cell.reviewCommentLbl.frame.origin.y, width: cell.reviewCommentLbl.frame.size.width, height: height)

        cell.reviewStarView.editable = false
        
        let ratingStr:String = ((reviewsAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "rating") as? String)!
        
        let ratingNo = NumberFormatter().number(from: ratingStr)
        let ratingFloat = CGFloat(ratingNo!)
        cell.reviewStarView.rating = ratingFloat
        
        
     //   let image = UIImage(named: "heartsmallGrey") as UIImage!

        
        cell.reviewStarView.emptySelectedImage = UIImage(named: "str-gry") as UIImage!
        cell.reviewStarView.fullSelectedImage = UIImage(named: "str-ylo") as UIImage!

        cell.reviewStarView.contentMode = UIViewContentMode.scaleAspectFit
        cell.reviewStarView.maxRating = 5
        cell.reviewStarView.minRating = 1
        cell.reviewStarView.halfRatings = false
        cell.reviewStarView.floatRatings = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       // let d = NSDate()
        
        let dateStr = (reviewsAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "date_added") as? String
        
        let s = dateFormatter.date(from: dateStr!)
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        cell.dateLbl.text = dateFormatter.string(from: s!)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
       
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       // print("didsele", terminator: "")
    }

    class func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        let commentStr = (reviewsAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "text")! as? String
        
        
        let font = UIFont.systemFont(ofSize: 16.0)
      //  let size = commentStr!.sizeWithAttributes([NSFontAttributeName:font])
        let height = ReviewsVC.heightForView(commentStr!, font: font, width: 304)

        
        return max(60, 60 + height)
    }

    
    
    func postReview()
    {
        
        self.textviewReview.resignFirstResponder();
        
        if(textviewReview.hasText)
        {
            
            
            let values:String = NSString(format: "product_id=%@&customer_id=%@&review=%@&rating=%f",productID,userIDStr,textviewReview.text,self.floatRatingView.rating) as String
           
            SharedManager.showHUD(viewController: self)
            
            BusinessManager.postReview(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                
                self.textviewReview.text = "";
                self.floatRatingView.rating = 0.0
             
                
                if code == "200"
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKey: "Message") as! String, viewController: self)
                }
            });
            
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Information", alertMessage: kReviewAlertMessage, viewController: self)
        }
        
        
    }
    

}
