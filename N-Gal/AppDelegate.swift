   //
//  AppDelegate.swift
//  N-Gal
//
//  Created by Adyas on 07/02/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit
import UserNotifications
import OneSignal
import IQKeyboardManagerSwift

var userIDStr:String = ""
var addressIDStr:String = ""
let kUserDetails:String = "_k_USER_DETAILS"
var deviceTokenString:String = ""
var badgeCount = 0
var themeColor:UIColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate//, UNUserNotificationCenterDelegate
{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UITabBar.appearance().tintColor = themeColor
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: true)
        UINavigationBar.appearance().barTintColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black]
        UINavigationBar.appearance().tintColor = UIColor.black
        
        Thread.sleep(forTimeInterval: 1)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().toolbarTintColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload? = result?.notification.payload
            
            print("Message = \(payload!.body)")
            print("badge number = \(payload?.badge)")
            print("notification sound = \(payload?.sound)")
            
            if let additionalData = result!.notification.payload!.additionalData {
                print("additionalData = \(additionalData)")
                
                // DEEP LINK and open url in RedViewController
                // Send notification with Additional Data > example key: "OpenURL" example value: "https://google.com"
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let instantiateRedViewController : HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainCategory") as! HomeViewController
                //  instantiateRedViewController.receivedURL = additionalData["OpenURL"] as! String!
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = instantiateRedViewController
                self.window?.makeKeyAndVisible()
                
                
                if let actionSelected = payload?.actionButtons {
                    print("actionSelected = \(actionSelected)")
                }
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "fc566da0-616e-4e57-8161-5c3d0cbaf1df",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        // 04fea69e-391b-4510-9334-9800c7f03ad0
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension Data
{
    func hexString() -> String
    {
        return self.reduce("") { string, byte in
            string + String(format: "%02X", byte)
        }
    }
}

