//
//  SignUpViewController.swift
//  E-Commerce
//
//  Created by Apple on 24/06/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

import UIKit

protocol loginIntimation {
    func loginSuccess()
    func loginFailure()
}

class SignUpViewController: ParentViewController, UIPickerViewDelegate, UIPickerViewDataSource
{
    var delegate:loginIntimation?
    //Login
    @IBOutlet var userIDTxt: UITextField!
    @IBOutlet var passwordLgTxt: UITextField!
    @IBOutlet var remainMeBtn: UIButton!
    @IBOutlet weak var forgotPassView: UIView!
    @IBOutlet weak var forgotPassEmailTxt: UITextField!
    
    @IBOutlet var viewLogin: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var phoneNumberTxt: UITextField!
    @IBOutlet var address1Txt: UITextField!
    @IBOutlet var address2Txt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var pincodeTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet var confirmPwdTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var newsLetterBtn: UIButton!
    
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var picker: UIPickerView!
    var selectedIndex = Int()
    var pickerType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPicker.isHidden = true
        //Login
        forgotPassView.isHidden = true
        viewLogin.isHidden = false
        
        self.mainScrollView.autoresizesSubviews = true
        self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 730)
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.refreshList(notification:)), name: NSNotification.Name(rawValue: "getName"), object: nil)
        
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    
    @IBAction func remaindMe(_ sender: AnyObject)
    {
        if remainMeBtn.isSelected == true
        {
            remainMeBtn.isSelected = false
        }
        else
        {
            remainMeBtn.isSelected = true
        }
    }
    
    @IBAction func forgotPassword(_ sender: AnyObject)
    {
        forgotPassView.isHidden = false
        forgotPassView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func forgotPassDoneAction(_ sender: AnyObject)
    {
        if self.isValidEmail(forgotPassEmailTxt.text!)
        {
            self.view.endEditing(true)
                SharedManager.showHUD(viewController: self)
            BusinessManager.forgotPasswod(forgotPassEmailTxt.text!, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    let alert = UIAlertController(title: "Information", message: result.value(forKeyPath: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        self.forgotPassView.isHidden = true
                        self.forgotPassEmailTxt.text = ""
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title:"Information", alertMessage: kEmailValidAlertMessage, viewController: self)
        }
    }
    
    @IBAction func forgotPassCancelAction(_ sender: AnyObject) {
        forgotPassView.isHidden = true
        self.view.endEditing(true)
        forgotPassEmailTxt.text = ""
    }
    
    @IBAction func closeButtonAction(_ sender: AnyObject)
    {
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.dismiss(animated: false, completion: nil)
            self.delegate?.loginFailure()
        })

    }
    
    func login()
    {
        self.view.endEditing(true)
        SharedManager.showHUD(viewController: self)
        BusinessManager.loginWith(userIDTxt.text!, password: passwordLgTxt.text!, deviceToken: deviceTokenString) { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            let code = result.value(forKey: "status") as! String
            
            if code == "200"
            {
                userIDStr = result.value(forKeyPath: "customer.customer_id") as! String
                addressIDStr = result.value(forKeyPath: "customer.address_id") as! String
                UserDefaults.standard.set(addressIDStr, forKey: "ADDRESS_ID")
                let data = NSKeyedArchiver.archivedData(withRootObject: result)
                UserDefaults.standard.set(data, forKey: kUserDetails)
                
                self.dismiss(animated: true, completion: { () -> Void in
                    self.delegate?.loginSuccess()
                });
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
        }
    }
    
    @IBAction func signIn(_ sender: AnyObject)
    {
        login()
    }
    
    @IBAction func signUp(_ sender: AnyObject) {
        
        viewLogin.isHidden = true
    }
    
    
    @IBAction func buttonBackPressed(_ sender: AnyObject) {
        
        viewLogin.isHidden = false
    }
    
    @IBAction func newsLetterAction(_ sender: AnyObject)
    {
        if newsLetterBtn.isSelected
        {
           newsLetterBtn.isSelected = false
        }
        else
        {
            newsLetterBtn.isSelected = true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        mainScrollView.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y-85), animated: true)

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        mainScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == firstNameTxt
        {
            firstNameTxt.resignFirstResponder()
            lastNameTxt.becomeFirstResponder()
        }
        else if textField == lastNameTxt
        {
            lastNameTxt.resignFirstResponder()
            phoneNumberTxt.becomeFirstResponder()
        }
        else if textField == phoneNumberTxt
        {
            phoneNumberTxt.resignFirstResponder()
            address1Txt.becomeFirstResponder()
        }
        else if textField == address1Txt
        {
            address1Txt.resignFirstResponder()
            cityTxt.becomeFirstResponder()
        }
        else if textField == cityTxt
        {
            cityTxt.resignFirstResponder()
            pincodeTxt.becomeFirstResponder()
        }
        else if textField == pincodeTxt
        {
            pincodeTxt.resignFirstResponder()
        }
        else if textField == countryTxt
        {
            stateTxt.becomeFirstResponder()
        }
        else if textField == stateTxt
        {
            stateTxt.resignFirstResponder()
            passwordTxt.becomeFirstResponder()
        }
        else if textField == passwordTxt
        {
            passwordTxt.resignFirstResponder()
            emailTxt.becomeFirstResponder()
        }
        else if textField == emailTxt
        {
            emailTxt.resignFirstResponder()
            phoneNumberTxt.becomeFirstResponder()
        }
        else if textField == phoneNumberTxt
        {
            phoneNumberTxt.resignFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true
    }


    @IBAction func signUpAction(_ sender: AnyObject)
    {
        self.view.endEditing(true)
        
        if !firstNameTxt.text!.isEmpty && !lastNameTxt.text!.isEmpty && !phoneNumberTxt.text!.isEmpty && !address1Txt.text!.isEmpty && !cityTxt.text!.isEmpty && !pincodeTxt.text!.isEmpty && !countryTxt.text!.isEmpty && !stateTxt.text!.isEmpty && !passwordTxt.text!.isEmpty && !emailTxt.text!.isEmpty  && checkBoxBtn.isSelected && passwordTxt.text == confirmPwdTxt.text
        {
            let values:NSString = NSString(format: "firstname=%@&lastname=%@&phone=%@&address=%@&address_2=%@&city=%@&pincode=%@&country=%@&state=%@&password=%@&email=%@&newsletter=%@&device_type=2&device_id=%@",firstNameTxt.text!,lastNameTxt.text!,phoneNumberTxt.text!,address1Txt.text!,address2Txt.text!,cityTxt.text!,pincodeTxt.text!,countryID,stateID,passwordTxt.text!,emailTxt.text!,"1",deviceTokenString)
            
                SharedManager.showHUD(viewController: self)
            BusinessManager.registerUserWithValues(values as String, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    let alert = UIAlertController(title: "Thanks", message: result.value(forKeyPath: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        self.userIDTxt.text = self.emailTxt.text!
                        self.passwordLgTxt.text = self.passwordTxt.text!
                        
                        self.login()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else if passwordTxt.text != confirmPwdTxt.text
        {
            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kPasswordMismatchAlertMessage, viewController: self)
        }
        else
        {
            if firstNameTxt.text!.isEmpty
            {
                SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter your First Name", viewController: self)
            }
            else
            {
                if lastNameTxt.text!.isEmpty
                {
                    SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter your Last Name", viewController: self)
                }
                else
                {
                    if emailTxt.text!.isEmpty
                    {
                        SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter your Email Address", viewController: self)
                    }
                    else
                    {
                        if phoneNumberTxt.text!.isEmpty
                        {
                            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter your Mobile Number", viewController: self)
                        }
                        else
                        {
                            if address1Txt.text!.isEmpty
                            {
                                SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter Address", viewController: self)
                            }
                            else
                            {
                                if cityTxt.text!.isEmpty
                                {
                                    SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter your City", viewController: self)
                                }
                                else
                                {
                                    if pincodeTxt.text!.isEmpty
                                    {
                                        SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter Pincode", viewController: self)
                                    }
                                    else
                                    {
                                        if countryTxt.text!.isEmpty
                                        {
                                            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Select Country", viewController: self)
                                        }
                                        else
                                        {
                                            if stateTxt.text!.isEmpty
                                            {
                                                SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Select State", viewController: self)
                                            }
                                            else
                                            {
                                                if passwordTxt.text!.isEmpty
                                                {
                                                    SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Enter Password", viewController: self)
                                                }
                                                else
                                                {
                                                    if confirmPwdTxt.text!.isEmpty
                                                    {
                                                        SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Please Re-enter your Password", viewController: self)
                                                    }
                                                    else
                                                    {
                                                        if !checkBoxBtn.isSelected
                                                        {
                                                            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "You must Accept our Terms & Conditions", viewController: self)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func checkBoxAction(_ sender: AnyObject)
    {
        if checkBoxBtn.isSelected
        {
            checkBoxBtn.isSelected = false
        }
        else
        {
            checkBoxBtn.isSelected = true
        }
    }
    
    @IBAction func clickViewPolicy(_ sender: AnyObject)
    {
        let url = URL(string: "https://www.n-gal.com/privacy-policy")!
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OtherCategoriesViewController") as! OtherCategoriesViewController
        viewController.urlString = "\(url)"
      //  self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "OtherCategoriesViewController") as! OtherCategoriesViewController
//        viewController.urlString = "\(url)"
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func countryAction(_ sender: AnyObject)
    {
        stateName = ""
        self.view.endEditing(true)
        
        var countryNames = [String]()
        let country_id = NSMutableArray()
        
        for i in 0 ..< self.countryListAry.count
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: i)
            countryNames.append(name as! String)
            country_id.add((self.countryListAry.value(forKey: "country_id") as! NSArray).object(at: i) as! String)
        }
        pickerType = "country"
        
        self.viewPicker.isHidden = false
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    @IBAction func stateAction(_ sender: AnyObject)
    {
        // self.showStateList()
        self.view.endEditing(true)
        
        for i : Int in 0 ..< self.countryListAry.count
        {
            let id:String = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "country_id") as? String)!
            
            if countryID.isEqual(to: id as String)
            {
                self.stateAryList = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "state")! as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        pickerType = "state"
        
        self.viewPicker.isHidden = false
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    func refreshList(notification: NSNotification)
    {
        self.countryTxt.text = (countryName as NSString) as String
        self.stateTxt.text = (stateName as NSString) as String
    }
    
    //MARK: PickerView Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerType == "country"
        {
            return countryListAry.count
        }
        else
        {
            return stateAryList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerType == "country"
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        else
        {
            let name = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedIndex = row
    }
    
    
    @IBAction func clickPickerCancel(_ sender: Any)
    {
        self.viewPicker.isHidden = true
    }
    
    @IBAction func clickPickerDone(_ sender: Any)
    {
        if pickerType == "country"
        {
            countryTxt.text = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            countryName = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            countryID = (self.countryListAry.value(forKey: "country_id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        else
        {
            stateTxt.text = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            stateName = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            stateID = (self.stateAryList.value(forKey: "state_id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        
        self.viewPicker.isHidden = true
    }
}
