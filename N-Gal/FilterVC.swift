//
//  FilterVC.swift
//  Exlcart
//
//  Created by Adyas on 31/12/16.
//  Copyright © 2016 adyas. All rights reserved.
//

import UIKit

class FilterVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var filterTableView: UITableView!
    @IBOutlet var filterTableView1: UITableView!
    @IBOutlet var btnReset: UIButton!
    @IBOutlet var btnFilter: UIButton!
    
    var filterAry:NSMutableArray = []
    var filterID:String = String()
    var filterValues:NSArray = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnReset.layer.borderColor = UIColor.darkGray.cgColor
        btnFilter.layer.borderColor = UIColor.darkGray.cgColor
        filterTableView.tableFooterView = UIView()
        filterTableView1.tableFooterView = UIView()
        self.navigationController?.isNavigationBarHidden = true
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.filterTableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
        self.tableView(self.filterTableView, didSelectRowAt: indexPath)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == self.filterTableView
        {
            return self.filterAry.count
        }
        else
        {
            return filterValues.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.filterTableView
        {
            let cell = filterTableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as UITableViewCell
            cell.textLabel?.text = (filterAry.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            
            return cell
        }
        else
        {
            let cell = filterTableView1.dequeueReusableCell(withIdentifier: "FilterCell1", for: indexPath) as UITableViewCell
            cell.textLabel?.text = (filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
            cell.accessoryType = .checkmark
            if filterID.contains((filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "filter_id") as! String)
            {
                cell.tintColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)
            }
            else
            {
                cell.tintColor = .lightGray
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.filterTableView
        {
            filterValues = (filterAry.object(at: indexPath.row) as AnyObject).value(forKey: "filter") as! NSArray
            self.filterTableView1.reloadData()
        }
        else
        {
            let value = (filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "filter_id") as! String
            let str = value +  (",")
            filterID.append(str)
            
            let cell = tableView.cellForRow(at: indexPath)
            
            if cell!.isSelected == true
            {
                cell!.tintColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        if tableView == self.filterTableView1
        {
            let value = (filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "filter_id") as! String
            
            filterID = filterID.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: value + (","), with: "") as String
            
            let cell = tableView.cellForRow(at: indexPath)
            
            if cell!.isSelected == false
            {
                cell!.tintColor = .lightGray
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func resetFilter(_ sender: Any)
    {
        filterID.removeAll()
        self.filterTableView.reloadData()
        self.filterTableView1.reloadData()
    }
    
    @IBAction func applyFilter(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getFilterData"), object: filterID)
    }
}

