//
//  ResultSuccessViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 11/10/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

@objc class ResultSuccessViewController: ParentViewController {

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var lblStatus: UILabel!
    var transStatus : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        transStatus = UserDefaults.standard.string(forKey: "TRANS_STATUS")!
        
        if transStatus == "Transaction Successful"
        {
            self.lblStatus.text = "YOUR ORDER SUCCESSFULY PLACED"
            self.txtMessage.isHidden = false
            self.btnContinue.titleLabel?.text = "CONTINUE SHOPPING"
        }
        else
        {
            self.lblStatus.text = "STATUS: \(transStatus)"
            self.lblStatus.text = transStatus
            self.txtMessage.isHidden = true
            self.btnContinue.titleLabel?.text = "RETRY CHECKOUT"
        }
        
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.blue, for: .normal)
        myBackButton.sizeToFit()
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        // Do any additional setup after loading the view.
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        if transStatus == "Transaction Successful"
        {
            UserDefaults.standard.removeObject(forKey: kAddToCart)
            UserDefaults.standard.removeObject(forKey: "COUPON")
            UserDefaults.standard.removeObject(forKey: "VOUCHER")
            UserDefaults.standard.removeObject(forKey: "REWARDS")
            
            paymentDict = [:]
            shippingDict = [:]
            shippingResultDict = [:]
            productTempDict = [:]
            productTempArry.removeAllObjects()
            orderID = ""
            self.gotoRootViewController()
            self.updateCartBadge()
        }
        else
        {
            self.tabBarController?.selectedIndex = 3
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func clickContinue(_ sender: Any)
    {
        if transStatus == "Transaction Successful"
        {
            UserDefaults.standard.removeObject(forKey: kAddToCart)
            UserDefaults.standard.removeObject(forKey: "COUPON")
            UserDefaults.standard.removeObject(forKey: "VOUCHER")
            UserDefaults.standard.removeObject(forKey: "REWARDS")
            
            paymentDict = [:]
            shippingDict = [:]
            shippingResultDict = [:]
            productTempDict = [:]
            productTempArry.removeAllObjects()
            orderID = ""
            self.gotoRootViewController()
            self.updateCartBadge()
        }
        else
        {
            self.tabBarController?.selectedIndex = 3
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
