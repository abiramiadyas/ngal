//
//  ProductListVC.swift
//  Exlcart
//
//  Created by iPhone on 14/07/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

let reuseIdentifier = "collCell"

class ProductListVC: ParentViewController,UICollectionViewDataSource,UICollectionViewDelegate,loginIntimation//, UICollectionViewDelegateFlowLayout
{

    var sortItems = ["Name (A - Z)", "Name (Z - A)","Price (Low > High)","Price (High > Low)", "Model (A - Z)", "Model (Z - A)"]
   
    @IBOutlet weak var productListCollectionView: UICollectionView!
    @IBOutlet var buttonsView: UIView!
    
    var productListAry:NSMutableArray = []
    var filterAry:NSMutableArray = []
    var addFavTag:Int = 0
    var catTitle:String = String()
    var filterID:String = ""
    var page:Int = 1
    var pageCount = Double()
    var sortPage:Int = 1
    var sortPageCount = Double()
    var limit:String = "6"
    var productsCount:NSString = ""
    var sortProductsCount:NSString = ""
    
    var isSorted : Bool = false
    var isFromHome : Bool = false
    var sortKey = ""
    var orderKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.title = catTitle
        self.updateCartBadge()
        
        //Navigation Bar
        let barView = UIView()
        barView.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        
        let img1 = UIImageView()
        img1.image = UIImage (named: "right-arrow.png")
        img1.frame = CGRect(x: 0, y: 10, width: 15, height: 20)
        img1.contentMode = .scaleAspectFit
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.black, for: .normal)
        myBackButton.sizeToFit()
        myBackButton.frame = CGRect(x: 12, y: 5, width: 55, height: 30)
        
        barView.addSubview(img1)
        barView.addSubview(myBackButton)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: barView)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        self.tabBarController?.tabBar.isHidden = false
        
        if productListAry.count == 0
        {
            buttonsView.isHidden = false
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.getProductList(categoryID, languageID: languageID as String, page:1, filters:filterID, limit:limit, sort: sortKey, order: orderKey, completionHandler: { (result) -> () in
                
                SharedManager.dismissHUD(viewController: self)
                
                if let resultArr = result.value(forKey: "products")
                {
                    
                    for i in 0 ..< (resultArr as AnyObject).count
                    {
                        let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                        
                        if ((nameSTr)?.lowercased().contains("series"))!
                        {
                            
                        }
                        else
                        {
                            self.productListAry.add((resultArr as AnyObject).object(at: i))
                        }
                    }
                    
                    
                    
                   // self.productListAry = resultArr as! NSMutableArray
                    
                    if self.productListAry.count == 0
                    {
                        SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyProductAlertMessage, viewController: self)
                        self.sortProducts()
                    }
                    else
                    {
                        self.productsCount = result.value(forKey: "count") as! NSString
                        self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                        self.sortProducts()
                        self.productListCollectionView.reloadData()
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyProductAlertMessage, viewController: self)
                    self.sortProducts()
                }
            })
            
        }
        else
        {
            if isFromHome
            {
                sortProducts()
                buttonsView.isHidden = true
                self.productListCollectionView.frame.origin.y = 0
                self.productListCollectionView.frame.size.height = self.view.frame.size.height
                
                self.productListCollectionView.reloadData()
            }
            else
            {
                self.productListCollectionView.reloadData()
            }
        }
        
        if self.filterAry.count == 0 && !categoryID.isEmpty{
            
            BusinessManager.getFilterProducts(categoryID, languageID: languageID as String, completionHandler: { (result) -> () in
                
                self.filterAry = result.value(forKey: "filters") as! NSMutableArray
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.title = nil
        isSorted = false
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func sortProducts()
    {
        for (index,tempDic) in self.productListAry.enumerated()
        {
            let temp:NSMutableDictionary = tempDic as! NSMutableDictionary
            //(forKey: "price")
            let priceWithOutSymbol:String = String((temp.object(forKey: "price") as! String).characters.dropFirst())
            
            if let myNumber = Int(priceWithOutSymbol.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                temp.setObject(myNumber, forKey: "price_sort" as NSCopying)
                self.productListAry.replaceObject(at: index, with: temp)
            }
        }
        
        let flowlayout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout1.minimumInteritemSpacing = 1.0
        flowlayout1.minimumLineSpacing = 1.0
        flowlayout1.sectionInset.top = 0
        flowlayout1.sectionInset.left = 1.0;
        flowlayout1.sectionInset.right = 1.0;
        flowlayout1.itemSize.width = (self.productListCollectionView.frame.width - 3) / 2
        flowlayout1.itemSize.height = 294;
        self.productListCollectionView .reloadData()
        self.productListCollectionView.collectionViewLayout = flowlayout1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productListAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        let imageUrl =  (self.productListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as! String
        
        if (imageUrl as AnyObject).lowercased.range(of: "no_image") != nil
        {
            
        }
        else
        {
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
            cell.pinImage.sd_setImage(with: URL(string: trimmedUrl))
            cell.pinImage.contentMode = UIViewContentMode.scaleAspectFit
            
            cell.title.text = (self.productListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            
            cell.splPriceLbl.text = (productListAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
            
            if !cell.splPriceLbl.text!.isEmpty
            {
                cell.splPriceLbl.text = (productListAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                
                cell.priceLbl.text = (productListAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
                
                cell.splPriceLbl.isHidden = false
                let attributeString:NSMutableAttributedString =  NSMutableAttributedString(string: cell.splPriceLbl.text!)
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
                cell.splPriceLbl.attributedText = attributeString
            }
            else
            {
                cell.priceLbl.text = (productListAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                
                cell.splPriceLbl.isHidden = true
            }
            cell.priceLbl.sizeToFit()
            cell.splPriceLbl.frame.origin.x = cell.priceLbl.frame.origin.x + cell.priceLbl.frame.width + 5
            cell.splPriceLbl.sizeToFit()
            if (self.productListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String != ""
            {
                let discoutStr:String = ((self.productListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String)!
                cell.discountLbl.text = discoutStr
                
                cell.discountLbl.isHidden = false
            }
            else
            {
                cell.discountLbl.isHidden = true
            }
            
            
            let str2 = (self.productListAry.object(at: indexPath.row) as! NSDictionary).value(forKey: "product_id") as! NSString
            
            if self.checkProductInWishList(str2 as String) == true
            {
                let image = UIImage(named: "heartsmallorg") as UIImage!
                cell.imgFav.image  = image
            }
            else
            {
                let image = UIImage(named: "heartsmallGrey") as UIImage!
                cell.imgFav.image  = image
            }
            cell.addToFav.addTarget(self, action: #selector(ProductListVC.addToFav(_:)), for: UIControlEvents.touchUpInside)
            cell.addToFav.tag = (indexPath as NSIndexPath).row
        }
        
        cell.pinImage.contentMode = UIViewContentMode.scaleAspectFill

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        self.performSegue(withIdentifier: "ProductDetailsSegue", sender: indexPath)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetailsSegue"
        {
            let productDetailObj = segue.destination as! ProductDetailsVC
          
            productDetailObj.productID = (self.productListAry.object(at: (sender! as AnyObject).row) as! NSDictionary).value(forKey: "product_id") as! String

        }
        else{
            let Obj = segue.destination as! FilterVC
            Obj.filterAry = filterAry
        }
    }
    
   /* func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if SharedManager.DeviceType.IS_IPHONE_4_OR_LESS
        {
            return CGSize(width:150, height:255)
        }
        else if SharedManager.DeviceType.IS_IPHONE_5
        {
            return CGSize(width:150, height:255)
        }
        else if SharedManager.DeviceType.IS_IPHONE_6
        {
            return CGSize(width:176, height:255)
        }
        else if SharedManager.DeviceType.IS_IPHONE_6P
        {
            return CGSize(width:191, height:255)
        }
        else
        {
            return CGSize(width:191, height:255)
        }
    }*/
    
    func pullToRefresh()
    {
        if isSorted == true
        {
            if sortPage != Int(self.sortPageCount) + 1 && !categoryID.isEmpty
            {
                sortPage += 1
                BusinessManager.getSortedProductList(categoryID, languageID: languageID as String, page:sortPage, limit:limit, sort:sortKey, order:orderKey, filters:filterID, completionHandler: { (result) -> () in
                    
                    if let resultArr = result.value(forKey: "products")
                    {
                        for i in 0 ..< (resultArr as AnyObject).count
                        {
                            let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                            
                            if ((nameSTr)?.lowercased().contains("series"))!
                            {
                                
                            }
                            else
                            {
                                self.productListAry.add((resultArr as AnyObject).object(at: i))
                            }
                        }
                    }
                    
                    //   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    
                    self.productListCollectionView.reloadData()
                    
                    //  }
                })
            }
            else
            {
                
            }
        }
        else
        {
            if page != Int(self.pageCount) + 1 && !categoryID.isEmpty
            {
                page += 1
                BusinessManager.getProductList(categoryID, languageID: languageID as String, page:page, filters:filterID, limit: "6", sort: sortKey, order: orderKey, completionHandler: { (result) -> () in
                    
                    if let resultArr = result.value(forKey: "products")
                    {
                        for i in 0 ..< (resultArr as AnyObject).count
                        {
                            let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                            
                            if ((nameSTr)?.lowercased().contains("series"))!
                            {
                                
                            }
                            else
                            {
                                self.productListAry.add((resultArr as AnyObject).object(at: i))
                            }
                        }
                    }
                    //   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    
                    self.productListCollectionView.reloadData()
                    
                    //  }
                })
            }
            else
            {
                
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.pullToRefresh()
    }

     func addToCard(_ sender : UIButton)
    {
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let object:NSArray = (self.productListAry.object(at: (index as NSIndexPath).row) as! NSDictionary).value(forKey: "options") as! NSArray
        if object.count == 0
        {
            
            let str2 = (self.productListAry.object(at: (index as NSIndexPath).row) as! NSDictionary).value(forKey: "product_id") as! NSString
            
            
            if self.checkProductInCart(str2 as String) == true
            {
                let isCartHave:Bool = self.checkProductInCart(str2 as String)
                
                if UserDefaults.standard.object(forKey: kAddToCart) != nil
                {
                    let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
                    let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
          
                    if isCartHave
                    {
                        for tempDic in cartAry
                        {
                            let tempCart:NSDictionary = tempDic as! NSDictionary
                            if let str1 = tempCart.value(forKey: "product_id")
                            {
                                if (str1 as! NSString).isEqual(to: str2 as String)
                                {
                                    cartAry.remove(tempDic)
                                }
                            }
                        }
                    }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToCart)
                        
                   
                }
            }
            else
            {
                let isCartHave:Bool = self.checkProductInCart(str2 as String)
                
                if UserDefaults.standard.object(forKey: kAddToCart) != nil
                {
                    let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
                    let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
              
                    if !isCartHave
                    {
                        let userQuantity : NSMutableDictionary  = self.productListAry.object(at: (index as NSIndexPath).row) as! NSMutableDictionary
                        userQuantity.setValue("1", forKey: "userQuantity")
                        cartAry.add(userQuantity)
                    }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToCart)
                    
                    
                }
                else
                {
                    let userQuantity : NSMutableDictionary  = self.productListAry.object(at: (index as NSIndexPath).row) as! NSMutableDictionary
                    userQuantity.setValue("1", forKey: "userQuantity")
                    let array:NSMutableArray = []
                    array.add(userQuantity)

                    let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                    UserDefaults.standard.set(data1, forKey: kAddToCart)
                }
            }
            
        }
        else
        {
            self.performSegue(withIdentifier: "ProductDetailsSegue", sender: index)
        }
        self.productListCollectionView.reloadData()
        self.updateCartBadge()
    }
    
    func addToFav(_ sender : UIButton)
    {
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let array:NSMutableDictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableDictionary
            userIDStr = array.value(forKeyPath: "customer.customer_id") as! String
        
            self.addFavTag = sender.tag
        
        
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let str2 = (self.productListAry.object(at: (index as NSIndexPath).row) as! NSDictionary).value(forKey: "product_id") as! NSString
        
        let values:String = NSString(format: "product_id=%@&user_id=%@",str2 as String,userIDStr) as String
        
        if self.checkProductInWishList(str2 as String) == true
        {
            SharedManager.showHUD(viewController: self)
            BusinessManager.removeFromWishListWithValues(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    let isHave:Bool = self.checkProductInWishList(str2 as String)
                    
                    if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                    {
                        let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                        let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                        
                        if isHave
                        {
                            for tempDic in cartAry
                            {
                                let tempCart:NSDictionary = tempDic as! NSDictionary
                                if let str1 = tempCart.value(forKey: "product_id")
                                {
                                    if (str1 as! NSString).isEqual(to: str2 as String)
                                    {
                                        cartAry.remove(tempDic)
                                    }
                                }
                                
                            }
                        }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                    }
                    
                }

                self.productListCollectionView.reloadData()
            })
        }
    
        else
        {
                SharedManager.showHUD(viewController: self)
                BusinessManager.addToWishListWithValues(values, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)

                    
                    let code = result.value(forKey: "status") as! String
                   
                    
                    if code == "200"
                    {
                        let isHave:Bool = self.checkProductInWishList(str2 as String)
                        
                        if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                        {
                            let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                            
                            if !isHave
                            {
                                cartAry.add(self.productListAry.object(at: (index as NSIndexPath).row))
                            }
                            let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                            UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                        }
                        else
                        {
                            let array:NSMutableArray = []
                            array.add(self.productListAry.object(at: (index as NSIndexPath).row))
                            let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                            UserDefaults.standard.set(data1, forKey: kAddToWishList)
                            
                        }
                    }
                        self.productListCollectionView.reloadData()
                })
            }
            
        }


            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
                viewController.delegate = self
                
                self.present(viewController, animated: true, completion: { () -> Void in
                    
                })
                
            }
            
    
    
        }
    
        
        
    func loginSuccess() {
       
        let index:IndexPath = IndexPath(row: addFavTag, section: 0)
        let str2 = (self.productListAry.object(at: index.row) as! NSDictionary).value(forKey: "product_id") as! NSString
        
        let values:String = NSString(format: "product_id=%@&user_id=%@",str2 as String,userIDStr) as String
        
            SharedManager.showHUD(viewController: self)
        BusinessManager.addToWishListWithValues(values, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)

            
            let code = result.value(forKey: "status") as! String
            
            if code == "200"
            {
                let isHave:Bool = self.checkProductInWishList(str2 as String)
                
                if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                {
                    let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                    let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                    
                    if !isHave
                    {
                        cartAry.add(self.productListAry.object(at: (index as NSIndexPath).row))
                    }
                    let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                    UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                }
                else
                {
                    let array:NSMutableArray = []
                    array.add(self.productListAry.object(at: (index as NSIndexPath).row))
                    let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                    UserDefaults.standard.set(data1, forKey: kAddToWishList)
                    
                }
            }
                       self.productListCollectionView.reloadData()
        })

    }
    
    func loginFailure() {
        
    }
    
    func checkProductInWishList(_ str2:String) -> Bool
    {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToWishList) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in favAry
            {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                if let str1 = tempCart.value(forKey: "product_id")
                {
                    if (str1 as! NSString).isEqual(to: str2 as String)
                    {
                        isAleadyHave = true
                    }
                }
            }
        }
        return isAleadyHave
    }
    
    func checkProductInCart(_ str2:String) -> Bool
    {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToCart) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
        
            for tempDic in cartAry
            {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                if let str1 = tempCart.value(forKey: "product_id")
                {
                    if (str1 as! NSString).isEqual(to: str2 as String)
                    {
                        isAleadyHave = true
                    }
                }
            }
        }
        return isAleadyHave
    }
    
    @IBAction func sortAction(_ sender: AnyObject)
    {
        let picker = BSModalPickerView.init(values: sortItems)
        
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice {
                
                switch(Int((picker?.selectedIndex)!))
                {
                case 0:
                    self.sortKey = "pd.name"
                    self.orderKey = "ASC"
                    break
                case 1:
                    self.sortKey = "pd.name"
                    self.orderKey = "DESC"
                    break
                case 2:
                    self.sortKey = "p.price"
                    self.orderKey = "ASC"
                    break
                case 3:
                    self.sortKey = "p.price"
                    self.orderKey = "DESC"
                    break
                case 5:
                    self.sortKey = "p.model"
                    self.orderKey = "ASC"
                    break
                case 6:
                    self.sortKey = "p.model"
                    self.orderKey = "DESC"
                    break
                default:
                    break
                    
                }
                self.sorting()
            }
            else
            {
                
            }
        })
    }
    
    func sorting()
    {
        SharedManager.showHUD(viewController: self)
        BusinessManager.getSortedProductList(categoryID, languageID: languageID as String, page:1, limit:limit, sort: sortKey, order: orderKey, filters:filterID, completionHandler: { (result) -> () in
            
            SharedManager.dismissHUD(viewController: self)
            
            if let resultArr = result.value(forKey: "products")
            {
                self.isSorted = true
                self.productListAry.removeAllObjects()
                self.sortPage = 1
                
                for i in 0 ..< (resultArr as AnyObject).count
                {
                    let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                    
                    if ((nameSTr)?.lowercased().contains("series"))!
                    {
                        
                    }
                    else
                    {
                        self.productListAry.add((resultArr as AnyObject).object(at: i))
                    }
                }
                
                // self.productListAry = resultArr as! NSMutableArray
                
                if self.productListAry.count == 0
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyProductAlertMessage, viewController: self)
                    self.sortProducts()
                }
                else
                {
                    self.sortProductsCount = result.value(forKey: "count") as! NSString
                    self.sortPageCount = Double(self.sortProductsCount as String)!/Double(self.limit)!
                    self.sortProducts()
                    self.productListCollectionView.reloadData()
                    self.productListCollectionView.setContentOffset(.zero, animated: true)
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyProductAlertMessage, viewController: self)
                self.sortProducts()
            }
        })
    }

    @IBAction func filterAction(_ sender: AnyObject)
    {
        if filterAry.count != 0
        {
            NotificationCenter.default.addObserver(self, selector: #selector(ProductListVC.getFilterID(notification:)), name: NSNotification.Name(rawValue: "getFilterData"), object: nil)
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kEmptyFilterAlertMessage, viewController: self)
        }
    }
    
    func getFilterID(notification: NSNotification)
    {
        if !(notification.object as! String).isEmpty
        {
            productListAry = []
            page = 1
            filterID = notification.object as! String
            self.viewDidLoad()
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getFilterData"), object: nil)
        }
    }
    
    func reviewsBtnClick(_ sender: UIButton)
    {
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // instantiate your desired ViewController
        let VC = storyboard.instantiateViewController(withIdentifier: "ReviewSegue") as! ReviewsVC
        VC.reviewsAry = (self.productListAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "reviews_list") as! NSArray
        VC.productName = ((self.productListAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String)!
        
        VC.productID = ((self.productListAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "product_id") as? String)!
        self.navigationController!.pushViewController(VC, animated: true)
    }
}
