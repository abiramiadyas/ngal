//
//  Constant.h
//  4Tech
//
//  Created by Adyas on 17/11/16.
//  Copyright © 2016 adyas. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define kGETMethod                               "GET"
#define kPOSTMethod                              "POST"

#define kLanguageID                             "_kLanguage_ID"
#define kSortOrder                              "_kSort_Order_List"

#define kCountryList                            "_kCountry_List"

#define kAddToCart                              "_kADDTOCART_CONSTANT"
#define kAddToCartOptions                       "_KADDTOCART_CONSTANT_OPTIONS"
#define kAddToWishlist                          "_KADDTOWISHLIST_CONSTANT"
#define kUserDetails                            "_k_USER_DETAILS"
#define kProfileInfo                            "_kPROFILE_INFO"

#define kEmailValidAlertMessage                 "Please enter valid email Id"
#define kEmptyTextFieldAlertMessage             "Please enter all the details"
#define kAlertTitleMessage                      "Information"
#define kAddToCartAlertMessage                  "Your cart is empty!"
#define kProductMaximumQuantity                 "This Product is not available in the desired Quantity or not in Stock"
#define kProductMinimunQuantity                 "Minimum quantity should be atleast 1"
#define kProductWishlistAlertMessage            "Your favorite item list is empty!"
#define kProductOrdersAlertMessage              "Your order list is empty!"
#define kEmptyProductAlertMessage               "No products Available"
#define kEmptyFilterAlertMessage                "Filter is not Available"
#define kReviewAlertMessage                     "Please enter the review"
#define kEmptySearchAlertMessage                "Try with some other keyword."
#define kPasswordMismatchAlertMessage           "Passwords does not matched"


#endif /* Constant_h */
