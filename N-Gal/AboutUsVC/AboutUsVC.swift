//
//  AboutUsVC.swift
//  Exlcart
//
//  Created by iPhone on 23/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController, UIWebViewDelegate {

    var urlString = String ()
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.webView.delegate = self
        
        SharedManager.showHUD(viewController: self)
        urlString = "https://www.n-gal.com/about-us"
        self.webView.loadRequest(NSURLRequest(url: NSURL(string: self.urlString)! as URL) as URLRequest)
        // self.view.addSubview(self.webView)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SharedManager.dismissHUD(viewController: self)
        
        if webView.isLoading
        {
            return
        }
        else
        {
            
        }
    }
}
