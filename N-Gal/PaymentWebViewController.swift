//
//  PaymentWebViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 13/12/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class PaymentWebViewController: ParentViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.webView.delegate = self
        SharedManager.showHUD(viewController: self)
        let url = NSString(format: "https://www.n-gal.com/index.php?route=payment/mobileccavenuepay&order_id=%@", orderID)
        print("url:\(url)")
        let urlRequest = URLRequest(url: URL(string: url as String)!)
        self.webView.loadRequest(urlRequest)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SharedManager.dismissHUD(viewController: self)
        let string = webView.request!.url!.absoluteString
        let responseString = string as NSString
        
        if responseString.range(of: "payment/mobileccavenuepay/callback").location != NSNotFound
        {
            let html = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML")!
            
            var status = "Not Known"
            
            print(html as NSString)
            //|| ((html as NSString).range(of: "aborted").location != NSNotFound)
            if ((html as NSString).range(of: "failure").location != NSNotFound)
            {
                status = "0"
                let alert = UIAlertController(title: "Sorry!", message: "Your Transaction Failed", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                     let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
                     self.navigationController?.pushViewController(viewController, animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else if ((html as NSString).range(of: "aborted").location != NSNotFound)
            {
                status = "0"
                let alert = UIAlertController(title: "Sorry!", message: "Your Transaction Cancelled", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
                    self.navigationController?.pushViewController(viewController, animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else if ((html as NSString).range(of: "success").location != NSNotFound)
            {
                status = "1"
                
                let alert = UIAlertController(title: "Success!", message: "Your Transaction Processed Successfully", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(alert :UIAlertAction) in
                    
                    UserDefaults.standard.removeObject(forKey: kAddToCart)
                    UserDefaults.standard.removeObject(forKey: "COUPON")
                    UserDefaults.standard.removeObject(forKey: "VOUCHER")
                    UserDefaults.standard.removeObject(forKey: "REWARDS")
                    paymentDict = [:]
                    shippingDict = [:]
                    shippingResultDict = [:]
                    productTempDict = [:]
                    productTempArry.removeAllObjects()
                    orderID = ""
                    self.updateCartBadge()
                    
                    UserDefaults.standard.set("Transaction Successful", forKey: "TRANS_STATUS")
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessViewController") as! ResultSuccessViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Sorry!", message: "Something went wrong!!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
                    self.navigationController?.pushViewController(viewController, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
                    self.navigationController?.pushViewController(viewController, animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
