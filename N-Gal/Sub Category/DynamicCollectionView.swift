//
//  DynamicCollectionView.swift
//  Exlcart
//
//  Created by Adyas Iinfotech on 16/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class DynamicCollectionView: UICollectionView {

    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
        
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }

}
