//
//  OtherCategoriesViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 17/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class OtherCategoriesViewController: UIViewController, UIWebViewDelegate
{
    var urlString = String ()
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        //Navigation Bar
        let barView = UIView()
        barView.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        
        let img1 = UIImageView()
        img1.image = UIImage (named: "right-arrow.png")
        img1.frame = CGRect(x: 0, y: 10, width: 15, height: 20)
        img1.contentMode = .scaleAspectFit
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.black, for: .normal)
        myBackButton.sizeToFit()
        myBackButton.frame = CGRect(x: 12, y: 5, width: 55, height: 30)
        
        barView.addSubview(img1)
        barView.addSubview(myBackButton)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: barView)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        self.webView.delegate = self
        
        SharedManager.showHUD(viewController: self)
        print(self.urlString)
        self.webView.loadRequest(NSURLRequest(url: NSURL(string: self.urlString)! as URL) as URLRequest)
       // self.view.addSubview(self.webView)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func closeButtonAction(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SharedManager.dismissHUD(viewController: self)
        
        if webView.isLoading
        {
            return
        }
        else
        {
            
        }
    }    
}
