//
//  CheckOutVC.swift
//  Exlcart
//
//  Created by iPhone on 23/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CheckOutVC: ParentViewController,UITabBarControllerDelegate,loginIntimation {

    @IBOutlet weak var checkOutTable: UITableView!
    @IBOutlet weak var subTotalView: UIView!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    
    @IBOutlet weak var txtGiftCode: UITextField!
    @IBOutlet weak var lblGiftTypeTitle: UILabel!
    @IBOutlet weak var viewCoupon: UIView!
    @IBOutlet weak var viewBlurEff: UIView!
    var totalsArr = NSMutableArray()
    var giftTypeArr = NSMutableArray()
    var checkOutAry:NSMutableArray = []
    var cart:NSMutableArray = []
    var jsonStr:String = ""
    var totalPrice:String = ""
    var rewardStr:String = ""
    var maxRewardStr:String = ""
    
    var giftType = String()
    
    var customerId = userIDStr
    
    var localRewards : String = ""
    var localCoupon : String = ""
    var localVoucher : String = ""
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var totalScrollView: UIScrollView!
    
    @IBOutlet weak var viewWarning: UIView!
    @IBOutlet weak var txtRewards: UITextField!
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet var okBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.viewCoupon.isHidden = true
        self.viewBlurEff.isHidden = true
        okBtn.layer.borderColor = themeColor.cgColor
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated);
        let tabbarObj:UITabBarController = self.tabBarController!
        tabbarObj.delegate=self
        self.setLeftNavigationButton()
        
        UserDefaults.standard.set("", forKey: "COUPON")
        UserDefaults.standard.set("", forKey: "VOUCHER")
        UserDefaults.standard.set("", forKey: "REWARDS")
        
        if let id = UserDefaults.standard.string(forKey: "ADDRESS_ID")
        {
            addressIDStr = id
        }
        
        let delayTime = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
        }
        self.loadData()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        self.navigationController?.popToRootViewController(animated: false)
    }

    func loadData()
    {
        self.checkOutTable.tableFooterView = UIView()
        if let data = UserDefaults.standard.object(forKey: kAddToCart) as? Data
        {
            checkOutAry = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            if checkOutAry.count == 0
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kAddToCartAlertMessage, viewController: self)
                checkOutAry = []
                self.updateCartBadge()
                self.checkOutTable.reloadData()
            }
            else
            {
                let products:NSMutableArray = []
                var dicti:NSMutableDictionary = [:]
                var tempData:NSMutableDictionary = [:]
                
                for temp in checkOutAry
                {
                    tempData = temp as! NSMutableDictionary
                    let options:NSArray = tempData.value(forKey: "options") as! NSArray
                    let productIdDic:NSMutableDictionary = [:]
                    if options.count == 0
                    {
                        productIdDic.setValue(tempData.value(forKeyPath: "product_id") as! String, forKey: "product_id")
                        productIdDic.setValue(tempData.value(forKeyPath: "userQuantity") as! String, forKey: "quantity")
                        products.add(productIdDic)
                    }
                    else
                    {
                        let optionValues:NSMutableDictionary = tempData.value(forKey: "product_option_selection_value") as! NSMutableDictionary
                     
                        productIdDic.setValue(optionValues, forKey: "option")
                        
                        productIdDic.setValue(tempData.value(forKeyPath: "product_id") as! String, forKey: "product_id")
                        
                        productIdDic.setValue(tempData.value(forKeyPath: "userQuantity") as! String, forKey: "quantity")
                        
                        products.add(productIdDic)
                    }
                    
                    if userIDStr != ""
                    {
                        if let rewards = UserDefaults.standard.value(forKey: "REWARDS")
                        {
                            localRewards = rewards as! String
                        }
                        
                        if let coupon = UserDefaults.standard.value(forKey: "COUPON")
                        {
                            localCoupon = coupon as! String
                        }
                        
                        if let coupon = UserDefaults.standard.value(forKey: "VOUCHER")
                        {
                            localVoucher = coupon as! String
                        }
                        
                        dicti = ["customer_id": userIDStr, "address_id": addressIDStr, "products":products, "coupon": localCoupon, "reward": localRewards, "voucher": localVoucher, "language_id":tempData.value(forKeyPath: "language_id") as! String]
                    }
                    else
                    {
                        if let coupon = UserDefaults.standard.value(forKey: "COUPON")
                        {
                            localCoupon = coupon as! String
                        }
                        
                        if let voucher = UserDefaults.standard.value(forKey: "VOUCHER")
                        {
                            localVoucher = voucher as! String
                        }
                        
                        dicti = ["customer_id": userIDStr, "address_id": "0", "products":products, "coupon": localCoupon, "reward": "", "voucher": localVoucher, "language_id":tempData.value(forKeyPath: "language_id") as! String]
                    }
                }
                
                do
                {
                    let jsonData: Data = try JSONSerialization.data(withJSONObject: dicti, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    jsonStr = NSString.init(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                }
                catch {
                    print("error")
                }
                
                SharedManager.showHUD(viewController: self)
                BusinessManager.getCartProducts(jsonStr, completionHandler: { (result) -> ()
                    in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if let resultStr = result.value(forKey: "cart")
                    {
                        self.cart = resultStr as! NSMutableArray
                        self.totalsArr = result.value(forKeyPath: "cart_info.totals") as! NSMutableArray

                        if userIDStr != ""
                        {
                            self.rewardStr = "\(String(describing: result.value(forKey: "customer_reward_points")!))"
                            self.maxRewardStr = "\(String(describing: result.value(forKey: "maximum_reward_points")!))"
                            
                            if self.rewardStr != "0" && self.maxRewardStr != "0"
                            {
                                self.giftTypeArr = ["COUPON", "GIFT VOUCHER", "REWARD POINTS"]
                            }
                            else
                            {
                                self.giftTypeArr = ["COUPON", "GIFT VOUCHER"]
                            }
                        }
                        else
                        {
                            self.giftTypeArr = ["COUPON", "GIFT VOUCHER"]
                        }
                        
                        // Frames:
                        self.checkOutTable.frame.size.height = self.checkOutTable.contentSize.height
                    }
                    super.viewWillAppear(true)
                    self.updateCartBadge()
                    self.checkOutTable.reloadData()
                    let temp = NSKeyedArchiver.archivedData(withRootObject: self.cart)
                    UserDefaults.standard.set(temp, forKey: "cart_array")
                    
                   // Frames:
                    
                    self.checkOutTable.frame.size.height = self.checkOutTable.contentSize.height
                    
                    self.mainScrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: self.checkOutTable.contentSize.height)
                })
            }
        }
    }
    
    @IBAction func proceedToCheckOutAction(_ sender: AnyObject)
    {
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            if checkOutAry.count == 0
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kAddToCartAlertMessage, viewController: self)
                return
            }
            else
            {
                self.performSegue(withIdentifier: "CheckOutStep2Segue", sender: nil)
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
    
            })
        }
    }
    
    func loginSuccess()
    {
        if checkOutAry.count == 0
        {
            SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kAddToCartAlertMessage, viewController: self)
            return
        }
        else
        {
            self.performSegue(withIdentifier: "CheckOutStep2Segue", sender: nil)
        }
        
    }
    
    func loginFailure() {
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == self.categoryListTbl
        {
            return self.categoryListAry.count
        }
        else
        {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView ==  self.categoryListTbl && arrayForBool.count > 0 {
            if CBool(arrayForBool[section] as! NSNumber)
            {
                return (self.subListAry[section] as AnyObject).count
            }
            else
            {
                return 0
            }
        }
        else if tableView == self.checkOutTable
        {
            if cart.count != 0
            {
                return self.cart.count + self.giftTypeArr.count + self.totalsArr.count
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.categoryListTbl
        {
            var cell = self.categoryListTbl.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
            
            if (cell == nil){
                cell = UITableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
            }
            
            let manyCells: Bool = CBool(arrayForBool[(indexPath as NSIndexPath).section] as! NSNumber)
            
            if !manyCells
            {
                cell?.backgroundColor = UIColor.clear
                cell?.textLabel?.text = ""
            }
            else
            {
                
                cell?.textLabel?.text = ((self.subListAry.object(at: (indexPath as NSIndexPath).section) as! NSArray).value(forKey: "name") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell?.backgroundColor = UIColor.white
                cell?.indentationLevel = 2
                
            }
            cell?.textLabel?.textColor = UIColor.black
            cell?.accessoryType = UITableViewCellAccessoryType.none
            
            return cell!

        }
        else
        {
            if indexPath.row >= self.cart.count + self.giftTypeArr.count
            {
                let cell:CheckOutTableViewCell = self.checkOutTable.dequeueReusableCell(withIdentifier: "totalCell") as! CheckOutTableViewCell
                
                cell.lblTotalTitle.text = "\((totalsArr[indexPath.row - (self.cart.count + self.giftTypeArr.count)] as AnyObject).value(forKey: "title") as! String)"
                
                cell.lblTotalValue.text = "\((totalsArr[indexPath.row - (self.cart.count + self.giftTypeArr.count)] as AnyObject).value(forKey: "value") as! String)"
                
                if "\((totalsArr[indexPath.row - (self.cart.count + self.giftTypeArr.count)] as AnyObject).value(forKey: "title") as! String)" == "Total"
                {
                    self.totalPrice = "\((totalsArr[indexPath.row - (self.cart.count + self.giftTypeArr.count)] as AnyObject).value(forKey: "value") as! String)"
                    cartTotalAmount = "\(self.totalPrice)" as NSString
                }
                return cell
            }
            else if indexPath.row >= self.cart.count
            {
                let cell:CheckOutTableViewCell = self.checkOutTable.dequeueReusableCell(withIdentifier: "giftCell") as! CheckOutTableViewCell
                
                cell.lblGiftType.text = "\(giftTypeArr[indexPath.row - self.cart.count] as! String)"
                cell.lblGiftType.layer.borderColor = UIColor.darkGray.cgColor
                
                return cell
            }
            else
            {
                let cell:CheckOutTableViewCell = self.checkOutTable.dequeueReusableCell(withIdentifier: "cell") as! CheckOutTableViewCell
                cell.titleLbl.text = (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
                cell.quantityLbl.text = (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "quantity") as? String
                let price = (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "price") as? String
                cell.priceLbl.text = price
                
                let igst = "\(String(describing: (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "igstrate")!))"
                if igst == "0"
                {
                    let cgst = "\(String(describing: (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "cgstrate")!))"
                    let sgst = "\(String(describing: (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "sgstrate")!))"
                    
                    let cgstAmount = "\(String(describing: (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "cgst")!))"
                    let sgstAmount = "\(String(describing: (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "sgst")!))"
                    
                    cell.lblGst.text = "CGST (\(String(describing: cgst))%) : \(String(describing: cgstAmount)) \nSGST (\(String(describing: sgst))%) : \(String(describing: sgstAmount))"
                }
                else
                {
                    let igstAmount = "\(String(describing: (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "igst")!))"
                    cell.lblGst.text = "IGST (\(String(describing: igst))%) : \(String(describing: igstAmount))"
                }
                
                let imageUrl =  (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as! String
                let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                cell.productImage.sd_setImage(with: URL(string: trimmedUrl))
                cell.increaseQuantityBtn.addTarget(self, action: #selector(CheckOutVC.increaseQuantity(_:)), for: UIControlEvents.touchUpInside)
                cell.deleteBtn.addTarget(self, action: #selector(CheckOutVC.deleteAction(_:)), for: UIControlEvents.touchUpInside)
                cell.increaseQuantityBtn.tag = (indexPath as NSIndexPath).row;
                
                cell.deleteBtn.tag = (indexPath as NSIndexPath).row;
                
                cell.decreaseQuantityBtn.addTarget(self, action: #selector(CheckOutVC.decreaseQuantity(_:)), for: UIControlEvents.touchUpInside)
                cell.decreaseQuantityBtn.tag = (indexPath as NSIndexPath).row
                
                
                let optionArray = (self.cart.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "option") as! NSArray
                var y:CGFloat = 42.0
                
                for temp in optionArray
                {
                    cell.nameLbl.frame = CGRect(x: 95, y: y, width: 52, height: 20)
                    cell.nameLbl.font = UIFont.systemFont(ofSize: 12)
                    cell.nameLbl.text = (temp as AnyObject).object(forKey: "name") as? String
                    if (cell.nameLbl.text?.lowercased().contains("size"))!
                    {
                        cell.nameLbl.text = "Size"
                    }
                    cell.colonLbl.frame = CGRect(x: 150, y: y, width: 52, height: 20)
                    cell.colonLbl.font = UIFont.systemFont(ofSize: 12)
                    cell.colonLbl.text = ":"
                    
                    cell.textLbl.frame =  CGRect(x: 180, y: y, width: 52, height: 20)
                    cell.textLbl.font = UIFont.systemFont(ofSize: 12)
                    cell.textLbl.text = (temp as AnyObject).object(forKey: "value") as? String
                    
                    y = y + 20
                }
                
                
                return cell
            }
        }
    }

    func deleteAction(_ sender : UIButton)
    {
        let cartTemp:NSMutableDictionary = checkOutAry.object(at: sender.tag) as! NSMutableDictionary
        let options:NSArray = cartTemp.value(forKey: "options") as! NSArray
            
            let str1:NSString = cartTemp.value(forKey: "product_id") as! NSString

                    if options.count == 0
                    {
                    for removeCart in checkOutAry
                    {
                        let removeCartData:NSMutableDictionary = removeCart as! NSMutableDictionary
                        let str2:NSString = removeCartData.value(forKey: "product_id") as! NSString
                        

                            if str1.isEqual(to: str2 as String)
                            {
                                
                                checkOutAry.remove(removeCartData)
                                cart.removeAllObjects()
                                break
                            }
                    }
                            let cartTempData = NSKeyedArchiver.archivedData(withRootObject: checkOutAry)
                            UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
                    }
                        else
                        {
                            let optionValues:NSMutableDictionary = cartTemp.value(forKey: "product_option_selection_value") as! NSMutableDictionary
                            
                            for removeCart in checkOutAry
                            {

                            let removeCartData:NSMutableDictionary = removeCart as! NSMutableDictionary
                                let str2:NSString = removeCartData.value(forKey: "product_id") as! NSString
                                
                            if str1.isEqual(to: str2 as String)
                            {
                                
                                let temp:NSMutableDictionary = removeCartData.object(forKey: "product_option_selection_value") as! NSMutableDictionary
                                if temp == optionValues
                                {
                                    checkOutAry.remove(removeCartData)
                                    cart.removeAllObjects()
                                    break
                                }
                            }
                        }
                            
                                
                                let cartTempData = NSKeyedArchiver.archivedData(withRootObject: checkOutAry)
                                UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
        }
        self.loadData()
    }
    
    func increaseQuantity(_ sender : UIButton)
    {
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.checkOutTable.cellForRow(at: index) as! CheckOutTableViewCell
        let quantity:Int?  = Int((cell.quantityLbl.text)!)
      //  let maxQuantity:String  = (checkOutAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "quantity") as! String

        let optionsDic = (checkOutAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "product_option_selection_value") as! NSMutableDictionary
        
        let selectedOption = (checkOutAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "options") as! NSArray
        var selectedI = Int()
        var selectedJ = Int()
        
        for i in 0..<selectedOption.count
        {
            let values = (selectedOption[i] as AnyObject).value(forKey: "product_option_value") as! NSMutableArray
            
            for j in 0..<values.count
            {
                let valuedId = (values[j] as AnyObject).value(forKey: "product_option_value_id") as! String
                
                let key:NSArray = optionsDic.allKeys as NSArray
                let optionsKey = optionsDic.value(forKey: key[0] as! String) as! String
                
                if optionsKey == valuedId
                {
                    selectedI = i
                    selectedJ = j
                    
                    break
                }
            }
        }
        
        let selected = ((selectedOption[selectedI] as AnyObject).value(forKey: "product_option_value") as! NSMutableArray).object(at: selectedJ)
        let maxQuantity = (selected as AnyObject).value(forKey: "quantity") as! String
        
        if Int(maxQuantity) > quantity
        {
            let answer = quantity! + 1
            let price = (cart.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "total") as? String

            cell.quantityLbl.text =   "\(answer)"
            cell.priceLbl.text = price
            
            let userQuantity:NSDictionary = checkOutAry.object(at: (index as NSIndexPath).row) as! NSDictionary
            userQuantity.setValue(cell.quantityLbl.text, forKey: "userQuantity")
            checkOutAry.replaceObject(at: sender.tag, with: userQuantity)
            
            let cartTempData = NSKeyedArchiver.archivedData(withRootObject: self.checkOutAry)
            UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
            
            self.loadData()
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kProductMaximumQuantity, viewController: self)
        }
    }
    
    func decreaseQuantity(_ sender : UIButton)
    {
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.checkOutTable.cellForRow(at: index) as! CheckOutTableViewCell
        let quantity:Int?  = Int((cell.quantityLbl.text)!)
       
     
            if quantity > 1
            {
            let answer = quantity! - 1
            let price = (cart.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "total") as? String
            
            cell.quantityLbl.text =   "\(answer)"
            cell.priceLbl.text = price
            let userQuantity:NSDictionary = checkOutAry.object(at: (index as NSIndexPath).row) as! NSDictionary
            userQuantity.setValue(cell.quantityLbl.text, forKey: "userQuantity")
            
            checkOutAry.replaceObject(at: sender.tag, with: userQuantity)
            cart.remove(userQuantity)
                
            let cartTempData = NSKeyedArchiver.archivedData(withRootObject: self.checkOutAry)
            UserDefaults.standard.set(cartTempData, forKey: kAddToCart)

            self.loadData()
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kProductMinimunQuantity, viewController: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.categoryListTbl
        {
            return 44
        }
        else
        {
            if indexPath.row >= self.cart.count + self.giftTypeArr.count
            {
                return 54
            }
            else if indexPath.row >= self.cart.count
            {
                return 42
            }
            else
            {
                return 130
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.categoryListTbl
        {
            self.addMenuView()
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
            categoryID = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "category_id") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
            VC.catTitle = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "name") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
            self.navigationController?.pushViewController(VC, animated: true)

        }
        else
        {
            if indexPath.row >= self.cart.count + self.giftTypeArr.count
            {
                
            }
            else if indexPath.row >= self.cart.count
            {
                giftType = "\(giftTypeArr[indexPath.row - self.cart.count] as! String)"
                
                if giftType == "REWARD POINTS"
                {
                    self.viewCoupon.isHidden = false
                    self.viewBlurEff.isHidden = false
                    
                    self.lblGiftTypeTitle.text = "Your Reward Points (Available \(self.rewardStr))"
                    
                    self.txtRewards.isHidden = false
                    self.txtGiftCode.isHidden = true
                    self.txtCoupon.isHidden = true
                    self.txtRewards.placeholder = "Points to use (Max \(self.maxRewardStr))"
                }
                else if giftType == "GIFT VOUCHER"
                {
                    self.viewCoupon.isHidden = false
                    self.viewBlurEff.isHidden = false
                    
                    self.lblGiftTypeTitle.text = "Gift Voucher"
                    self.txtRewards.isHidden = true
                    self.txtGiftCode.isHidden = false
                    self.txtCoupon.isHidden = true
                    self.txtGiftCode.placeholder = "Enter your Gift Voucher code"
                }
                else
                {
                    self.viewCoupon.isHidden = false
                    self.viewBlurEff.isHidden = false
                    
                    self.lblGiftTypeTitle.text = "Coupon"
                    self.txtRewards.isHidden = true
                    self.txtGiftCode.isHidden = true
                    self.txtCoupon.isHidden = false
                    self.txtCoupon.placeholder = "Enter your Coupon code"
                }
            }
            else
            {
                self.performSegue(withIdentifier: "checkoutToDetail", sender: indexPath)
            }
        }
    }
    
    @IBAction func clickClose(_ sender: Any)
    {
       // self.viewWarning.isHidden = true
        self.viewCoupon.isHidden = true
        self.viewBlurEff.isHidden = true
    }
    
    @IBAction func clickApplyCode(_ sender: Any)
    {
        if giftType == "REWARD POINTS"
        {
            if !self.txtRewards.hasText
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "Please Enter Ponits to Apply", viewController: self)
            }
            else
            {
                self.checkVoucher(type: giftType as NSString, code: self.txtRewards.text! as String as NSString)
                self.txtRewards.resignFirstResponder()
            }
        }
        else if giftType == "COUPON"
        {
            if !self.txtCoupon.hasText
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: "Please Enter your Coupon Code to Apply", viewController: self)
            }
            else
            {
                self.checkVoucher(type: giftType as NSString, code: self.txtCoupon.text! as String as NSString)
                self.txtCoupon.resignFirstResponder()
            }
        }
        else
        {
            if !self.txtGiftCode.hasText
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: "Please Enter your Voucher Code to Apply", viewController: self)
            }
            else
            {
                self.checkVoucher(type: giftType as NSString, code: self.txtGiftCode.text! as String as NSString)
                self.txtGiftCode.resignFirstResponder()
            }
        }
    }
    
    func checkVoucher(type: NSString, code: NSString)
    {
        var status = String()
        var message = String()
        
        if type == "COUPON"
        {
            let values:NSString = NSString(format: "coupon=%@&language_id=%@", code,languageID)
            
            BusinessManager.validateCouponWith(values as String, completionHandler: { (result) -> () in
                
                status = result.value(forKeyPath: "status") as! String
                
                message = result.value(forKeyPath: "message") as! String
                
                if status == "200"
                {
                    UserDefaults.standard.set(code, forKey: "COUPON")
                    self.viewBlurEff.isHidden = true
                    self.viewCoupon.isHidden = true
                    
                    SharedManager.showAlertWithMessage(title: "Success", alertMessage: message, viewController: self)
                }
                else
                {
                  //  self.viewWarning.isHidden = false
                  //  self.viewBlurEff.isHidden = false
                    
                    UserDefaults.standard.set("", forKey: "COUPON")
                    SharedManager.showAlertWithMessage(title: "Warning!", alertMessage: "1. Please Login & try\n2. Verify Coupon Code is correct\n3. Order Total matches allowed Coupon Value\n4. Contact Us for Help", viewController: self)
                }
                
                self.loadData()
            })
        }
        else if type == "GIFT VOUCHER"
        {
            let values:NSString = NSString(format: "voucher=%@&language_id=%@", code,languageID)
            
            
            BusinessManager.validateVoucherWith(values as String, completionHandler: { (result) -> () in
                
                status = result.value(forKey: "status") as! String
                
                message = result.value(forKey: "message") as! String
                
                if status == "200"
                {
                    UserDefaults.standard.set(code, forKey: "VOUCHER")
                    SharedManager.showAlertWithMessage(title: "Success", alertMessage: message, viewController: self)
                }
                else
                {
                    
                    
                    UserDefaults.standard.set("", forKey: "VOUCHER")
                    SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: message, viewController: self)
                }
                self.loadData()
                
                self.viewBlurEff.isHidden = true
                self.viewCoupon.isHidden = true
            })
        }
        else
        {
            let totalRewards = Int(rewardStr)
            let maxRewards = Int(maxRewardStr)
            let rewardstoUse = Int(code as String)
            
            if rewardstoUse > totalRewards
            {
                UserDefaults.standard.set("", forKey: "REWARDS")
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "You have only \(rewardStr) Reward Points", viewController: self)
            }
            else
            {
                if rewardstoUse > maxRewards
                {
                    UserDefaults.standard.set("", forKey: "REWARDS")
                    SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "You can use maximum \(maxRewardStr) points to this/these Product/s", viewController: self)
                }
                else
                {
                    UserDefaults.standard.set(code, forKey: "REWARDS")
                    
                    self.viewBlurEff.isHidden = true
                    self.viewCoupon.isHidden = true
                }
            }
            self.loadData()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "checkoutToDetail"
        {
            let productDetailObj = segue.destination as! ProductDetailsVC
            productDetailObj.productID = (self.checkOutAry.object(at: (sender! as AnyObject).row) as AnyObject).value(forKey: "product_id") as! String
        }
    }
}
