//
//  CheckOutStep4TVCell.swift
//  Exlcart
//
//  Created by iPhone on 29/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class CheckOutStep4TVCell: UITableViewCell {

    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var lblGst: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
