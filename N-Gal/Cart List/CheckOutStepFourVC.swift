//
//  CheckOutStepFourVC.swift
//  Exlcart
//
//  Created by Tech Basics on 14/12/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

@objc class CheckOutStepFourVC: ParentViewController {

   // var paymentInfo:NSMutableArray = []
    var shippingDetails:NSString = ""
    @IBOutlet var paymentTypeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        
        let barView = UIView()
        barView.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        
        let img = UIImageView()
        img.image = UIImage (named: "right-arrow.png")
        img.frame = CGRect(x: 0, y: 10, width: 15, height: 20)
        img.contentMode = .scaleAspectFit
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.black, for: .normal)
        myBackButton.sizeToFit()
        myBackButton.frame = CGRect(x: 12, y: 5, width: 55, height: 30)
        
        barView.addSubview(img)
        barView.addSubview(myBackButton)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: barView)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
       /* if paymentInfo.count == 0
        {
            SharedManager.showHUD(viewController: self)
            BusinessManager.GetPaymentMethod(shippingDetails as String, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    paymentInfo = result.object(forKey: "payment_method") as! NSMutableArray
                    
                    UserDefaults.standard.set(result, forKey: "Payment Method")
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }*/
        
     var yValue:CGFloat = 40
        var index:Int = 0
        
        for temp in paymentInfo
        {
            let title = (temp as AnyObject).object(forKey: "code") as? String
            
           /* if title?.lowercased() == "ccavenuepay"
            {
                paymentInfo.remove(temp)
            }*/
            if title == "payu"
            {
                paymentInfo.remove(temp)
               // break
            }
        }
        
        for tempDic in paymentInfo
        {
            let button = UIButton(type: UIButtonType.custom)
            button.frame = CGRect(x: 10, y: yValue, width: 21, height: 21)
            if(index == 0)
            {
                button.setImage(UIImage(named: "ico-tick"), for:UIControlState())
            }
            else
            {
                button.setImage(UIImage(named: "ico-rnd1"), for:UIControlState())
            }
            button.tag = index
            button.addTarget(self, action: #selector(CheckOutStepFourVC.chooseOption(_:)), for: UIControlEvents.touchUpInside)
            paymentTypeView.addSubview(button)
           
            let label = UILabel(frame: CGRect(x: 40, y: yValue, width: UIScreen.main.bounds.width - 65, height: 20))
            
            label.text = (tempDic as AnyObject).object(forKey: "title") as? String
            label.font = UIFont.systemFont(ofSize: 13)
           
            if (tempDic as AnyObject).object(forKey: "code") as? String == "ccavenuepay" || label.text == "Ccavenuepay"
            {
                label.text = "Online Payment"
            }
            
            if label.text == "cod" || label.text == "Cod"
            {
                label.text = "Cash on Delivery"
            }
            
            paymentTypeView.addSubview(label)
            yValue = yValue + 30
            index += 1
        }

        if(paymentInfo.count > 0)
        {
            paymentResultDict = paymentInfo.object(at: 0) as! NSMutableDictionary
        }
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "CheckOutStepThreeVC") as! CheckOutStepThreeVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func chooseOption(_ sender : UIButton)
    {
        for subviews in paymentTypeView.subviews
        {
            if (subviews.isKind(of: UIButton.self))
            {
                let button = subviews as? UIButton
                if (button == sender)
                {
                    sender.setImage(UIImage(named: "ico-tick"), for: UIControlState())
                }
                else
                {
                    button!.setImage(UIImage(named: "ico-rnd1"), for:UIControlState())
                }
            }
        }
        paymentResultDict = paymentInfo.object(at: sender.tag) as! NSMutableDictionary
    }

    
    @IBAction func continueBtnClick(_ sender: AnyObject) {
        

        self.performSegue(withIdentifier: "toCheckOutStep4", sender: nil)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "toCheckOutStep4") {
            
            let produObj = segue.destination as! CheckOutStep4VC
            
        }
    }
    
}
