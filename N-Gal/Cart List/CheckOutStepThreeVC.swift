//
//  CheckOutStepThreeVC.swift
//  Exlcart
//
//  Created by Tech Basics on 14/12/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class CheckOutStepThreeVC: UIViewController {

    @IBOutlet var shippingTypeView: UIView!
    var paymentinfoDic:NSMutableDictionary = [:]
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        
        let barView = UIView()
        barView.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        
        let img = UIImageView()
        img.image = UIImage (named: "right-arrow.png")
        img.frame = CGRect(x: 0, y: 10, width: 15, height: 20)
        img.contentMode = .scaleAspectFit
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.black, for: .normal)
        myBackButton.sizeToFit()
        myBackButton.frame = CGRect(x: 12, y: 5, width: 55, height: 30)
        
        barView.addSubview(img)
        barView.addSubview(myBackButton)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: barView)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
   
        var yValue:CGFloat = 40
        var index:Int = 0
        for tempDic in shippingInfo
        {
            
            let button = UIButton(type: UIButtonType.custom)
            button.frame = CGRect(x: 10, y: yValue, width: 21, height: 21)
            if(index == 0)
            {
                button.setImage(UIImage(named: "ico-tick"), for:UIControlState())
            }
            else
            {
                button.setImage(UIImage(named: "ico-rnd1"), for:UIControlState())
            }
            
            
            button.tag = index
            button.addTarget(self, action: #selector(CheckOutStepThreeVC.chooseOption(_:)), for: UIControlEvents.touchUpInside)
            shippingTypeView.addSubview(button)
            
            let quoteAry = ((tempDic as AnyObject).object(forKey: "quote") as? NSArray)
            
            
            let label = UILabel(frame: CGRect(x: 40, y: yValue, width: UIScreen.main.bounds.width - 65, height: 20))
           // label.text = ((tempDic as AnyObject).object(forKey: "title") as? String)?.capitalized
            label.font = UIFont.systemFont(ofSize: 13)
            for tempAry in quoteAry!{
                label.text = ((tempAry as AnyObject).object(forKey: "title") as? String)?.capitalized
            }
            shippingTypeView.addSubview(label)
            yValue = yValue + 30
            index += 1
            
        }

        if(shippingInfo.count > 0)
        {
            shippingResultDict = shippingInfo.object(at: 0) as! NSMutableDictionary
        }
        else
        {
            
        }
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "CheckOutStep2VC") as! CheckOutStep2VC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        
    }
        
    func chooseOption(_ sender : UIButton)
    {
        for subviews in shippingTypeView.subviews
        {
            if (subviews.isKind(of: UIButton.self))
            {
                let button = subviews as? UIButton
                if (button == sender)
                {
                    sender.setImage(UIImage(named: "ico-tick"), for: UIControlState())
                }
                else
                {
                    button!.setImage(UIImage(named: "ico-rnd1"), for:UIControlState())
                }
            }
        }
        
        shippingResultDict = shippingInfo.object(at: sender.tag) as! NSMutableDictionary
        
    }
    
    @IBAction func continueBtnClick(_ sender: AnyObject) {
        
        SharedManager.showHUD(viewController: self)
        BusinessManager.GetPaymentMethod(shipingAddressJsonStr as String, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            let code = result.value(forKey: "status") as! String
            
            if code == "200"
            {
                
                self.paymentinfoDic = result as! NSMutableDictionary
                
                self.performSegue(withIdentifier: "checkOutFourSegue", sender: shipingAddressJsonStr)

              //  var resultDic:NSMutableArray = result.objectForKey("payment_method") as! NSMutableArray
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
        })
        
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "checkOutFourSegue")
        {
    
            let produObj = segue.destination as! CheckOutStepFourVC
            produObj.shippingDetails = shipingAddressJsonStr
        
            paymentInfo = paymentinfoDic.object(forKey: "payment_method") as! NSMutableArray
        
            UserDefaults.standard.set(self.paymentinfoDic, forKey: "Payment Method")
        }
    }
}
