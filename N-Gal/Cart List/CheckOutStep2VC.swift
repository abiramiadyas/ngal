//
//  CheckOutStep2VC.swift
//  Exlcart
//
//  Created by iPhone on 29/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

let kProfileInfo:String = "_kPROFILE_INFO"

import UIKit

var paymentDict:NSMutableDictionary = NSMutableDictionary()
var shippingDict:NSMutableDictionary = NSMutableDictionary()
var shippingResultDict:NSMutableDictionary = NSMutableDictionary()
var productTempDict:NSMutableDictionary = NSMutableDictionary()
var productTempArry:NSMutableArray = NSMutableArray()
var paymentResultDict:NSMutableDictionary = NSMutableDictionary()
var orderID:String = ""

class CheckOutStep2VC: ParentViewController, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    @IBOutlet weak var proceedToCheckOutBtn: UIButton!
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var shipFnameTxt: UITextField!
    @IBOutlet weak var shipLnameTxt: UITextField!
    @IBOutlet weak var shipPhoneNumberTxt: UITextField!
    @IBOutlet weak var shipStreetTxt: UITextField!
    @IBOutlet weak var shipCityTxt: UITextField!
    @IBOutlet weak var shipZipCodeTxt: UITextField!
    @IBOutlet weak var shipStateTxt: UITextField!
    @IBOutlet weak var shipCountryTxt: UITextField!
    @IBOutlet weak var editInfoView: UIView!
    @IBOutlet weak var fnameTxt: UITextField!
    @IBOutlet weak var lNameTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var streetTxt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    
    var isClickShippingCountryBtn = false
    var isChangeBillingAddress = false
    
    var profileDetailsAry:NSMutableArray = []
    var shippingTypeAry:NSMutableArray = []
    var shippingCountryID:NSString = ""
    
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var picker: UIPickerView!
    var selectedIndex = Int()
    var pickerType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        
        let barView = UIView()
        barView.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        
        let img = UIImageView()
        img.image = UIImage (named: "right-arrow.png")
        img.frame = CGRect(x: 0, y: 10, width: 15, height: 20)
        img.contentMode = .scaleAspectFit
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.black, for: .normal)
        myBackButton.sizeToFit()
        myBackButton.frame = CGRect(x: 12, y: 5, width: 55, height: 30)
        
        barView.addSubview(img)
        barView.addSubview(myBackButton)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: barView)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        self.viewPicker.isHidden = true
        editInfoView.isHidden = true
        mainScroll.isHidden = false
        self.proceedToCheckOutBtn.isHidden = false
        mainScroll.autoresizesSubviews = true
        self.view .bringSubview(toFront: proceedToCheckOutBtn)
        
           NotificationCenter.default.addObserver(self, selector: #selector(CheckOutStep2VC.refreshList(notification:)), name: NSNotification.Name(rawValue: "getName"), object: nil)
        
        let detail = UserDefaults.standard.object(forKey: kUserDetails)
        
        if  UserDefaults.standard.object(forKey: kUserDetails) != nil
        {
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let array:NSMutableDictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableDictionary
            userIDStr = array.value(forKeyPath: "customer.customer_id") as! String
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.getProfileInformation(userIDStr, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            self.profileDetailsAry = result.value(forKey: "customer_address") as! NSMutableArray

            self.shipFnameTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "firstname") as? String
            self.shipLnameTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "lastname") as? String
            let mobile = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "telephone") as? String
            self.shipPhoneNumberTxt.text = NSString(format: "%@", mobile!) as String
                
            self.shipCityTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "city") as? String
            self.shipStreetTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_1") as? String
            self.shipStateTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "state") as? String
            self.shipZipCodeTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "postcode") as? String
            self.shipCountryTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "country") as? String
            countryID = (self.profileDetailsAry.object(at: 0) as! NSObject).value(forKey: "country_id") as! NSString
            self.shippingCountryID = ((self.profileDetailsAry.object(at: 0) as! NSObject).value(forKey: "country_id") as! NSString)
                let data = NSKeyedArchiver.archivedData(withRootObject: self.profileDetailsAry)
                UserDefaults.standard.set(data, forKey:kProfileInfo)
            })
        }
        else
        {
            let data = NSKeyedArchiver.archivedData(withRootObject: self.profileDetailsAry)
            UserDefaults.standard.set(data, forKey:kProfileInfo)
        }
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func proceedToCheckOutAction(_ sender: AnyObject)
    {
        let value = UserDefaults.standard.object(forKey: kProfileInfo)        
        if(isChangeBillingAddress)
        {
            if  UserDefaults.standard.object(forKey: kProfileInfo) != nil
            {
                
                let data = UserDefaults.standard.object(forKey: kProfileInfo) as! Data
                let profileDetailsAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            var value:String = String()
            value = NSString(format: "address_id=%@&firstname=%@&lastname=%@&address_1=%@&city=%@&postcode=%@&country=%@&state=%@",((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_id") as? String)!,self.fnameTxt.text!,self.lNameTxt.text!,self.streetTxt.text!,self.cityTxt.text!,self.zipCodeTxt.text!,countryID,stateTxt.text!) as String
            BusinessManager.updateProfileInformationWith(value, completionHandler: { (result) -> () in
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    self.proceedToCheckOut()
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
                DispatchQueue.main.async
                    {
                }
                
            })

        }
        }
        else
        {
            self.proceedToCheckOut()
        }
    }
    
    func proceedToCheckOut() {
        
        let kProfile = UserDefaults.standard.object(forKey: kProfileInfo)
        if  UserDefaults.standard.object(forKey: kProfileInfo) != nil
        {
        let data = UserDefaults.standard.object(forKey: kProfileInfo) as! Data
        let profileDetailsAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
        paymentDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "customer_id") as! String, forKey: "customer_id" as NSCopying)
        paymentDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_id") as! String, forKey: "address_id" as NSCopying)
        paymentDict.setObject(shipFnameTxt.text!, forKey: "payment_firstname" as NSCopying)
        paymentDict.setObject(shipLnameTxt.text!, forKey: "payment_lastname" as NSCopying)
        paymentDict.setObject(shipStreetTxt.text!, forKey: "payment_address_1" as NSCopying)
        paymentDict.setObject(shipStreetTxt.text!, forKey: "payment_address_2" as NSCopying)
        paymentDict.setObject(shipCityTxt.text!, forKey: "payment_city" as NSCopying)
        paymentDict.setObject(shipZipCodeTxt.text!, forKey: "payment_postcode" as NSCopying)
        paymentDict.setObject(shipCountryTxt.text!, forKey: "payment_country" as NSCopying)
        paymentDict.setObject(shippingCountryID as String, forKey: "payment_country_id" as NSCopying)
        paymentDict.setObject(shipStateTxt.text!, forKey: "payment_zone" as NSCopying)
        paymentDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "zone_id") as! String, forKey: "payment_zone_id" as NSCopying)
        paymentDict.setObject(shipPhoneNumberTxt.text!, forKey: "payment_telephone" as NSCopying)
        paymentDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "email") as! String, forKey: "payment_email" as NSCopying)
        
        shippingDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_id") as! String, forKey: "address_id" as NSCopying)
        shippingDict.setObject(shipFnameTxt.text!, forKey: "shipping_firstname" as NSCopying)
        shippingDict.setObject(shipLnameTxt.text!, forKey: "shipping_lastname" as NSCopying)
        shippingDict.setObject(shipStreetTxt.text!, forKey: "shipping_address_1" as NSCopying)
        shippingDict.setObject(shipStreetTxt.text!, forKey: "shipping_address_2" as NSCopying)
        shippingDict.setObject(shipCityTxt.text!, forKey: "shipping_city" as NSCopying)
        shippingDict.setObject(shipZipCodeTxt.text!, forKey: "shipping_postcode" as NSCopying)
        shippingDict.setObject(shipCountryTxt.text!, forKey: "shipping_country" as NSCopying)
        shippingDict.setObject(shippingCountryID as String, forKey: "shipping_country_id" as NSCopying)
        shippingDict.setObject(shipStateTxt.text!, forKey: "shipping_zone" as NSCopying)
        shippingDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "zone_id") as! String, forKey: "shipping_zone_id" as NSCopying)
        shippingDict.setObject(shipPhoneNumberTxt.text!, forKey: "shipping_telephone" as NSCopying)
        shippingDict.setObject((profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "email") as! String, forKey: "shipping_email" as NSCopying)
        }
        
        var cartAry:NSMutableArray  = NSMutableArray()
        if var data = UserDefaults.standard.object(forKey: kAddToCart) as? Data
        {
            data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            cartAry = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
        }
        let tempOptionDict:NSMutableDictionary = NSMutableDictionary()
        tempOptionDict.setObject("5", forKey: "215" as NSCopying)
        tempOptionDict.setObject("4", forKey: "218" as NSCopying)
        
        productTempArry = NSMutableArray()
        
        for tempDic in cartAry
        {
            let tempCart:NSDictionary = tempDic as! NSDictionary
            
            let str1 = (tempDic as AnyObject).value(forKey: "product_id") as! NSString
            let str2 = (tempDic as AnyObject).value(forKey: "userQuantity") as! NSString
            
            productTempDict = NSMutableDictionary()
            productTempDict.setObject(str1, forKey: "product_id" as NSCopying)
            productTempDict.setObject(str2, forKey: "quantity" as NSCopying)
            let allKeys:Array = tempCart.allKeys
            
            if (allKeys as NSArray).contains("product_option_selection_value") {
                let dic1 = (tempDic as AnyObject).value(forKey: "product_option_selection_value") as! NSDictionary
                productTempDict.setObject(dic1, forKey: "option" as NSCopying)
            }
            else
            {
                productTempDict.setObject("", forKey: "option" as NSCopying)
            }
            
            productTempArry.add(productTempDict)
        }
        
        
        let mainDict:NSMutableDictionary = NSMutableDictionary()
        
        mainDict.setObject(productTempArry, forKey: "products" as NSCopying)
        mainDict.setObject(languageID, forKey: "language_id" as NSCopying)
        mainDict.setObject(cartTotalAmount, forKey: "cart_total_amount" as NSCopying)
        mainDict.setObject(userIDStr, forKey: "customer_id" as NSCopying)
        mainDict.setObject(paymentDict, forKey: "payment_address" as NSCopying)
        mainDict.setObject(shippingDict, forKey: "shipping_address" as NSCopying)
        
        let theJSONData = try? JSONSerialization.data(
            withJSONObject: mainDict ,
            options: JSONSerialization.WritingOptions(rawValue: 0))
        let theJSONText = NSString(data: theJSONData!,
            encoding: String.Encoding.utf8.rawValue)
        
        SharedManager.showHUD(viewController: self)
        BusinessManager.GetShippingMethod(theJSONText as! String, completionHandler: { (result) -> () in
        SharedManager.dismissHUD(viewController: self)
            
            let code = result.value(forKey: "status") as! String
            
            if code == "200"
            {
                let resultDic:NSMutableArray = result.object(forKey: "shipping_method") as! NSMutableArray
                self.shippingTypeAry = resultDic
                
                self.performSegue(withIdentifier: "checkOutThreeSegue", sender: theJSONText)
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
            
        })

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "checkOutThreeSegue")
        {
            let produObj = segue.destination as! CheckOutStepThreeVC
            shippingInfo = self.shippingTypeAry
            shipingAddressJsonStr = (sender as? String)! as NSString
        }
    }
    
    func choosePaymentMethod(_ values:String)
    {
        BusinessManager.GetPaymentMethod(values as String, completionHandler: { (result) -> () in
            
            let code = result.value(forKey: "status") as! String
            if code == "200"
            {
                paymentResultDict = result as! NSMutableDictionary
                
                let resultDic:NSMutableArray = result.object(forKey: "payment_method") as! NSMutableArray
                self.performSegue(withIdentifier: "CheckOutStep3Segue", sender: nil)
                
                let alert = UIAlertController(title: "Payment Type", message: "Choose a method for payment", preferredStyle: UIAlertControllerStyle.alert)
                for obj in resultDic
                {
                    let object:NSDictionary = obj as! NSDictionary
                    alert.addAction(UIAlertAction(title: object.value(forKey: "title") as? String, style: UIAlertActionStyle.default, handler:    {(alert :UIAlertAction) in
                        paymentResultDict = object as! NSMutableDictionary
                        self.confirmOrder()
                    }))
                    
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
            
        })
    }
    
    func confirmOrder()
    {
        let mainDict:NSMutableDictionary = NSMutableDictionary()
        
        mainDict.setObject(productTempArry, forKey: "products" as NSCopying)
        mainDict.setObject(languageID, forKey: "language_id" as NSCopying)
        mainDict.setObject(paymentDict, forKey: "payment_address" as NSCopying)
        mainDict.setObject(shippingDict, forKey: "shipping_address" as NSCopying)
        mainDict.setObject("", forKey: "coupon" as NSCopying)
        mainDict.setObject("", forKey: "voucher" as NSCopying)
        mainDict.setObject(userIDStr, forKey: "customer_id" as NSCopying)
        mainDict.setObject(paymentResultDict, forKey: "payment_method" as NSCopying)
        mainDict.setObject((shippingResultDict.object(forKey: "quote") as AnyObject).object(at: 0) as! NSDictionary, forKey: "shipping_method" as NSCopying)
      
        let theJSONData = try? JSONSerialization.data(
            withJSONObject: mainDict ,
            options: JSONSerialization.WritingOptions(rawValue: 0))
        let theJSONText = NSString(data: theJSONData!,
            encoding: String.Encoding.utf8.rawValue)
        
        BusinessManager.confirmOrderWith(theJSONText as! String, completionHandler: { (result) -> () in
            
            let code = result.value(forKey: "status") as! String
            if code == "200"
            {
                let tempOrderID = result.value(forKeyPath: "order_info.order_id") as! Int
                orderID = NSString(format: "%d",tempOrderID ) as String
                self.performSegue(withIdentifier: "toCheckOutStep4", sender: nil)
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
        })
    }
    
   
    @IBAction func editAction(_ sender: AnyObject)
    {
        self.mainScroll.isHidden = true
        self.editInfoView.isHidden = false
        self.proceedToCheckOutBtn.isHidden = true
        
        self.fnameTxt.text = shipFnameTxt.text
        self.lNameTxt.text = shipLnameTxt.text
        self.cityTxt.text = shipCityTxt.text
        self.streetTxt.text = shipStreetTxt.text
        self.stateTxt.text = shipStateTxt.text
        self.countryTxt.text = shipCountryTxt.text
        self.zipCodeTxt.text = shipZipCodeTxt.text
    }
    
    @IBAction func shippingCountryAction(_ sender: AnyObject) {
        
        isClickShippingCountryBtn = true
        stateName = ""
        
        self.view.endEditing(true)
        
        var countryNames = [String]()
        let country_id = NSMutableArray()
        
        for i in 0 ..< self.countryListAry.count
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: i)
            countryNames.append(name as! String)
            country_id.add((self.countryListAry.value(forKey: "country_id") as! NSArray).object(at: i) as! String)
        }
        pickerType = "country"
        
        self.viewPicker.isHidden = false
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    @IBAction func shippingStateAction(_ sender: AnyObject)
    {
        // self.showStateList()
        self.view.endEditing(true)
        
        for i : Int in 0 ..< self.countryListAry.count
        {
            let id:String = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "country_id") as? String)!
            
            if countryID.isEqual(to: id as String)
            {
                self.stateAryList = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "state")! as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        pickerType = "state"
        
        self.viewPicker.isHidden = false
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    func refreshList(notification: NSNotification)
    {
        self.countryTxt.text = (countryName as NSString) as String
        self.stateTxt.text = (stateName as NSString) as String
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        
        if(textField == shipFnameTxt || textField == shipLnameTxt || textField == shipPhoneNumberTxt  || textField == shipStreetTxt || textField == shipCityTxt || textField == shipZipCodeTxt || textField == shipStateTxt)
        {
             isChangeBillingAddress = true
        }
        
        return true
    }
    
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        
        self.editInfoView.isHidden = true
        self.mainScroll.isHidden = false
        self.proceedToCheckOutBtn.isHidden = false
    }

    @IBAction func continueAction(_ sender: AnyObject) {
        
        if(self.fnameTxt.hasText && self.lNameTxt.hasText  && self.cityTxt.hasText && self.streetTxt.hasText && self.stateTxt.hasText && self.zipCodeTxt.hasText)
        {
            
            var value:String = String()
            value = NSString(format: "address_id=%@&customer_id=%@&firstname=%@&lastname=%@&address_1=%@&city=%@&postcode=%@&country=%@&state=%@&country_id=%@&language_id=%@",((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_id") as? String)!,userIDStr,self.fnameTxt.text!,self.lNameTxt.text!,self.streetTxt.text!,self.cityTxt.text!,self.zipCodeTxt.text!,countryID,stateID,countryID,languageID) as String
            
                SharedManager.showHUD(viewController: self)
            BusinessManager.updateProfileInformationWith(value, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    let alert = UIAlertController(title: "Information", message: result.value(forKeyPath: "Message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) -> Void in
                        self.viewDidLoad()
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kEmptyTextFieldAlertMessage, viewController: self)

        }

    }
    
    //MARK: PickerView Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerType == "country"
        {
            return countryListAry.count
        }
        else
        {
            return stateAryList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerType == "country"
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        else
        {
            let name = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedIndex = row
    }
    
    
    @IBAction func clickPickerCancel(_ sender: Any)
    {
        self.viewPicker.isHidden = true
    }
    
    @IBAction func clickPickerDone(_ sender: Any)
    {
        if pickerType == "country"
        {
            countryTxt.text = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            countryName = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            countryID = (self.countryListAry.value(forKey: "country_id") as AnyObject).object(at: selectedIndex) as! NSString
            shippingCountryID = (self.countryListAry.value(forKey: "country_id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        else
        {
            stateTxt.text = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            stateName = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            stateID = (self.stateAryList.value(forKey: "state_id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        
        self.viewPicker.isHidden = true
    }
}
