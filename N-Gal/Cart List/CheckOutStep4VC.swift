//
//  CheckOutStep4VC.swift
//  Exlcart
//
//  Created by iPhone on 29/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit
var price:String?
class CheckOutStep4VC: ParentViewController
{
    @IBOutlet weak var imgTerms: UIImageView!
    @IBOutlet weak var shippingCostLbl: UILabel!
    @IBOutlet weak var productTable: UITableView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var shippingAddressLbl: UILabel!
    @IBOutlet weak var shippingView: UIView!

    @IBOutlet weak var viewTerms: UIView!
    @IBOutlet weak var changeBtn: UIButton!
    var shipingAddressJsonStr:NSString = ""
    var paymentinfoDic:NSMutableDictionary = [:]
    var paymentInfo:NSMutableArray = []

    @IBOutlet weak var totalScrollView: UIScrollView!
    var checkOutAry:NSArray = []
    var totalPrice:Float = 0.00
    
    var isAccepted : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        
        scroll.autoresizesSubviews = true
        let mainDict:NSMutableDictionary = NSMutableDictionary()
        
        imgTerms.image = UIImage.init(named: "ico-rnd1.png")
        
      //  "sort_order":shippingResultDict.object(forKey: "sort_order") as! NSArray?        
        
        let dict = ["code":((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "code") as! String,"cost":"\(String(describing: ((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "cost")))","tax_class_id":((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "tax_class_id") as! NSString,"text":((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "text") as! String,"title":((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "title") as! String] as [String : Any]
        
        mainDict.setObject(productTempArry, forKey: "products" as NSCopying)
        mainDict.setObject(languageID, forKey: "language_id" as NSCopying)
        mainDict.setObject(paymentDict, forKey: "payment_address" as NSCopying)
        mainDict.setObject(shippingDict, forKey: "shipping_address" as NSCopying)
        mainDict.setObject("\(String(describing: UserDefaults.standard.string(forKey: "COUPON")!))", forKey: "coupon" as NSCopying)
        mainDict.setObject("\(String(describing: UserDefaults.standard.string(forKey: "VOUCHER")!))", forKey: "voucher" as NSCopying)
        mainDict.setObject("\(String(describing: UserDefaults.standard.string(forKey: "REWARDS")!))", forKey: "reward" as NSCopying)
        mainDict.setObject(userIDStr, forKey: "customer_id" as NSCopying)
        mainDict.setObject(cartTotalAmount, forKey: "cart_total_amount" as NSCopying)
        mainDict.setObject(paymentResultDict, forKey: "payment_method" as NSCopying)
        mainDict.setObject(dict, forKey: "shipping_method" as NSCopying)
        
        let theJSONData = try? JSONSerialization.data(withJSONObject: mainDict ,  options: JSONSerialization.WritingOptions(rawValue: 0))
        let theJSONText = NSString(data: theJSONData!, encoding: String.Encoding.utf8.rawValue)
            SharedManager.showHUD(viewController: self)
                
        BusinessManager.confirmOrderWith(theJSONText! as String, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            let code = result.value(forKey: "status") as! String
            if code == "200"
            {
                let tempOrderID = result.value(forKeyPath: "order_info.order_id") as! Int
                orderID = NSString(format: "%d",tempOrderID ) as String
                
                self.totalPrice = result.value(forKeyPath: "order_info.total") as! Float
                price = "\(self.totalPrice)"
                
                let totalsArray = result.value(forKeyPath: "order_info.totals") as! NSArray
                let totalView:UIView = UIView()
                
                var y:CGFloat = 5.0
                for temp in totalsArray
                {
                    let nameLbl:UILabel = UILabel(frame: CGRect(x: 5,y: y,width: 220,height: 20))
                    nameLbl.text = ((temp as AnyObject).object(forKey: "title") as? String)
                    nameLbl.font = UIFont.systemFont(ofSize: 13)
                    nameLbl.textAlignment = .left
                    
                    let colonLbl:UILabel = UILabel(frame: CGRect(x: nameLbl.frame.size.width+15, y: y, width: 200, height: 25))
                    colonLbl.font = UIFont.systemFont(ofSize: 13)
                    colonLbl.text = ":"
                    
                    let textLbl:UILabel = UILabel(frame: CGRect(x:  nameLbl.frame.size.width+45, y: y, width: 205, height: 20))
                    textLbl.font = UIFont.systemFont(ofSize: 13)
                    textLbl.text = (temp as AnyObject).object(forKey: "value") as? String
                    textLbl.textAlignment = .left
                    
                    y = y + max(nameLbl.frame.size.height, textLbl.frame.size.height) + 10
                    totalView.addSubview(nameLbl)
                    totalView.addSubview(colonLbl)
                    totalView.addSubview(textLbl)
                }
                totalView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: y)
                
                totalView.backgroundColor = UIColor.white
                self.totalScrollView.addSubview(totalView)
                
                self.totalScrollView.contentSize = CGSize(width: 300, height: 150)
                self.autoAdjustFrame()
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
        })
        
        self.productTable.tableFooterView = UIView()
        if UserDefaults.standard.object(forKey: "cart_array") != nil{
            let data = UserDefaults.standard.object(forKey: "cart_array") as! Data
            checkOutAry = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
        }
        shippingAddressLbl.text = NSString(format: "%@ %@ %@ %@ %@ \n%@", shippingDict.object(forKey: "shipping_firstname") as! String, shippingDict.object(forKey: "shipping_address_1") as! String, shippingDict.object(forKey: "shipping_city") as! String, shippingDict.object(forKey: "shipping_zone") as! String,shippingDict.object(forKey: "shipping_country") as! String, shippingDict.object(forKey: "shipping_telephone") as! String) as String
        
        if let str = ((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).value(forKeyPath: "cost") as? String
        {
            let shippingAmt:String = (((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "cost") as? String)!
            let ratingNo = (shippingAmt as NSString).floatValue
            
            totalPrice = totalPrice + ratingNo
        }
        else
        {
            let shippingAmt:Int = (((shippingResultDict.object(forKey: "quote") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "cost") as? Int)!
            
            totalPrice = totalPrice + Float(shippingAmt)
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
    }
    
    override func autoAdjustFrame()
    {
        var frame: CGRect = self.productTable.frame
        frame.size.height = self.productTable.contentSize.height
        self.productTable.frame = frame
        
        frame = self.totalScrollView.frame
        frame.size.height = self.totalScrollView.contentSize.height
        frame.origin.y = self.productTable.frame.origin.y+self.productTable.frame.size.height+15
        self.totalScrollView.frame = frame
        
        frame = self.viewTerms.frame
        frame.origin.y = self.totalScrollView.frame.origin.y+self.totalScrollView.frame.size.height+15
        self.viewTerms.frame = frame
        
        frame = self.changeBtn.frame
        frame.origin.y = self.viewTerms.frame.origin.y+self.viewTerms.frame.size.height+15
        self.changeBtn.frame = frame
        
        var height:CGFloat = 0.0
        
        height = shippingView.frame.size.height + productTable.contentSize.height + totalScrollView.contentSize.height + self.viewTerms.frame.size.height + changeBtn.frame.size.height + 200
        
        scroll.contentSize = CGSize(width: scroll.frame.size.width, height: height)
    }

    @IBAction func changeAddressAction(_ sender: AnyObject)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKind(of: CheckOutStep2VC.self)
            {
               _ = self.navigationController?.popToViewController(controller , animated: true)
                break
            }
        }
    }
    
    @IBAction func clickAccepTterms(_ sender: AnyObject)
    {
        if isAccepted == true
        {
            isAccepted = false
            imgTerms.image = UIImage.init(named: "ico-rnd1.png")
        }
        else
        {
            isAccepted = true
            imgTerms.image = UIImage.init(named: "ico-tick.png")
        }
    }
    
    @IBAction func changeOrderAction(_ sender: AnyObject)
    {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func placeOrderAction(_ sender: AnyObject)
    {
        if isAccepted == false
        {
            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "You must agree to Delivery and Returns Policy", viewController: self)
        }
        else
        {
            let valuesDict:NSMutableDictionary = NSMutableDictionary()
            valuesDict.setObject(orderID, forKey: "order_id" as NSCopying)
            valuesDict.setObject("1", forKey: "order_status_id" as NSCopying)
            
            let str:NSString = paymentResultDict.object(forKey: "code") as! NSString
            
            if !str.isEqual(to: "ccavenuepay")
            {
                let theJSONData = try? JSONSerialization.data(withJSONObject: valuesDict, options: JSONSerialization.WritingOptions(rawValue: 0))
                let theJSONText = NSString(data: theJSONData!, encoding: String.Encoding.utf8.rawValue)
                
                SharedManager.showHUD(viewController: self)
                BusinessManager.placeOrderWith(theJSONText! as String, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    let code = result.value(forKey: "status") as! String
                    if code == "200"
                    {                        
                        UserDefaults.standard.set("Transaction Successful", forKey: "TRANS_STATUS")
                        
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessViewController") as! ResultSuccessViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                        
                      /*  let alert = UIAlertController(title: "Information", message: result.value(forKeyPath: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(alert :UIAlertAction) in
                            UserDefaults.standard.removeObject(forKey: kAddToCart)
                            UserDefaults.standard.removeObject(forKey: "COUPON")
                            UserDefaults.standard.removeObject(forKey: "VOUCHER")
                            UserDefaults.standard.removeObject(forKey: "REWARDS")
                            
                            paymentDict = [:]
                            shippingDict = [:]
                            shippingResultDict = [:]
                            productTempDict = [:]
                            productTempArry.removeAllObjects()
                            orderID = ""
                            self.gotoRootViewController()
                            self.updateCartBadge()
                        }))
                        self.present(alert, animated: true, completion: nil)*/
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                    }
                })
            }
            else
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentWebViewController") as! PaymentWebViewController
                
                self.navigationController?.pushViewController(viewController, animated: true)
                
              /*  let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AddressVC") as! AddressViewController
                viewController.amount = totalPrice
                viewController.shippingDict = shippingDict
                self.navigationController?.pushViewController(viewController, animated: true)*/
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.categoryListTbl
        {
            return self.categoryListAry.count
        }
        else
        {
            return self.checkOutAry.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.categoryListTbl
        {
            let cell:UITableViewCell = self.categoryListTbl.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
            cell.textLabel!.text = (self.categoryListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            cell.textLabel?.font = UIFont(name: "Arial", size: 14)
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell
        }
        else
        {
            let cell:CheckOutStep4TVCell = self.productTable.dequeueReusableCell(withIdentifier: "cell") as! CheckOutStep4TVCell
            cell.titleLbl.text = (checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            let quantity = NSString(format: "%@",(checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "quantity") as! String) as String
            
            let price = "\(quantity) x \(String(describing: (checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "price") as! String))"
            cell.priceLbl.text = price
            
            let igst = "\(String(describing: (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "igstrate")!))"
            if igst == "0"
            {
                let cgst = "\(String(describing: (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "cgstrate")!))"
                let sgst = "\(String(describing: (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "sgstrate")!))"
                
                let cgstAmount = "\(String(describing: (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "cgst")!))"
                let sgstAmount = "\(String(describing: (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "sgst")!))"
                
                cell.lblGst.text = "CGST (\(String(describing: cgst))%) : \(String(describing: cgstAmount)) \nSGST (\(String(describing: sgst))%) : \(String(describing: sgstAmount))"
            }
            else
            {
                let igstAmount = "\(String(describing: (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "igst")!))"
                cell.lblGst.text = "IGST (\(String(describing: igst))%) : \(String(describing: igstAmount))"
            }
            
            let imageUrl =  (self.checkOutAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as! String
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            cell.productImage.sd_setImage(with: URL(string: trimmedUrl))
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.categoryListTbl
        {
            return 44
        }
        else
        {
            return 100
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }

    @IBAction func back(_ sender: AnyObject)
    {
        BusinessManager.GetPaymentMethod(shipingAddressJsonStr as String, completionHandler: { (result) -> () in
            
            let code = result.value(forKey: "status") as! String
            if code == "200"
            {
                self.paymentinfoDic = result as! NSMutableDictionary
                self.performSegue(withIdentifier: "checkOutFourSegue", sender: self.shipingAddressJsonStr)
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
            }
        })
    }
    
    @IBAction func clickViewPolicy(_ sender: AnyObject)
    {
        let url = URL(string: "https://www.n-gal.com/delivery-and-returns")!
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "OtherCategoriesViewController") as! OtherCategoriesViewController
        viewController.urlString = "\(url)"
      //  self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
    }
}
