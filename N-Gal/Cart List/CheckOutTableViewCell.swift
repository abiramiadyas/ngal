//
//  CheckOutTableViewCell.swift
//  Exlcart
//
//  Created by iPhone on 23/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class CheckOutTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblGst: UILabel!
    @IBOutlet weak var increaseQuantityBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var decreaseQuantityBtn: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblGiftType: UILabel!
    @IBAction func increaseQuantitAction(_ sender: AnyObject) {
    }
    @IBAction func decreaseQuantityAction(_ sender: AnyObject) {
    }
    @IBAction func deleteAction(_ sender: AnyObject) {
    }
    
    let nameLbl = UILabel()
    let colonLbl = UILabel()
    let textLbl = UILabel()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.addSubview(nameLbl)
        self.contentView.addSubview(colonLbl)
        self.contentView.addSubview(textLbl)
        
        self.contentView.autoresizingMask = [UIViewAutoresizing.flexibleRightMargin, UIViewAutoresizing.flexibleLeftMargin, UIViewAutoresizing.flexibleBottomMargin, UIViewAutoresizing.flexibleTopMargin]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
