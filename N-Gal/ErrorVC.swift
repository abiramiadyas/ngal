//
//  ErrorVC.swift
//  Exlcart
//
//  Created by Adyas on 28/06/16.
//  Copyright © 2016 adyas. All rights reserved.
//

import UIKit

class ErrorVC: UIViewController {

    @IBOutlet var errorImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(ErrorVC.gotoMainVC))
        self.errorImage.isUserInteractionEnabled = true
        self.errorImage.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }

    func gotoMainVC()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarcontroller") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = viewController
        appDelegate.window?.makeKeyAndVisible()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
