//
//  SearchCVCell.swift
//  Exlcart
//
//  Created by iPhone on 22/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class SearchCVCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet var splPriceLbl: UILabel!

    @IBOutlet weak var discountLbl: UILabel!
    
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var addToFav: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.contentView.autoresizingMask = [UIViewAutoresizing.flexibleRightMargin, UIViewAutoresizing.flexibleLeftMargin, UIViewAutoresizing.flexibleBottomMargin, UIViewAutoresizing.flexibleTopMargin]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
    }

}
