//
//  SearchVC.swift
//  Exlcart
//
//  Created by iPhone on 22/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit
let reuseIdentifierSearch = "searchCell"

class SearchVC: ParentViewController,UITabBarControllerDelegate, UICollectionViewDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchCollectionView: UICollectionView!
    var searchResultAry:NSMutableArray = []
    var searchText:String = ""
    
    var page:Int = 1
    var pageCount = Double()
    var limit:String = "6"
    var productsCount:NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        self.title = "Search"
        
        let flowlayout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout1.minimumInteritemSpacing = 1.0
        flowlayout1.minimumLineSpacing = 1.0
        flowlayout1.sectionInset.top = 0
        flowlayout1.sectionInset.left = 1.0;
        flowlayout1.sectionInset.right = 1.0;
        flowlayout1.itemSize.width = (self.searchCollectionView.frame.width - 3) / 2
        flowlayout1.itemSize.height = 274;
        self.searchCollectionView .reloadData()
        self.searchCollectionView.collectionViewLayout = flowlayout1
        
        self.searchCollectionView.reloadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setLeftNavigationButton()

        
        let tabbarObj:UITabBarController = self.tabBarController!
        tabbarObj.delegate=self
        
        if self.searchResultAry.count == 0
        {
            self.searchBar.becomeFirstResponder()
        }
        else
        {
            self.searchCollectionView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
       // searchResultAry.removeAllObjects()
       // self.searchCollectionView.reloadData()
    }
    
    func sortProducts()
    {
        let flowlayout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout1.minimumInteritemSpacing = 1.0
        flowlayout1.minimumLineSpacing = 1.0
        flowlayout1.sectionInset.top = 0
        flowlayout1.sectionInset.left = 1.0;
        flowlayout1.sectionInset.right = 1.0;
        flowlayout1.itemSize.width = (self.searchCollectionView.frame.width - 3) / 2
        flowlayout1.itemSize.height = 274;
        self.searchCollectionView .reloadData()
        self.searchCollectionView.collectionViewLayout = flowlayout1
    }
    
    func pullToRefresh()
    {
        if page != Int(self.pageCount) + 1
        {
            page += 1
            BusinessManager.getSearchResult(searchText, languageId: languageID, userid: userIDStr, limit: limit, page: page, completionHandler: { (result) -> () in
                
                let resultArr = result.value(forKey: "products") as! NSMutableArray
                
                for i in 0 ..< (resultArr as AnyObject).count
                {
                    let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                    
                    if ((nameSTr)?.lowercased().contains("series"))!
                    {
                        
                    }
                    else
                    {
                        self.searchResultAry.add((resultArr as AnyObject).object(at: i))
                    }
                }
                
              //  self.searchResultAry.addObjects(from: result.value(forKey: "products") as! NSMutableArray as [AnyObject])
                
                //   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                
                self.searchCollectionView.reloadData()
                
                //  }
                
            })
        }
            
        else{
            
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.pullToRefresh()
    }

     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
       _ = self.navigationController?.popToRootViewController(animated: false)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searchResultAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierSearch, for: indexPath) as! SearchCVCell
        cell.title.text = (self.searchResultAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String

        let imageUrl =  (self.searchResultAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as! String
        let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        cell.pinImage.sd_setImage(with: URL(string: trimmedUrl))
        cell.pinImage.contentMode = UIViewContentMode.scaleAspectFill
        
        cell.splPriceLbl.text = (searchResultAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
        
        if !cell.splPriceLbl.text!.isEmpty
        {
            cell.splPriceLbl.text = (searchResultAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
            
            cell.priceLbl.text = (searchResultAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
            
            cell.splPriceLbl.isHidden = false
            let attributeString:NSMutableAttributedString =  NSMutableAttributedString(string: cell.splPriceLbl.text!)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.splPriceLbl.attributedText = attributeString
        }
        else
        {
            cell.priceLbl.text = (searchResultAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
            
            cell.splPriceLbl.isHidden = true
        }
        
        let str2 = (self.searchResultAry.object(at: indexPath.row) as AnyObject).value(forKey: "product_id") as! NSString
        if self.checkProductInWishList(str2 as String) == true
        {
            let image = UIImage(named: "heartsmallorg") as UIImage!
            cell.imgFav.image  = image
        }
        else
        {
            let image = UIImage(named: "heartsmallGrey") as UIImage!
            cell.imgFav.image  = image
        }
        cell.addToFav.addTarget(self, action: #selector(SearchVC.addToFav(_:)), for: UIControlEvents.touchUpInside)
        cell.addToFav.tag = (indexPath as NSIndexPath).row
        
        if (self.searchResultAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String != ""
        {
            let discoutStr:String = ((self.searchResultAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String)!
            cell.discountLbl.text = discoutStr
            
            cell.discountLbl.isHidden = false
        }
        else
        {
            cell.discountLbl.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ProductDetailsSegue1", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetailsSegue1"
        {
            let productDetailObj = segue.destination as! ProductDetailsVC
            productDetailObj.productID = (self.searchResultAry.object(at: (sender! as AnyObject).row) as AnyObject).value(forKey: "product_id") as! String
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .blue
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        self.view.endEditing(true)
        self.tabBarController?.selectedIndex = 0
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(false, animated: true)
        let url = searchBar.text!
      //  let trimmedUrl = url.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
        self.view.endEditing(true)
        searchText = url
        self.fetchSearchResult(searchText)
    }
    
    func fetchSearchResult(_ searchKey:String)
    {
        self.searchResultAry.removeAllObjects()
        self.page = 1
        
        SharedManager.showHUD(viewController: self)
        BusinessManager.getSearchResult(searchKey, languageId: languageID, userid: userIDStr, limit: limit, page: page, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            if let resultArr = result.value(forKey: "products")
            {
                for i in 0 ..< (resultArr as AnyObject).count
                {
                    let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                    
                    if ((nameSTr)?.lowercased().contains("series"))!
                    {
                        
                    }
                    else
                    {
                        self.searchResultAry.add((resultArr as AnyObject).object(at: i))
                    }
                }
                
              //  self.searchResultAry  = resultArr as! NSMutableArray
                if self.searchResultAry.count > 0
                {
                    self.productsCount = result.value(forKey: "count") as! NSString
                    self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                    self.searchCollectionView.scrollsToTop = true
                    self.searchCollectionView.reloadData()
                }
                else
                {
                    self.searchCollectionView.reloadData()
                    
                    SharedManager.showAlertWithMessage(title: "No result found!", alertMessage: kEmptySearchAlertMessage, viewController: self)
                }
            }
            else
            {
                self.searchCollectionView.reloadData()
                
                SharedManager.showAlertWithMessage(title: "No result found!", alertMessage: kEmptySearchAlertMessage, viewController: self)
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func checkProductInWishList(_ str2:String) -> Bool
    {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToWishList) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in favAry
            {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                if let str1 = tempCart.value(forKey: "product_id")
                {
                    if (str1 as! NSString).isEqual(to: str2 as String)
                    {
                        isAleadyHave = true
                    }
                }
            }
        }
        return isAleadyHave
    }
    
    func checkProductInCart(_ str2:String) -> Bool
    {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToCart) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in cartAry
            {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                if let str1 = tempCart.value(forKey: "product_id")
                {
                    if (str1 as! NSString).isEqual(to: str2 as String)
                    {
                        isAleadyHave = true
                    }
                }
            }
            
        }
        
        return isAleadyHave
    }
    
    
    func addToCard(_ sender : UIButton)
    {
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let object:NSArray = (self.searchResultAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "options") as! NSArray
        if object.count == 0
        {
            
            let str2 = (self.searchResultAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "product_id") as! NSString
            
            
            if self.checkProductInCart(str2 as String) == true
            {
                let isCartHave:Bool = self.checkProductInCart(str2 as String)
                
                if UserDefaults.standard.object(forKey: kAddToCart) != nil
                {
                    let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
                    let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                    
                
                    
                    if isCartHave
                    {
                        for tempDic in cartAry
                        {
                            let tempCart:NSDictionary = tempDic as! NSDictionary
                            if let str1 = tempCart.value(forKey: "product_id")
                            {
                                if (str1 as! NSString).isEqual(to: str2 as String)
                                {
                                    cartAry.remove(tempDic)
                                }
                            }
                            
                        }
                    }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToCart)
                   
                }
            }
            else
            {
                let isCartHave:Bool = self.checkProductInCart(str2 as String)
                
                if UserDefaults.standard.object(forKey: kAddToCart) != nil
                {
                    let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
                    let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                    
                 
                    
                    if !isCartHave
                    {
                        let userQuantity : NSMutableDictionary  = self.searchResultAry.object(at: (index as NSIndexPath).row) as! NSMutableDictionary
                        userQuantity.setValue("1", forKey: "userQuantity")
                        cartAry.add(userQuantity)
                    }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToCart)
               
                }
                else
                {
                    let userQuantity : NSMutableDictionary  = self.searchResultAry.object(at: (index as NSIndexPath).row) as! NSMutableDictionary
                    userQuantity.setValue("1", forKey: "userQuantity")
                    let array:NSMutableArray = []
                    array.add(userQuantity)
                    
                    let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                    UserDefaults.standard.set(data1, forKey: kAddToCart)
                    
                }
            }
        }
        else
        {
            self.performSegue(withIdentifier: "ProductDetailsSegue1", sender: index)
        }
        self.searchCollectionView.reloadData()
        self.updateCartBadge()
    }
    
    func addToFav(_ sender : UIButton)
    {
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let str2 = (self.searchResultAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "product_id") as! NSString
        
        let values:String = NSString(format: "product_id=%@&user_id=%@",str2 as String,userIDStr) as String
        
        if self.checkProductInWishList(str2 as String) == true
        {
            SharedManager.showHUD(viewController: self)
            BusinessManager.removeFromWishListWithValues(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    let isHave:Bool = self.checkProductInWishList(str2 as String)
                    
                    if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                    {
                        let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                        let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                        
                        if isHave
                        {
                            for tempDic in cartAry
                            {
                                let tempCart:NSDictionary = tempDic as! NSDictionary
                                if let str1 = tempCart.value(forKey: "product_id")
                                {
                                    if (str1 as! NSString).isEqual(to: str2 as String)
                                    {
                                        cartAry.remove(tempDic)
                                    }
                                }
                            }
                        }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                        
//                        self.addToWishListBtn.setTitle("          Add to wishlist", forState: UIControlState.Normal)
//                        let image = UIImage(named: "heartsmallGrey") as UIImage!
//                        self.favImgView.image = image
                        
                    }
                    
                }
                
                self.searchCollectionView.reloadData()
            })
        }
        else
        {
            if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
            {
                SharedManager.showHUD(viewController: self)
            BusinessManager.addToWishListWithValues(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    let isHave:Bool = self.checkProductInWishList(str2 as String)
                    
                    if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                    {
                        let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                        let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                        
                        if !isHave
                        {
                            cartAry.add(self.searchResultAry.object(at: (index as NSIndexPath).row))
                        }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                    }
                    else
                    {
                        let array:NSMutableArray = []
                        array.add(self.searchResultAry.object(at: (index as NSIndexPath).row))
                        let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                        UserDefaults.standard.set(data1, forKey: kAddToWishList)
                        
                    }
                    
//                    self..setTitle("          Remove wishlist", forState: UIControlState.Normal)
//                    let image = UIImage(named: "heartsmallorg") as UIImage!
//                    self.favImgView.image = image
                    
                    
                }
                
                self.searchCollectionView.reloadData()
            })
        }
        
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            
            //viewController.delegate = self
            
            
            self.present(viewController, animated: true, completion: { () -> Void in
                
            })
            
        }
    }
        
    }
    func reviewsBtnClick(_ sender: UIButton){
        
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "ReviewSegue") as! ReviewsVC
        VC.reviewsAry = (self.searchResultAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "reviews_list") as! NSArray
        VC.productID = ((self.searchResultAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "product_id") as? String)!
        
        VC.productName = ((self.searchResultAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String)!
        self.navigationController!.pushViewController(VC, animated: true)
    }
}
