//
//  HeaderCollectionViewCell.swift
//  SizeChartDemo
//
//  Created by Adyas Iinfotech on 09/05/17.
//  Copyright © 2017 Adyas Iinfotech. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
}
