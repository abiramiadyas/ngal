//
//  SpecificationTableViewCell.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 15/09/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class SpecificationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
