//
//  ZoomViewController.swift
//  Exlcart
//
//  Created by Tech Basics on 10/02/16.
//  Copyright (c) 2016 iPhone. All rights reserved.
//

import UIKit

class ZoomViewController: ParentViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var productImage:NSArray = []
    var selectedTag = Int ()
   
    @IBOutlet weak var imageCollView: UICollectionView!

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var zoomImgScrollView: UIScrollView!
    
    var isSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.zoomImgScrollView.minimumZoomScale = 1.0;
        self.zoomImgScrollView.maximumZoomScale = 6.0;
        let tap = UITapGestureRecognizer(target: self, action: #selector(ZoomViewController.tappedMe))
        self.mainImage.addGestureRecognizer(tap)
        self.mainImage.isUserInteractionEnabled = true
        
        zoomImgScrollView.autoresizesSubviews = true
        
        
        if productImage.count != 0
        {
            let imageUrl = (self.productImage[0] as AnyObject).value(forKey: "image") as! String
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
            
            self.imageCollView.reloadData()
        }
         else
        {
            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "No images Found", viewController: self)
        }
    }
    
    func loadScrollImages()
    {
        let imageWidth:CGFloat = 70
        let imageHeight:CGFloat = 70
        var yPosition:CGFloat = 0
        var scrollViewContentSize:CGFloat=0;
        
        for index : Int in 0 ..< self.productImage.count
        {
            if index == 0
            {
                let imageUrl = (self.productImage[index] as AnyObject).value(forKey: "image") as! String
                let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
            }
            
            let imgUrl : String = (self.productImage[index] as AnyObject).value(forKey: "image") as! String
            let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            let myImageView:UIImageView = UIImageView()
            myImageView.sd_setImage(with: URL(string: trimmedUrl))
            myImageView.contentMode = UIViewContentMode.scaleAspectFit
            
            myImageView.frame.size.width = imageWidth
            myImageView.frame.size.height = imageHeight
            myImageView.frame.origin.x = yPosition
            myImageView.tag = index

           // self.imageScrollView.addSubview(myImageView)
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ZoomViewController.imageTapped(_:)))
            myImageView.addGestureRecognizer(tapGestureRecognizer)
            myImageView.isUserInteractionEnabled = true
            let spacer:CGFloat = 20
            
            yPosition+=imageHeight + spacer
            scrollViewContentSize+=imageHeight + spacer
            
          //  self.imageScrollView.contentSize = CGSize(width: scrollViewContentSize, height: imageWidth)
            
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: { () -> Void in
        })
        
    }
    
    func viewForZoomingInScrollView(_ scrollView: UIScrollView) -> UIView?
    {
        return self.mainImage
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, withView view: UIView!, atScale scale: CGFloat)
    {
        
    }
    
    func imageTapped(_ img: UITapGestureRecognizer)
    {
        self.zoomImgScrollView.zoomScale = 1.0
        let myImageView:UIImageView = img.view as! UIImageView
        
        self.selectedTag = myImageView.tag
        
        
        let imgUrl : String = (self.productImage[myImageView.tag] as AnyObject).value(forKey: "image") as! String
        let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
        
       // self.loadScrollImages()
    }

    func tappedMe()
    {
        self.zoomImgScrollView.zoomScale = 2.0;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return productImage.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "zoomCell", for: indexPath) as! ColorCollectionViewCell
        
        let imageURL = (self.productImage[indexPath.row] as AnyObject).value(forKey: "image") as! String
        
        if (imageURL as AnyObject).lowercased.range(of: "no_image") != nil
        {
            
        }
        else
        {
            let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            cell.productImg.sd_setImage(with: URL(string: trimmedUrl))
        }
        
        if selectedTag == indexPath.row
        {
            cell.productImg.layer.borderColor = UIColor(red: 252/255, green: 17/255, blue: 94/255, alpha: 1).cgColor
            cell.productImg.layer.borderWidth = 2.0
            cell.productImg.layer.cornerRadius = 7.0
            cell.productImg.clipsToBounds = true
        }
        else
        {
            cell.productImg.layer.borderColor = UIColor.white.cgColor
            cell.productImg.layer.borderWidth = 0
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.zoomImgScrollView.zoomScale = 1.0
        let imgUrl : String = (self.productImage[indexPath.row] as AnyObject).value(forKey: "image") as! String
        let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
        
        selectedTag = indexPath.row
        
        self.imageCollView.reloadData()
    }
    
}
