//
//  ProductDetailsVC.swift
//  Exlcart
//  Created by iPhone on 17/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

let kAddToCart:String = "_kADDTOCART_CONSTANT"
let kAddToCartOptions:String = "_kADDTOCART_CONSTANT_OPTIONS"
let kAddToWishList:String = "_kADDTOWISHLIST_CONSTANT"

class ProductDetailsVC: ParentViewController,loginIntimation, UIWebViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate
    , FSPagerViewDataSource, FSPagerViewDelegate
{
    @IBOutlet weak var specificationView: UIView!
    @IBOutlet weak var sizeScrollView: UIScrollView!
    @IBOutlet weak var btncheckDeliverySubmit: UIButton!
    @IBOutlet weak var relatedProductsView: UIView!
    @IBOutlet weak var relatedProductsCollecVw: UICollectionView!
    @IBOutlet weak var colorsView: UIView!
    @IBOutlet weak var imgpinTxt: UIImageView!
    @IBOutlet weak var colorsCollecVw: UICollectionView!
    var productID:String = ""
    var DeliveryDict : NSMutableDictionary = [:]
    var productDetailsDic : NSMutableDictionary = [:]
    var myImages:NSMutableArray = []
    var optionsDic:NSMutableDictionary = [:]
    var paragraphStyle = NSMutableParagraphStyle()
    let attributes : NSDictionary = [:]
    
    var relatedProductListAry : NSMutableArray = []
    var relatedProducts : NSDictionary = [:]
    var colorsArr = NSArray ()
    var descriptionWebView:UIWebView = UIWebView()
    let descView:UIView = UIView()
    var index = NSInteger()
    var index1 = NSInteger()
    var quantity1 = NSString()
    var scrollHeight:CGFloat = 0.0
    
    var descriptionY:CGFloat = 0.0
    
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var deliveryOptionsView: UIView!
    @IBOutlet weak var paymentOptionsLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    var first:Bool = true
    @IBOutlet weak var CheckDeliveryBtn: UIButton!
    @IBOutlet weak var viewBlurEff: UIView!
    @IBOutlet weak var viewPincode: UIView!
    @IBOutlet var ratingStar: TPFloatRatingView!
    @IBOutlet weak var favImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productImageView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var discribtionLbl: UILabel!
    @IBOutlet weak var addToCart: UIButton!
    @IBOutlet weak var addToWishListBtn: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet var splPriceLbl: UILabel!
    @IBOutlet weak var votesAndReviewLbl: UILabel!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var offerBtn: UIButton!
   // @IBOutlet weak var btnCheckDelivery: UIButton!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var relatedProductsLbl: UILabel!
    @IBOutlet weak var checkDeliveryTxt: UITextField!
    
    @IBOutlet weak var btnSizeChart: UIButton!
    var page:Int = 1
    var pageCount = Double()
    var limit:String = "8"
    var productsCount:NSString = ""
    
    var offerImageStr = ""
    
    // Size Chart
    @IBOutlet weak var sizeChartCollecVw: UICollectionView!
    @IBOutlet weak var sizeChartView: UIView!
    
    @IBOutlet weak var headerCollectionVw: UICollectionView!
    var sizeChartArr : NSArray = []
    
    var headingArr : NSArray = []
    var sizeValuesArr : NSMutableArray = []
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            pagerView.dataSource = self
            pagerView.delegate = self
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = .zero
        }
    }
    
    @IBOutlet weak var pagerControl: FSPageControl!
        {
        didSet {
            self.pagerControl.numberOfPages = self.myImages.count
            self.pagerControl.contentHorizontalAlignment = .center
            self.pagerControl.backgroundColor = .white
            self.pagerControl.fillColors = [.normal : UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)]
            self.pagerControl.strokeColors = [.selected : UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)]
            self.pagerControl.currentPage = 1
            self.pagerControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    // MARK: PAGER
    
    // MARK:- FSPagerView DataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int
    {
        return myImages.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell
    {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        //  cell.imageView?.image = UIImage(named: self.myImages[index] as! String)
        //  cell.imageView?.contentMode = .scaleAspectFill
        //  cell.imageView?.clipsToBounds = true
        // cell.textLabel?.text = index.description+index.description
        
        let imgUrl : String = (self.myImages[index] as AnyObject).value(forKey: "image") as! String
        let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        cell.imageView?.sd_setImage(with: URL(string: trimmedUrl))
        cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        cell.imageView?.backgroundColor = .white
        
        var activityLoader = UIActivityIndicatorView()
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = (cell.imageView?.center)!
        activityLoader.startAnimating()
        cell.imageView?.addSubview(activityLoader)
        
        cell.imageView?.sd_setImage(with: URL(string: trimmedUrl), completed:
            { (image, error, imageCacheType, imageUrl) in
                if image != nil
                {
                    activityLoader.stopAnimating()
                    
                }
                else
                {
                    print("image not found")
                    activityLoader.stopAnimating()
                }
        })
        
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pagerControl.currentPage = index
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        // instantiate your desired ViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "zoomViewControllerIdentifier") as! ZoomViewController
        viewController.productImage = self.myImages
        self.present(viewController, animated: true, completion: { () -> Void in
            
        })
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pagerControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pagerControl.currentPage = pagerView.currentIndex // Or Use KVO with property "currentIndex"
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Navigation Bar
        let barView = UIView()
        barView.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        
        let img1 = UIImageView()
        img1.image = UIImage (named: "right-arrow.png")
        img1.frame = CGRect(x: 0, y: 10, width: 15, height: 20)
        img1.contentMode = .scaleAspectFit
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setTitleColor(.black, for: .normal)
        myBackButton.sizeToFit()
        myBackButton.frame = CGRect(x: 12, y: 5, width: 55, height: 30)
        
        barView.addSubview(img1)
        barView.addSubview(myBackButton)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: barView)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        
        self.favImgView.frame.origin.x = self.favImgView.frame.origin.x
        
        self.offerBtn.isHidden = true
        self.btnSizeChart.isHidden = true
        self.relatedProductsView.addSubview(self.relatedProductsLbl)
        
        self.viewPincode.frame.size.height = 94
        
        self.relatedProductsCollecVw.delegate = self
        self.relatedProductsCollecVw.dataSource = self
        
        self.checkDeliveryTxt.delegate = self
        
        self.addToCart.setTitle("", for: UIControlState())
        self.titleLbl.text = ""
        self.priceLbl.text = ""
        self.splPriceLbl.text = ""
        self.votesAndReviewLbl.text = ""
        
        self.offerView.isHidden = true
        self.mainScrollView.autoresizesSubviews = true
        
        self.viewBlurEff.isHidden = true
        self.viewPincode.isHidden = true
        
        CheckDeliveryBtn.layer.borderColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1).cgColor
        discountLbl.text = ""
        
        let img = UIImage (named: "ic_share")
        let tintedImage = img?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
      //  img = img?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: tintedImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.share))
        self.navigationController?.navigationBar.tintColor = .black
        
        SharedManager.showHUD(viewController: self)
        
        // Related Products
        BusinessManager.getRelatedProducts(productID, languageID: languageID, page: 1, limit: "", completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            self.relatedProducts = result 
            
            if result.value(forKey: "status") as! String == "200"
            {
                let tempArr = NSMutableArray ()
              //  tempArr.addObjects(from: self.relatedProducts.value(forKey: "product") as! NSMutableArray as [AnyObject])
                
                let resultArr = self.relatedProducts.value(forKey: "product") as! NSMutableArray
                
                for i in 0 ..< (resultArr as AnyObject).count
                {
                    let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                    
                    if ((nameSTr)?.lowercased().contains("series"))!
                    {
                        
                    }
                    else
                    {
                        tempArr.add((resultArr as AnyObject).object(at: i))
                    }
                }
                
                for i in 0..<tempArr.count
                {
                    if let productIdStr = (tempArr[i] as AnyObject).value(forKeyPath: "product_id")
                    {
                        self.relatedProductListAry.addObjects(from: [tempArr[i]])
                    }
                    else
                    {
                        print("no_image")
                    }
                }
                
                if self.relatedProductListAry.count == 0
                {
                    self.relatedProductsView.isHidden = true
                    self.relatedProductsView.frame.size.height = 0
                }
                else
                {
                    self.relatedProductsView.isHidden = false
                    self.relatedProductsView.frame.size.height = 340
                    self.relatedProductsCollecVw.reloadData()
                }
            }
            else
            {
                self.relatedProductsView.frame.size.height = 0
            }
        });
        
        SharedManager.showHUD(viewController: self)
        // Product Detail
        BusinessManager.getProductDetails(productID,userID: userIDStr, languageID: languageID as String,completionHandler: { (result) -> () in
          SharedManager.dismissHUD(viewController: self)
            
            if self.productDetailsDic.count == 0
            {
                self.productDetailsDic = result as! NSMutableDictionary
                let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                
                if self.productDetailsDic.value(forKeyPath: "product.special_discount") != nil
                {
                    let discoutStr:String = (self.productDetailsDic.value(forKeyPath: "product.special_discount") as? String)!
                    self.discountLbl.text = discoutStr
                    self.discountLbl.textAlignment = .center
                    self.discountLbl.isHidden = false
                    
                    self.discountLbl.layer.borderColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1).cgColor
                    self.discountLbl.layer.borderWidth = 1
                    self.discountLbl.layer.cornerRadius = 5
                    self.discountLbl.clipsToBounds = true
                }
                else
                {
                    self.discountLbl.isHidden = true
                }
                
                self.colorsArr = self.productDetailsDic.value(forKeyPath: "product.colors.product_main_image") as! NSArray
                
                if self.colorsArr.count == 0
                {
                    self.colorsView.frame.size.height = 0
                }
                else
                {
                    self.colorsView.frame.size.height = 184
                    self.colorsCollecVw.reloadData()
                }
                
                // Size Chart
                self.sizeChartArr = userQuantity.value(forKeyPath: "size_chart.fields") as! NSArray
                self.sizeScrollView.showsHorizontalScrollIndicator = false
                self.sizeScrollView.showsVerticalScrollIndicator = false
                
                if self.sizeChartArr.count != 0
                {
                    self.btnSizeChart.isHidden = false
                    
                    self.headingArr = (self.sizeChartArr.object(at: 0) as AnyObject).value(forKey: "heading") as! NSArray
                    
                    let valueArr = (self.sizeChartArr.object(at: 0) as AnyObject).value(forKey: "size_values") as! NSArray
                    
                    self.sizeValuesArr = NSMutableArray()
                    
                    for i in 0..<valueArr.count
                    {
                        let values = valueArr[i] as! NSArray
                        
                        self.sizeValuesArr.addObjects(from: values as! [Any])
                    }
                    
                    if self.headingArr.count > 4
                    {
                        self.sizeScrollView.alwaysBounceHorizontal = true
                        
                        let count = (65 * self.headingArr.count) + (2 * self.headingArr.count)
                        
                        self.headerCollectionVw.isScrollEnabled = false
                        self.sizeChartCollecVw.isScrollEnabled = false
                        
                        var frame = self.headerCollectionVw.frame
                        frame.origin.y = 0
                        frame.size.width = CGFloat(count) + 5
                        self.headerCollectionVw.frame = frame
                        
                        frame = self.sizeChartCollecVw.frame
                        frame.origin.y = self.headerCollectionVw.frame.size.height
                        frame.size.width = CGFloat(count) + 5
                        self.sizeChartCollecVw.frame = frame
                        
                        let count1 = ((36 * (valueArr[0] as AnyObject).count) + (2 * (valueArr[0] as AnyObject).count))
                        
                        self.sizeChartCollecVw.frame.size.height = CGFloat(count1) + 2
                        
                        
                        self.sizeScrollView.contentSize = CGSize(width: CGFloat(count) + 7, height: self.sizeChartCollecVw.contentSize.height + self.headerCollectionVw.contentSize.height)
                        
                        self.sizeScrollView.addSubview(self.sizeChartCollecVw)
                        self.sizeScrollView.addSubview(self.headerCollectionVw)
                        
                        self.sizeScrollView.frame.size.height = self.sizeScrollView.contentSize.height
                        
                        if (valueArr[0] as AnyObject).count<3
                        {
                            self.sizeChartView.frame.size.height = self.sizeScrollView.contentSize.height
                        }
                        else
                        {
                            self.sizeChartView.frame.size.height = self.sizeScrollView.contentSize.height + 38
                        }
                        
                        
                        self.sizeChartView.center = CGPoint(x: self.view.bounds.midX,
                                                            y: self.view.bounds.midY)
                    }
                    else
                    {
                        
                        self.sizeScrollView.alwaysBounceVertical = true
                        
                        let count = (36 * (valueArr[0] as AnyObject).count) + (2 * (valueArr[0] as AnyObject).count)
                        
                        self.headerCollectionVw.isScrollEnabled = false
                        self.sizeChartCollecVw.isScrollEnabled = false
                        
                        var frame = self.headerCollectionVw.frame
                        frame.origin.y = 0
                        // frame.size.height = CGFloat(count) + 5
                        self.headerCollectionVw.frame = frame
                        
                        frame = self.sizeChartCollecVw.frame
                        frame.origin.y = self.headerCollectionVw.frame.size.height
                        frame.size.height = CGFloat(count) + 5
                        self.sizeChartCollecVw.frame = frame
                        
                        self.sizeScrollView.contentSize = CGSize(width: self.headerCollectionVw.frame.size.width, height:  CGFloat(count) + self.headerCollectionVw.frame.size.height + 5)
                        
                        self.sizeScrollView.addSubview(self.sizeChartCollecVw)
                        self.sizeScrollView.addSubview(self.headerCollectionVw)
                        
                        self.sizeScrollView.frame.size.height = self.sizeScrollView.contentSize.height
                        self.sizeScrollView.frame.size.width = self.sizeScrollView.contentSize.width
                        self.sizeChartView.frame.size.height = self.sizeScrollView.contentSize.height + 38
                        self.sizeChartView.frame.size.width = self.sizeScrollView.contentSize.width + 4
                        
                        self.sizeChartView.center = CGPoint(x: self.view.bounds.midX,
                                                            y: self.view.bounds.midY)
                    }
                    
                    self.sizeChartCollecVw.reloadData()
                    self.headerCollectionVw.reloadData()
                }
                else
                {
                    self.btnSizeChart.isHidden = true
                }
                
                userQuantity.setValue("1", forKey: "userQuantity")
                self.productDetailsDic.setObject(userQuantity, forKey: "product" as NSCopying)
                self.mainScrollView.isScrollEnabled = true
                
                let isAleadyHave:Bool = self.checkProductInCart()
                if isAleadyHave
                {
                    self.addToCart.setTitle("ADD TO CART", for: UIControlState())
                }
                else
                {
                    self.addToCart.setTitle("ADD TO CART", for: UIControlState())
                }
                
                let isAleadyFavorited:Bool = self.checkProductInWishList()
                if isAleadyFavorited
                {
                    let image = UIImage(named: "heartsmallorg") as UIImage!
                    self.favImgView.image = image
                    self.addToWishListBtn.setTitle("Remove from Wishlist", for: UIControlState())
                    self.favImgView.frame.origin.x = self.favImgView.frame.origin.x
                }
                else
                {
                    let image = UIImage(named: "heartsmallGrey") as UIImage!
                    self.favImgView.image = image
                    self.addToWishListBtn.setTitle("Add to Wishlist", for: UIControlState())
                    self.favImgView.frame.origin.x = self.favImgView.frame.origin.x + 50
                }
                
                let imageUrl = self.productDetailsDic.value(forKeyPath: "product.big_image") as! String
                let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                
               // self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
                self.titleLbl.text = self.productDetailsDic.value(forKeyPath: "product.name") as? String
                
                self.votesAndReviewLbl.text = NSString(format: "%@ REVIEWS", self.productDetailsDic.value(forKeyPath: "product.reviews") as! String) as String
                
                
                self.priceLbl.text = NSString(format: "%@",self.productDetailsDic.value(forKeyPath: "product.price") as! String) as String
                self.splPriceLbl.text = NSString(format: "%@", self.productDetailsDic.value(forKeyPath: "product.special") as! String) as String
                
                if !self.splPriceLbl.text!.isEmpty
                {
                    self.splPriceLbl.isHidden = false
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self.priceLbl.text!)
                    attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
                    self.priceLbl.attributedText = attributeString
                    self.priceLbl.textColor = UIColor.gray
                    self.splPriceLbl.textColor = UIColor.black
                }
                else
                {
                    self.splPriceLbl.isHidden = true
                    self.priceLbl.textColor = UIColor.black
                }
                
                self.setframes()
            }
            else
            {
                
            }
            
        });
    }
    
    func clickBack(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setframes()
    {
        self.title = "Product Detail"

        self.offerImageStr =  self.productDetailsDic.value(forKey: "offer_image") as! String
        
        if self.offerImageStr == ""
        {
            self.offerBtn.isHidden = true
        }
        else
        {
            self.offerBtn.isHidden = false
        }
        
        self.productImageView.frame = CGRect(x:self.productImageView.frame.origin.x, y: 10, width: self.productImageView.frame.size.width, height:self.productImageView.frame.size.height)
        
        self.myImages = (self.productDetailsDic.value(forKeyPath: "product.images") as? NSMutableArray)!
        
        let mainImgDic:NSDictionary = ["image":self.productDetailsDic.value(forKeyPath: "product.big_image") as! String]
        
        self.myImages.insert(mainImgDic, at: 0)
        self.pagerControl.numberOfPages = self.myImages.count
        self.pagerView.reloadData()
        
        if SharedManager.DeviceType.IS_IPHONE_5
        {
            self.pagerView.frame.size.height = 250
        }
        else
        {
            self.pagerView.frame.size.height = 310
        }
        
        self.pagerControl.frame.origin.y = self.pagerView.frame.size.height
        self.addToWishListBtn.frame.origin.y = self.pagerControl.frame.origin.y + self.pagerControl.frame.size.height + 7
        self.favImgView.frame.origin.y = self.pagerControl.frame.origin.y + self.pagerControl.frame.size.height + 12
        
        let font = UIFont.systemFont(ofSize: 15.0)
        self.titleLbl.font = font
        self.titleLbl.numberOfLines = 0;
        let height = ProductDetailsVC.heightForView(self.titleLbl.text!, font: font, width: UIScreen.main.bounds.size.width-20)
        
        self.titleLbl.frame = CGRect(x: 10, y: self.productImageView.frame.origin.y+self.productImageView.frame.size.height+10, width: UIScreen.main.bounds.size.width-20, height: height)
        self.mainScrollView.addSubview(self.titleLbl)
        
        self.ratingView.frame = CGRect(x: self.ratingView.frame.origin.x, y: self.titleLbl.frame.origin.y+height+5, width: self.ratingView.frame.size.width, height: self.ratingView.frame.size.height)
        
        // Options
        self.specificationView.frame.origin.y = self.ratingView.frame.origin.y + self.ratingView.frame.size.height + 10
        
        var indexY:Int = 0

        btnSizeChart.frame.origin.y = self.specificationView.frame.origin.y + self.specificationView.frame.size.height + 15
        
        var optionY:CGFloat = self.specificationView.frame.origin.y + self.specificationView.frame.size.height + 15
        let options:NSArray = (self.productDetailsDic.value(forKeyPath: "product.options") as? NSArray)!
        
        if options.count != 0
        {
            // let object: NSDictionary  = options as! NSDictionary
            
            if  let str = self.productDetailsDic.value(forKeyPath: "product.options.name") as! NSArray!
            {
                let string = str[0] as! String
                
                if string.lowercased().range(of:"size") != nil
                {
                    // self.btnSizeChart.isHidden = false
                }
                else
                {
                    // self.btnSizeChart.isHidden = true
                }
            }
        }
        else
        {
            self.btnSizeChart.isHidden = true
        }
        
        let buttonView : UIScrollView = UIScrollView()
        buttonView.showsHorizontalScrollIndicator = false
        
        for obj in options
        {
            var itemWidth:CGFloat = 10
            
            let myObject: NSDictionary  = obj as! NSDictionary
            
            let optionTitleLbl:UILabel = UILabel()
            optionTitleLbl.frame = CGRect(x: 10, y: optionY, width: 250, height: 30
            )
            optionY = optionY + 35
            
            let titleStr = myObject.value(forKey: "name") as! String
            optionTitleLbl.text = titleStr.uppercased()
            
            optionTitleLbl.backgroundColor = UIColor.clear
            optionTitleLbl.font = UIFont.boldSystemFont(ofSize: 16)
            optionTitleLbl.textAlignment = NSTextAlignment.left
            self.mainScrollView.addSubview(optionTitleLbl)
            let optionValues:NSArray = (myObject.value(forKey: "product_option_value") as? NSArray)!
            buttonView.tag = 2000
            var indexX:Int = 0
            
            for valueObj in optionValues
            {
                let value:NSDictionary = valueObj as! NSDictionary
                
                let quantity = value.value(forKey: "quantity") as! String
                
                if Int(quantity)! > 0
                {
                    let myString:String = value.value(forKey: "name") as! String
                    let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)])
                    
                    let button = CustomButton(type: UIButtonType.system)
                    
                    button.frame = CGRect(x: itemWidth, y: 5, width: max(65, size.width) , height: 40)
                    itemWidth = itemWidth + max(65, size.width) + 10
                    
                  /*  let quantity = value.value(forKey: "quantity") as! String
                    if Int(quantity)! != 0
                    {
                        button.addTarget(self, action: #selector(ProductDetailsVC.optionSelection(_:)), for: UIControlEvents.touchUpInside)
                    }
                    else
                    {
                        let image = UIImage (named: "cross.png")
                        button.setBackgroundImage(image, for: .normal)
                        button.addTarget(self, action: #selector(ProductDetailsVC.OutofStockSelection(_:)), for: UIControlEvents.touchUpInside)
                    }*/
                    
                    button.layer.cornerRadius = 5
                    button.clipsToBounds = true
                    button.backgroundColor = .clear
                    button.layer.borderWidth = 1.0
                    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                    button.layer.borderColor = UIColor.lightGray.cgColor
                    button.setTitle(myString , for: UIControlState())
                    button.setTitleColor(UIColor.black, for: UIControlState())
                    let tagStr:String = NSString(format: "%d%d", indexY,indexX) as String
                    button.tag = Int(tagStr)!
                    
                    button.type = myObject.value(forKey: "product_option_id") as! String
                    button.tagValue = indexX
                    button.addTarget(self, action: #selector(ProductDetailsVC.optionSelection(_:)), for: UIControlEvents.touchUpInside)
                    
                    buttonView.addSubview(button)
                    
                    indexX += 1
                }
                
            }
            
            indexY += 1
            buttonView.frame = CGRect(x: 0, y: optionTitleLbl.frame.origin.y + optionTitleLbl.frame.size.height + 5, width: UIScreen.main.bounds.width,  height: 50);
            
            buttonView.contentSize = CGSize(width: itemWidth, height : 50)
            buttonView.backgroundColor = .clear
            optionY = optionY + 60
            self.mainScrollView.addSubview(buttonView)
        }
        
        var frame = self.colorsView.frame
        frame.origin.y = optionY + 10
        self.colorsView.frame = frame
        
        if colorsArr.count != 0
        {
            descriptionY = self.colorsView.frame.origin.y + self.colorsView.frame.size.height + 10
        }
        else
        {
            descriptionY = self.colorsView.frame.origin.y
        }
        
        let url = NSString(format: "https://www.n-gal.com/index.php?route=product/productdesc&product_id=%@", self.productID)
        
        self.descriptionWebView.frame = CGRect(x: 0, y: self.colorsView.frame.origin.y + self.colorsView.frame.size.height + 10, width: UIScreen.main.bounds.size.width, height: 1)
        self.descriptionWebView.delegate = self
        self.descriptionWebView.sizeToFit()
        self.descriptionWebView.stringByEvaluatingJavaScript( from: String(format: "document.body.style.font-size = '8px'"))
        self.descriptionWebView.loadRequest(NSURLRequest(url: NSURL(string: url as String)! as URL) as URLRequest)
        self.mainScrollView.addSubview(self.descriptionWebView)
        
        self.mainScrollView.addSubview(self.colorsView)
        
        frame = self.relatedProductsView.frame
        frame.origin.y = descriptionWebView.frame.origin.y + descriptionWebView.frame.size.height + 10
        self.relatedProductsView.frame = frame
        
        var contentRect:CGRect = CGRect.zero
        for views in self.mainScrollView.subviews
        {
            let subView:UIView = views
            contentRect = contentRect.union(subView.frame)
            self.scrollHeight = self.scrollHeight + subView.frame.size.height
        }
        
        if self.titleLbl.frame.size.height > 25
        {
            self.scrollHeight = self.scrollHeight + 15
        }
        else
        {
            self.scrollHeight = self.scrollHeight + 10
        }
        
        self.ratingStar.editable = false
        
        var ratingStr:String = (self.productDetailsDic.value(forKeyPath: "product.rating") as? String)!
        
        if ratingStr == ""
        {
            ratingStr = "0"
        }
        
        let ratingNo  = (ratingStr as NSString).intValue
        let ratingFloat = CGFloat(ratingNo)
        self.ratingStar.rating = ratingFloat
        
        self.ratingStar.emptySelectedImage = UIImage(named: "str-gry") as UIImage!
        self.ratingStar.fullSelectedImage = UIImage(named: "str-ylo") as UIImage!
        
        self.ratingStar.contentMode = UIViewContentMode.scaleAspectFit
        self.ratingStar.maxRating = 5
        self.ratingStar.minRating = 1
        self.ratingStar.halfRatings = false
        self.ratingStar.floatRatings = true
    }
    
    func sortProducts()
    {
        let flowlayout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout1.minimumInteritemSpacing = 1.0
        flowlayout1.minimumLineSpacing = 1.0
        flowlayout1.sectionInset.top = 0
        flowlayout1.sectionInset.left = 1.0;
        flowlayout1.sectionInset.right = 1.0;
        flowlayout1.itemSize.width = (self.relatedProductsCollecVw.frame.width - 3) / 2
        flowlayout1.itemSize.height = 274;
        self.relatedProductsCollecVw.reloadData()
        self.relatedProductsCollecVw.collectionViewLayout = flowlayout1
    }
    
    func share()
    {
        // text to share
        let text = "https://www.n-gal.com/index.php?route=product/product&product_id=\(productID)"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        // activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func checkDeliveryFunc()
    {
        self.viewBlurEff.isHidden = false
        self.viewPincode.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SharedManager.dismissHUD(viewController: self)
        
        if webView.isLoading
        {
            return
        }
        else
        {
            let height = webView.scrollView.contentSize.height
        
            let descLbl:UILabel = UILabel()
        
            var coordY:CGFloat = 25.0
            
            if height > 40
            {
            
                descLbl.frame = CGRect(x: 10, y: 0, width: 250, height: 25)
                descLbl.text = NSString(format: "Description", locale: nil) as String
                descLbl.font = UIFont.boldSystemFont(ofSize: 16.0)
                descLbl.numberOfLines = 0
                descLbl.textAlignment = .left
                
                descriptionWebView.frame = CGRect(x:0, y:coordY, width:UIScreen.main.bounds.size.width, height:height)
                descriptionWebView.scrollView.bounces = false
                
                descView.addSubview(descLbl)
                descView.addSubview(descriptionWebView)
                
                coordY = coordY + max(descLbl.frame.size.height, descriptionWebView.frame.size.height) + 5
                
                descView.frame = CGRect(x:0, y: descriptionY + 5, width:UIScreen.main.bounds.size.width, height:coordY)
                
                descView.backgroundColor = UIColor.white
                self.mainScrollView.addSubview(descView)
            }
        }
        
        if self.relatedProductListAry.count != 0
        {
            var frame = self.relatedProductsView.frame
            frame.origin.y = descriptionY + descView.frame.size.height + 10
            self.relatedProductsView.frame = frame
            
            self.relatedProductsView.frame.size.height = 340
            
            self.mainScrollView.addSubview(self.relatedProductsView)
        }
        else
        {
            self.relatedProductsView.frame.size.height = 0
        }
        
        if colorsArr.count != 0
        {
            self.colorsView.frame.size.height = 184
        }
        else
        {
            self.colorsView.frame.size.height = 0
        }
        
        let optionsHeight = self.colorsView.frame.origin.y - (self.specificationView.frame.origin.y + self.specificationView.frame.size.height)
        
        let height = self.productImageView.frame.size.height + self.ratingView.frame.size.height + self.specificationView.frame.size.height + self.colorsView.frame.size.height + self.relatedProductsView.frame.size.height + optionsHeight +  descView.frame.size.height
        
        self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: height + 70)
    }
    
    
    func heightForViewSpecfic(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        if label.frame.height > 25
        {
            return label.frame.height
        }
        else
        {
            return 25
        }
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        let isAleadyHave:Bool = self.checkProductInCart()
        if isAleadyHave
        {
            self.addToCart.setTitle("ADD TO CART", for: UIControlState())
        }
        else
        {
            self.addToCart.setTitle("ADD TO CART", for: UIControlState())
        }
        
        let isAleadyFavorited:Bool = self.checkProductInWishList()
        if isAleadyFavorited
        {
            let image = UIImage(named: "heartsmallorg") as UIImage!
            self.favImgView.image = image
            self.addToWishListBtn.setTitle("Remove from Wishlist", for: UIControlState())
        }
        else
        {
            let image = UIImage(named: "heartsmallGrey") as UIImage!
            self.favImgView.image = image
            self.addToWishListBtn.setTitle("Add to Wishlist", for: UIControlState())
        }
        
    }
    
    @IBAction func buttonZoomPressed(_ sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        // instantiate your desired ViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "zoomViewControllerIdentifier") as! ZoomViewController
        viewController.productImage = self.myImages
        self.present(viewController, animated: true, completion: { () -> Void in
            
        })
    }
    
    @IBAction func clickCheckDelivery(_ sender: Any)
    {
        self.viewPincode.isHidden = false
        self.viewBlurEff.isHidden = false
    }
    
    func OutofStockSelection(_ sender:CustomButton)
    {
        SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "This Product is Out of Stock in this size", viewController: self)
    }
    
    func optionSelection(_ sender:CustomButton)
    {
        for scrollSubview in self.mainScrollView.subviews
        {
            if(scrollSubview.isKind(of: UIView.self) && scrollSubview.tag == 2000)
            {
                let buttonView:UIView = scrollSubview 
                
                for tempView in buttonView.subviews
                {
                    
                    if tempView.isKind(of: CustomButton.self) as Bool
                    {
                        let button = tempView as? CustomButton
                        
                        
                        if button?.type == sender.type
                        {
                            if sender.tagValue == button?.tagValue
                            {
                                if (button!.backgroundColor)! == UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)
                                {
                                    button!.backgroundColor = .clear
                                    button!.setTitleColor(UIColor.black, for: UIControlState())
                                }
                                else
                                {
                                    let filterPredicate:NSPredicate = NSPredicate(format: "product_option_id =%@",button!.type)
                                    
                                    let filterResult = (self.productDetailsDic.value(forKeyPath: "product.options") as! NSArray).filtered(using: filterPredicate)
                                    
                                    let tempAry:NSArray = filterResult as NSArray
                                    
                                    if tempAry.count > 0
                                    {
                                        let value:String = ((((tempAry.object(at: 0) as AnyObject).object(forKey: "product_option_value") as! NSArray).object(at: button!.tagValue) as AnyObject).object(forKey: "product_option_value_id") as? String)!
                                        
                                        let key:String = ((tempAry.object(at: 0) as AnyObject).object(forKey: "product_option_id") as?String)!
                                        
                                        optionsDic.setValue(value, forKey:key)
                                        
                                        
                                    }
                                    
                                    button!.backgroundColor = UIColor(red: 236/255, green: 87/255, blue: 107/255, alpha: 1)
                                    button!.setTitleColor(UIColor.white, for: UIControlState())
                                }
                            }
                            else
                            {
                                button!.backgroundColor = .clear
                                button!.setTitleColor(UIColor.black, for: UIControlState())
                            }
                        }
                    }
                }
            }
        }
    }

    
    func checkProductOptionInCart() -> Bool
    {
        var isCartOptionHave:Bool = false
        
        if  UserDefaults.standard.object(forKey: kAddToCart) != nil
        {
            let cartData = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: cartData) as! NSMutableArray
            
            for cartTemp in cartAry
                {
                    let tempDic:NSMutableDictionary = cartTemp as! NSMutableDictionary
                    let options = tempDic.value(forKey: "options") as! NSArray
                    
                    if options.count == 0
                    {
                        isCartOptionHave = false
                    }
                    
                    else
                    {
                        let optionValues = tempDic.value(forKey: "product_option_selection_value") as! NSMutableDictionary
                        if optionValues == self.optionsDic
                        {
                        isCartOptionHave = true
                        index1 = cartAry.index(of: cartTemp)
                        }
                    
                    }
                }
        }
    

        return isCartOptionHave
}

    func checkProductInCart() -> Bool
    {
        var isAleadyHave:Bool = false
        
        if  UserDefaults.standard.object(forKey: kAddToCart) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray

            for tempDic in cartAry
            {
                let tempCart:NSMutableDictionary = tempDic as! NSMutableDictionary
                
                if let str1 = tempCart.value(forKey: "product_id")
                {
                    if let str2 = self.productDetailsDic.value(forKeyPath: "product.product_id")
                    {
                        if (str1 as AnyObject).isEqual(to: str2 as! String)
                        {
                            isAleadyHave = true
                            index =  cartAry.index(of: tempCart)
                        }
                    }
                }
                
            }
            
        }
        
        return isAleadyHave
    }
    
    func checkProductInWishList() -> Bool
    {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToWishList) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in cartAry
            {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                if let str1 = tempCart.value(forKey: "product_id")
                {
                    if let str2 = self.productDetailsDic.value(forKeyPath: "product.product_id")
                    {
                        if (str1 as AnyObject).isEqual(to: str2 as! String)
                        {
                            isAleadyHave = true
                            
                        }
                    }
                }
            }
        }
        return isAleadyHave
    }
    
   class func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
   
        return label.frame.height

    }

    @IBAction func addToCartAction(_ sender: AnyObject)
    {
        let title:NSString = addToCart.currentTitle! as NSString
        
        if title.isEqual(to: "Remove from Cart")
        {
           // addToCart.setTitle("Add to Cart", for: UIControlState())
        }
        else
        {
            //addToCart.setTitle("Remove from Cart", for: UIControlState())
            
            let options:NSArray = (self.productDetailsDic.value(forKeyPath: "product.options") as? NSArray)!
            
            if optionsDic.allKeys.count != options.count
            {
                let alertTextArr:NSMutableArray = []
                
                for optionDic in options
                {
                    let optionsKey:NSArray = optionsDic.allKeys as NSArray
                    if(!optionsKey.contains((optionDic as AnyObject).value(forKey: "product_option_id") as! String))
                    {
                        alertTextArr.add((optionDic as AnyObject).value(forKey: "name") as! String)
                    }
                }
                
                var indexCount:Int = 0
                var alertStr:NSString = ""
                
                for str in alertTextArr
                {
                    if(indexCount == 0)
                    {
                        alertStr = (str as? String)! as NSString
                    }
                    else if(indexCount == alertTextArr.count - 1)
                    {
                        alertStr = (alertStr as String) + " and " + (str as! String) as NSString
                    }
                    else
                    {
                        alertStr = (alertStr as String) + ", " + (str as! String) as NSString
                    }
                    indexCount += 1
                }
                
                SharedManager.showAlertWithMessage(title: "Information", alertMessage: "Please choose " + (alertStr as String).lowercased() + " before you add to cart!", viewController: self)
                return
            }
            
            if options.count == 0
            {
                if  UserDefaults.standard.object(forKey: kAddToCart) != nil
                {
                    let cartData = UserDefaults.standard.object(forKey: kAddToCart) as! Data
                    let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: cartData) as! NSMutableArray
      
                    let isCartHave:Bool = self.checkProductInCart()

                    if !isCartHave
                    {
                        let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                        let maxQuantity = userQuantity.value(forKey: "quantity") as! String
                    
                        if Int(maxQuantity) > Int(txtQuantity.text!) || Int(maxQuantity) == Int(txtQuantity.text!)
                        {
                            userQuantity.setValue(txtQuantity.text, forKey: "userQuantity")
                            //userQuantity.setValue(optionsDic, forKey: "product_option_selection_value")
                        
                            cartAry.add(userQuantity)
                        
                            let cartTempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                            UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                        }
                    }
                    else
                    {
                        let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary

                        for tempCart in cartAry
                        {
                            let temp : NSMutableDictionary = tempCart as! NSMutableDictionary
                            let i = cartAry.index(of: tempCart)
                            
                            if i == index
                            {
                                let maxQuantity: String = temp.value(forKey: "quantity") as! String
                                let quantity = temp.value(forKey: "userQuantity") as! String
                                
                                if Int(maxQuantity) > Int(quantity)
                                {
                                    let answer =  Int(quantity)!+Int(txtQuantity.text!)!
                                    let value = String(answer)
                                    
                                    if Int(value)! < Int(maxQuantity) || Int(value)! == Int(maxQuantity)
                                    {
                                        userQuantity.setValue("\(txtQuantity.text!)", forKey: "userQuantity")
                                        cartAry.replaceObject(at: index, with: userQuantity)
                                        
                                        let cartTempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                                        UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                                }
                            }
                        }
                    }
                }
                else
                {
                    let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                    let maxQuantity = userQuantity.value(forKey: "quantity") as! String
                    
                    if Int(maxQuantity) > Int(txtQuantity.text!) || Int(maxQuantity) == Int(txtQuantity.text!)
                    {
                        userQuantity.setValue(txtQuantity.text, forKey: "userQuantity")
                        
                        let array:NSMutableArray = []
                        array.add(userQuantity)
                        
                        let cartAryTemp = NSKeyedArchiver.archivedData(withRootObject: array)
                        UserDefaults.standard.set(cartAryTemp, forKey: kAddToCart)
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                    }
                }
            }
            else if UserDefaults.standard.object(forKey: kAddToCart) != nil
            {
                let cartData = UserDefaults.standard.object(forKey: kAddToCart) as! Data
                let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: cartData) as! NSMutableArray
                    
                let isCartOptionHave:Bool = self.checkProductOptionInCart()
                
                if !isCartOptionHave
                {
                    let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                    
                    let maxQuantity = userQuantity.value(forKey: "quantity") as! String
                    
                    if Int(maxQuantity) > Int(txtQuantity.text!) || Int(maxQuantity) == Int(txtQuantity.text!)
                    {
                        userQuantity.setValue(txtQuantity.text, forKey: "userQuantity")
                        userQuantity.setValue(self.optionsDic, forKey: "product_option_selection_value")
                        
                        cartAry.add(userQuantity)
                        
                        let cartTempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("Product is not available in the desired quantity or out of stock!", comment: ""), viewController: self)
                    }
                }
                else
                {
                    for tempCart in cartAry
                    {
                        let temp : NSMutableDictionary = tempCart as! NSMutableDictionary
                        let i = cartAry.index(of: tempCart)
                        
                        if i == index1
                        {
                            let maxQuantity: String = temp.value(forKey: "quantity") as! String
                            let quantity1 = temp.value(forKey: "userQuantity") as! String
                            
                            if Int(maxQuantity) > 0
                            {
                                let answer =  Int(quantity1)!+Int(txtQuantity.text!)!
                                let value = String(answer)
                                
                                if Int(value)! < Int(maxQuantity) || Int(value)! == Int(maxQuantity)
                                {
                                    let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                                    userQuantity.setValue("\(txtQuantity.text!)", forKey: "userQuantity")
                                    userQuantity.setValue(optionsDic, forKey: "product_option_selection_value")
                                    
                                    cartAry.replaceObject(at: index1, with: userQuantity)
                                            
                                    let cartTempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                                    UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                            }
                        }
                    }
                }
            }
            else
            {
                let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                let maxQuantity = userQuantity.value(forKey: "quantity") as! String
                
                if Int(maxQuantity) > Int(txtQuantity.text!) || Int(maxQuantity) == Int(txtQuantity.text!)
                {
                    userQuantity.setValue(txtQuantity.text, forKey: "userQuantity")
                    userQuantity.setValue(self.optionsDic, forKey: "product_option_selection_value")
                    
                    let array:NSMutableArray = []
                    array.add(userQuantity)
                    
                    let cartTempData = NSKeyedArchiver.archivedData(withRootObject: array)
                    UserDefaults.standard.set(cartTempData, forKey: kAddToCart)
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: kProductMaximumQuantity, viewController: self)
                }
            }
        }
        self.updateCartBadge()
    }
   
    func loginSuccess()
    {
        let title:NSString = addToWishListBtn.currentTitle! as NSString
        let values:String = NSString(format: "product_id=%@&user_id=%@",self.productDetailsDic.value(forKeyPath: "product.product_id") as! String, userIDStr) as String
        
        if title.isEqual(to: "Remove from Wishlist")
        {
            SharedManager.showHUD(viewController: self)
            BusinessManager.removeFromWishListWithValues(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)

                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    let isHave:Bool = self.checkProductInWishList()
                    
                    if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                    {
                        let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                        let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                        
                        if isHave
                        {
                            cartAry.remove(self.productDetailsDic.value(forKey: "product")!)
                        }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                        self.addToWishListBtn.setTitle("Add to Wishlist", for: UIControlState())
                        self.favImgView.frame.origin.x = self.favImgView.frame.origin.x + 50
                        let image = UIImage(named: "heartsmallGrey") as UIImage!
                        self.favImgView.image = image
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
                SharedManager.showHUD(viewController: self)
            BusinessManager.addToWishListWithValues(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    let isHave:Bool = self.checkProductInWishList()
                    
                    if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                    {
                        let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                        let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                        
                        if !isHave
                        {
                            let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                            userQuantity.setValue("1", forKey: "userFavorited")
                            cartAry.add(userQuantity)
                        }
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                    }
                    else
                    {
                        let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                        userQuantity.setValue("1", forKey: "userFavorited")
                        let array:NSMutableArray = []
                        array.add(userQuantity)
                        let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                        UserDefaults.standard.set(data1, forKey: kAddToWishList)
                        
                    }
                    self.addToWishListBtn.setTitle("Remove from Wishlist", for: UIControlState())
                    self.favImgView.frame.origin.x = self.favImgView.frame.origin.x - 50
                    let image = UIImage(named: "heartsmallorg") as UIImage!
                    self.favImgView.image = image
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
    }
    
    func loginFailure()
    {
        
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
    @IBAction func addToWishListAction(_ sender: AnyObject)
    {
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let array:NSMutableDictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableDictionary
            userIDStr = array.value(forKeyPath: "customer.customer_id") as! String
            
            let title:NSString = addToWishListBtn.currentTitle! as NSString
            let values:String = NSString(format: "product_id=%@&user_id=%@",self.productDetailsDic.value(forKeyPath: "product.product_id") as! String, userIDStr) as String
            
            if title.isEqual(to: "Remove from Wishlist")
            {
                SharedManager.showHUD(viewController: self)
                BusinessManager.removeFromWishListWithValues(values, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)

                    let code = result.value(forKey: "status") as! String
                    
                    if code == "200"
                    {
                        let isHave:Bool = self.checkProductInWishList()
                        
                        if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                        {
                            let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                            
                            if isHave
                            {
                                if let str2 = self.productDetailsDic.value(forKeyPath: "product.product_id")
                                
                                {
                                    for tempDic in cartAry
                                    {
                                        let tempCart:NSDictionary = tempDic as! NSDictionary
                                        if let str1 = tempCart.value(forKey: "product_id")
                                        {
                                            if (str1 as AnyObject).isEqual(to: str2 as! String)
                                            {
                                                cartAry.remove(tempDic)
                                            }
                                        }
                                       
                                    }
                                
                                }

                                

                            }
                            let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                            UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                            self.addToWishListBtn.setTitle("Add to Wishlist", for: UIControlState())
                            self.favImgView.frame.origin.x = self.favImgView.frame.origin.x + 50
                            let image = UIImage(named: "heartsmallGrey") as UIImage!
                            self.favImgView.image = image
                        }
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                    }
                })
            }
            else
            {
                SharedManager.showHUD(viewController: self)
                BusinessManager.addToWishListWithValues(values, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    let code = result.value(forKey: "status") as! String
                    
                    if code == "200"
                    {
                        let isHave:Bool = self.checkProductInWishList()
                        
                        if UserDefaults.standard.object(forKey: kAddToWishList) != nil
                        {
                            let data = UserDefaults.standard.object(forKey: kAddToWishList) as! Data
                            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                            
                            if !isHave
                            {
                                let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                                userQuantity.setValue("1", forKey: "userFavorited")
                                cartAry.add(userQuantity)
                            }
                            let tempData = NSKeyedArchiver.archivedData(withRootObject: cartAry)
                            UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                        }
                        else
                        {
                            let userQuantity : NSMutableDictionary  = self.productDetailsDic.value(forKey: "product") as! NSMutableDictionary
                            userQuantity.setValue("1", forKey: "userFavorited")
                            let array:NSMutableArray = []
                            array.add(userQuantity)
                            let data1 = NSKeyedArchiver.archivedData(withRootObject: array)
                            UserDefaults.standard.set(data1, forKey: kAddToWishList)
                            
                        }
                        self.addToWishListBtn.setTitle("Remove from Wishlist", for: UIControlState())
                        self.favImgView.frame.origin.x = self.favImgView.frame.origin.x - 50
                        let image = UIImage(named: "heartsmallorg") as UIImage!
                        self.favImgView.image = image
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                    }
                })
            }

        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
                //
            })
        }
    }
    
    @IBAction func reviewBtnClick(_ sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // instantiate your desired ViewController
        let VC = storyboard.instantiateViewController(withIdentifier: "ReviewSegue") as! ReviewsVC
        VC.reviewsAry =  self.productDetailsDic.value(forKeyPath: "product.reviews_list") as! NSArray
        VC.productID = self.productDetailsDic.value(forKeyPath: "product.product_id") as! NSString as String
        VC.productName = (self.productDetailsDic.value(forKeyPath: "product.name") as? String)!
        self.navigationController!.pushViewController(VC, animated: true)
    }
    
    @IBAction func clickSpecification(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = mainStoryboard.instantiateViewController(withIdentifier: "SpecificationViewController") as! SpecificationViewController
        
        VC.detailsArr = (self.productDetailsDic.value(forKeyPath: "product.specification") as? NSMutableArray)!
        VC.productName = (self.productDetailsDic.value(forKeyPath: "product.name") as? String)!
        let tempArr = NSMutableArray()
        tempArr.add(self.productDetailsDic.value(forKeyPath: "product.manufacturer")!)
        tempArr.add(self.productDetailsDic.value(forKeyPath: "product.quantity")!)
        VC.gDetailsArr = tempArr
        
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func clickSizeChart(_ sender: Any)
    {
        self.sizeChartView.isHidden = false
        self.viewBlurEff.isHidden = false
    }
    
     @IBAction func submit(_ sender: Any)
     {
        if self.checkDeliveryTxt.hasText
        {
            var dicti:NSMutableDictionary = [:]
            dicti = ["pincode" : checkDeliveryTxt.text!]
            var jsonStr = ""
            
            do
            {
                let jsonData: Data = try JSONSerialization.data(withJSONObject: dicti, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonStr = NSString.init(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            }
            catch {
                print("error")
            }
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.checkDelivery(jsonStr, completionHandler: { (result) -> ()
                in
                SharedManager.dismissHUD(viewController: self)
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    self.checkDeliveryTxt.resignFirstResponder()
                    
                    self.viewPincode.frame.size.height = 240
                    self.btncheckDeliverySubmit.frame.size.height = 34
                    self.imgpinTxt.frame.size.height = 36
                    self.checkDeliveryTxt.frame.size.height = 36
                    
                    self.btncheckDeliverySubmit.frame.origin.y = 58
                    self.imgpinTxt.frame.origin.y = 12
                    self.checkDeliveryTxt.frame.origin.y = 12
                    
                    
                    self.deliveryOptionsView.frame = CGRect(x: 3, y: self.viewPincode.frame.size.height - self.deliveryOptionsView.frame.size.height - 3, width: self.viewPincode.frame.size.width - 6, height: self.deliveryOptionsView.frame.size.height)
                    
                    self.viewPincode.addSubview(self.deliveryOptionsView)
                    self.deliveryOptionsView.isHidden = false
                    
                    self.DeliveryDict = result as! NSMutableDictionary
                    
                   // self.viewBlurEff.isHidden = true
                   // self.viewPincode.isHidden = true
                    
                   self.durationLbl.text = self.DeliveryDict.value(forKeyPath: "message.duration") as? String
                    self.locationLbl.text = self.DeliveryDict.value(forKeyPath: "message.area") as? String
                    self.paymentOptionsLbl.text = self.DeliveryDict.value(forKeyPath: "message.payments") as? String
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message.fail") as! String, viewController: self)
                    
                    if (self.deliveryOptionsView != nil)
                    {
                        self.deliveryOptionsView.isHidden = true
                    }
                    
                    self.viewPincode.frame.size.height = 140
                    self.btncheckDeliverySubmit.frame.size.height = 34
                    self.imgpinTxt.frame.size.height = 36
                    self.checkDeliveryTxt.frame.size.height = 36
                    
                    self.btncheckDeliverySubmit.frame.origin.y = 87
                    self.imgpinTxt.frame.origin.y = 28
                    self.checkDeliveryTxt.frame.origin.y = 28
                    
                }
                
            })
        }
        else
        {
            if (self.deliveryOptionsView != nil)
            {
                self.deliveryOptionsView.isHidden = true
            }
            
            self.viewPincode.frame.size.height = 140
            self.btncheckDeliverySubmit.frame.size.height = 34
            self.imgpinTxt.frame.size.height = 36
            self.checkDeliveryTxt.frame.size.height = 36
            self.btncheckDeliverySubmit.frame.origin.y = 87
            self.imgpinTxt.frame.origin.y = 28
            self.checkDeliveryTxt.frame.origin.y = 28
            
            
            SharedManager.showAlertWithMessage(title: "Information", alertMessage: "Please Enter your Pincode to Check Delivery Options", viewController: self)
        }
    }

    @IBAction func checkDeliveryClose(_ sender: Any)
    {
        self.checkDeliveryTxt.resignFirstResponder()
        self.viewBlurEff.isHidden = true
        self.viewPincode.isHidden = true
        self.offerView.isHidden = true
        self.sizeChartView.isHidden = true
    }
        
    @IBAction func offer(_ sender: Any)
    {
        if offerImageStr == "" || offerImageStr == "https://www.n-gal.com/image/" || offerImageStr == "https://www.n-gal.com/image/no_image"
        {
            self.offerView.isHidden = true
            self.viewBlurEff.isHidden = true
            SharedManager.showAlertWithMessage(title: "", alertMessage: "This Product is already on the best price", viewController: self)
        }
        else
        {
            self.offerView.isHidden = false
            self.viewBlurEff.isHidden = false
            self.offerImage.sd_setImage(with: URL(string: offerImageStr))
        }
    }
    
    @IBAction func offerButtonClose(_ sender: Any) {
        self.offerView.isHidden = true
        self.viewBlurEff.isHidden = true
    }

    
    func openZoom() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // instantiate your desired ViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "zoomViewControllerIdentifier") as! ZoomViewController
        self.present(viewController, animated: true, completion:nil)
    }
    
    @IBAction func viewProductList(_ sender: AnyObject)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
        viewController.productListAry = self.relatedProductListAry
        viewController.catTitle = "Related Products"
        viewController.isFromHome = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func pullToRefresh()
    {
        
        if page != Int(self.pageCount) + 1 && !categoryID.isEmpty
        {
            page += 1
            BusinessManager.getRelatedProducts(productID, languageID: languageID, page: 1, limit: limit, completionHandler:
                { (result) -> () in
                    
                    
                    let resultArr = result.value(forKey: "product") as! NSMutableArray
                                                
                    for i in 0 ..< (resultArr as AnyObject).count
                    {
                        let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                        
                        if ((nameSTr)?.lowercased().contains("series"))!
                        {
                            
                        }
                        else
                        {
                            self.relatedProductListAry.add((resultArr as AnyObject).object(at: i))
                        }
                    }
                
                
                  //  self.relatedProductListAry.addObjects(from: result.value(forKey: "product") as! NSMutableArray as [AnyObject])
                
                                                
                //   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    
                self.relatedProductsCollecVw.reloadData()
                    
                //  }
            })
        }
            
        else
        {
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.colorsCollecVw
        {
            return colorsArr.count
        }
        else if collectionView == self.relatedProductsCollecVw
        {
            return relatedProductListAry.count
        }
        else if collectionView == self.sizeChartCollecVw
        {
            return sizeValuesArr.count
        }
        else
        {
            return headingArr.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.colorsCollecVw
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCell", for: indexPath) as! ColorCollectionViewCell
            
            let imageURL = (colorsArr[indexPath.row] as AnyObject) as! String
            
            if (imageURL as AnyObject).lowercased.range(of: "no_image") != nil
            {
                
            }
            else
            {
                let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                cell.productImg.sd_setImage(with: URL(string: trimmedUrl))
            }

            return cell
        }
        else if collectionView == self.relatedProductsCollecVw
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "relatedCell", for: indexPath) as! RelatedProductsCollectionViewCell
            
            let imageURL =  (self.relatedProductListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as! String
            if (imageURL as AnyObject).lowercased.range(of: "no_image") != nil
            {
                
            }
            else
            {
                let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                cell.productImg.sd_setImage(with: URL(string: trimmedUrl))
                
                cell.title.text = (self.relatedProductListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
                
                cell.splPriceLbl.text = (relatedProductListAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
                
                if !cell.splPriceLbl.text!.isEmpty
                {
                    cell.splPriceLbl.text = (relatedProductListAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                    
                    cell.priceLbl.text = (relatedProductListAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
                    
                    cell.splPriceLbl.isHidden = false
                    let attributeString:NSMutableAttributedString =  NSMutableAttributedString(string: cell.splPriceLbl.text!)
                    attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
                    cell.splPriceLbl.attributedText = attributeString
                }
                else
                {
                    cell.priceLbl.text = (relatedProductListAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                    
                    cell.splPriceLbl.isHidden = true
                }
                
                if (self.relatedProductListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String != ""
                {
                    let discoutStr:String = ((self.relatedProductListAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String)!
                    cell.discountLbl.text = discoutStr
                    
                    cell.discountLbl.isHidden = false
                }
                else
                {
                    cell.discountLbl.isHidden = true
                }
            }
            return cell
        }
        else if collectionView == self.sizeChartCollecVw
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sizeCell", for: indexPath) as! SizeCollectionViewCell
            
            cell.sizeLable.text = String(format: "\(self.sizeValuesArr[indexPath.row])")
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCell", for: indexPath) as! HeaderCollectionViewCell
            
            cell.titleLbl.text = String(format: "\(self.headingArr[indexPath.row])")
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == self.colorsCollecVw
        {
            if let color = (self.productDetailsDic.value(forKeyPath: "product.colors"))
            {
                let selproductID = ((color as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "product_id") as! String
                
                self.productDetailsDic.removeAllObjects()
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
                vc.productID = selproductID
                navigationController?.pushViewController(vc, animated: false)
            }
        }
        else if collectionView == self.relatedProductsCollecVw
        {
           // let product = (self.relatedProductListAry.value(forKey: "product.colors")) as! NSArray
            let selproductID = (self.relatedProductListAry[indexPath.row] as AnyObject).value(forKey: "product_id") as! String
            
            self.productDetailsDic.removeAllObjects()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            vc.productID = selproductID
            navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
       /* if scrollView.isEqual(sizeChartCollecVw), scrollView.isDragging
        {
            var offset = headerCollectionVw.contentOffset
            offset.x = sizeChartCollecVw.contentOffset.x
            headerCollectionVw.setContentOffset(offset, animated: true)
        }
        else if scrollView.isEqual(headerCollectionVw), scrollView.isDragging
        {
            var offset = sizeChartCollecVw.contentOffset
            offset.x = headerCollectionVw.contentOffset.x
            sizeChartCollecVw.setContentOffset(offset, animated: true)
        }*/
    }
}
