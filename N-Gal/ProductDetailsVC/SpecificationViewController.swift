//
//  SpecificationViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 15/09/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class SpecificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var tblOptions: UITableView!
    
    var gDetailsArr = NSMutableArray()
    var optionArr = NSMutableArray()
    var detailsArr = NSMutableArray()
    var productName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // print(detailsArr)
        lblProductName.text = productName
        
        let tempDict = NSMutableDictionary()
        tempDict.setObject(gDetailsArr[0], forKey: "text" as NSCopying)
        tempDict.setObject("Brand", forKey: "name" as NSCopying)
        
        let tempDict1 = NSMutableDictionary()
        tempDict1.setObject(gDetailsArr[1], forKey: "text" as NSCopying)
        tempDict1.setObject("Stock", forKey: "name" as NSCopying)
        
        let tempArr = NSMutableArray ()
        tempArr.add(tempDict)
        tempArr.add(tempDict1)
        
        let newDict = NSMutableDictionary()
        newDict.setObject(tempArr, forKey: "attribute" as NSCopying)
        newDict.setObject("", forKey: "name" as NSCopying)
        
        detailsArr.insert(newDict, at: 0)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        detailsArr.removeAllObjects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return detailsArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       // print((((detailsArr[0] as AnyObject).value(forKey: "attribute")!) as AnyObject).count)
        return (((detailsArr[section] as AnyObject).value(forKey: "attribute")!) as AnyObject).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SpecificationTableViewCell = self.tblOptions.dequeueReusableCell(withIdentifier: "optionCell") as! SpecificationTableViewCell
        
        cell.lblName.text = "\((((detailsArr.object(at: indexPath.section) as AnyObject).value(forKey: "attribute") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "name") as! String)"
        cell.lblValue.text = "\((((detailsArr.object(at: indexPath.section) as AnyObject).value(forKey: "attribute") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "text") as! String)"
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 54
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section != 0
        {
            return 44
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let CellIdentifier: String = "sectionHeader"
        let headerView: SpecificationTableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! SpecificationTableViewCell?

        headerView?.lblTitle.text = (detailsArr.object(at: section) as AnyObject).value(forKeyPath: "name") as! String?
        
        if headerView == nil
        {
            print("No cells with matching CellIdentifier loaded from your storyboard")
            
            //   NSException.raise("headerView == nil..", format: "No cells with matching CellIdentifier loaded from your storyboard")
        }
        return headerView!
    }
    
   /* func values()
    {
        if specificationValue.count > 0
        {
            let value:NSMutableArray = (((self.productDetailsDic.value(forKeyPath: "product.specification") as! NSMutableArray).object(at: 0) as AnyObject).object(forKey: "attribute") as? NSMutableArray)!
            let detailY:CGFloat = self.discribtionLbl.frame.origin.y + self.discribtionLbl.frame.size.height + (optionY - (self.discribtionLbl.frame.origin.y + self.discribtionLbl.frame.size.height))
            let sepcView:UIView = UIView()
            
            var coordinateY:CGFloat = 45.0
            let titleLbl:UILabel = UILabel()
            titleLbl.frame = CGRect(x: 5, y: 15, width: 200, height: 25);
            titleLbl.text = NSString(format: "Specification : %@", (((self.productDetailsDic.value(forKeyPath: "product.specification") as! NSMutableArray).object(at: 0) as AnyObject).object(forKey: "name") as! String)) as String
            sepcView.addSubview(titleLbl)
            for temmp in value
            {
                let temmmp:NSDictionary = temmp as! NSDictionary
                
                let nameLbl:UILabel = UILabel(frame: CGRect(x: 5, y: coordinateY, width: (UIScreen.main.bounds.size.width/2) - 30, height: self.heightForViewSpecfic((temmmp.object(forKey: "name") as? String)!, font: UIFont.systemFont(ofSize: 12), width: (UIScreen.main.bounds.size.width/2) - 30)))
                nameLbl.backgroundColor = UIColor.clear
                nameLbl.text = temmmp.object(forKey: "name") as? String
                nameLbl.font = UIFont.systemFont(ofSize: 12)
                nameLbl.numberOfLines = 0
                
                let colonLbl:UILabel = UILabel(frame: CGRect(x: nameLbl.frame.size.width+8, y: coordinateY, width: 2, height: 25))
                colonLbl.backgroundColor = UIColor.clear
                colonLbl.font = UIFont.systemFont(ofSize: 12)
                colonLbl.text = ":"
                
                let textLbl:UILabel = UILabel(frame: CGRect(x: nameLbl.frame.size.width+12, y: coordinateY, width: (UIScreen.main.bounds.size.width/2) + 13, height: self.heightForViewSpecfic((temmmp.object(forKey: "text") as? String)!, font: UIFont.systemFont(ofSize: 12), width: (UIScreen.main.bounds.size.width/2) - 13)))
                textLbl.backgroundColor = UIColor.clear
                textLbl.text = temmmp.object(forKey: "text") as? String
                textLbl.font = UIFont.systemFont(ofSize: 12)
                textLbl.numberOfLines = 0
                
                sepcView.addSubview(nameLbl)
                sepcView.addSubview(textLbl)
                sepcView.addSubview(colonLbl)
                
                coordinateY = coordinateY + max(nameLbl.frame.size.height, textLbl.frame.size.height) + 5
            }
            
            sepcView.frame = CGRect(x: 0, y: detailY-15, width: UIScreen.main.bounds.size.width, height: coordinateY)
            sepcView.backgroundColor = UIColor.clear
            self.mainScrollView.addSubview(sepcView)
            
            var frame = self.colorsView.frame
            frame.origin.y = sepcView.frame.origin.y + sepcView.frame.size.height + 10
            self.colorsView.frame = frame
            
            print(self.colorsView.frame.size.height)
            
            self.mainScrollView.addSubview(self.colorsView)
            
            descriptionY = frame.origin.y + frame.size.height
            
            print(descriptionY)
        }
    }*/

}
