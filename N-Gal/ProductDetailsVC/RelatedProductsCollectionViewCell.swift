//
//  RelatedProductsCollectionViewCell.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 08/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class RelatedProductsCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet var splPriceLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }
}


