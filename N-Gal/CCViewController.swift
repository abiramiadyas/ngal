//
//  CCViewController.swift
//  N-Gal
//
//  Created by Adyas on 01/03/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class CCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate
{
    var payOptions:NSMutableArray = []
    var cellIdentifiers:NSMutableArray = []
    var arrayForBool:NSMutableArray = []
    var cardsList:NSMutableArray = []
    var collapsed = Bool()
    var cardDic:NSMutableDictionary = [:]
    var bankName = String()
    var cvvNo:String = ""
    var cardNumber:String = ""
    var month:String = ""
    var year:String = ""
    var cardNameHolder: String = ""
    var payOptionId: String = ""
    var cardListstr:String = ""
    var payment: String = ""
    var  dataAcceptedAt = ""
    var cardTypeStr = ""
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        payOptions.removeLastObject()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        for i in 0 ..< payOptions.count
        {
            self.arrayForBool.insert(NSNumber(value: false as Bool), at: i)
            cellIdentifiers.add((payOptions.object(at: i) as AnyObject).value(forKey: "payOptDesc") as! String)
            
            let arrayString = (payOptions.object(at: i) as AnyObject).value(forKey: "cardsList") as! NSString
                let data:Data = arrayString.data(using: String.Encoding.utf8.rawValue)!
            do
            {
                let array = try JSONSerialization.jsonObject(with: data, options: []) as! NSArray
                cardsList.insert(array, at: i)
            }
            catch
            {
                
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.payOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PayTypeCell") as! CustomCell
        
        cell.lblName.text = (payOptions.object(at: indexPath.row) as AnyObject).value(forKey: "payOptDesc") as? String

       /* else
        {
            print("payOptions: \(payOptions)")
            let identifier = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "payOptDesc") as? String
            if identifier == "Credit Card" && arrayForBool[indexPath.section] as! Bool == true{
                
              payment = "Credit Card"
                let payOpt = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "payOpt") as? String
                
                cardListstr = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "cardsList") as! String
                
                payOptionId = payOpt!
                print("payOptionId : \(payOptionId)")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreditCardCell") as! CustomCell
                
                cell.bankNameBtn.addTarget(self, action: #selector(CCViewController.showCardsList(_:)), for: UIControlEvents.touchUpInside)
                cell.bankNameTxt.text = bankName
                cell.bankNameBtn.tag = indexPath.section;
                bankName = cell.bankNameTxt.text!
                cvvNo = cell.cvvTxt.text!
                cardNumber = cell.ccCardNumberTxt.text!
                month = cell.monthTxt.text!
                year = cell.yearTxt.text!
                cardNameHolder = cell.customerName.text!
                
                cell.cvvTxt.tag = indexPath.section;
                cell.payBtn.addTarget(self, action: #selector(CCViewController.pay(_:)) , for: UIControlEvents.touchUpInside)
                cell.payBtn.tag = indexPath.section;
                return cell
            }else if identifier == "Debit Card" && arrayForBool[indexPath.section] as! Bool == true{
                
                payment = "Debit Card"
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreditCardCell") as! CustomCell
                cell.bankNameBtn.tag = indexPath.section;
                let payOpt = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "payOpt") as? String
                payOptionId = payOpt!
     
                cell.bankNameBtn.addTarget(self, action: #selector(CCViewController.showCardsList(_:)), for: UIControlEvents.touchUpInside)
                cell.bankNameTxt.text = bankName
                cell.bankNameBtn.tag = indexPath.section;
                
                bankName = cell.bankNameTxt.text!
                cvvNo = cell.cvvTxt.text!
                cardNumber = cell.ccCardNumberTxt.text!
                month = cell.monthTxt.text!
                year = cell.yearTxt.text!
                cardNameHolder = cell.customerName.text!
                cell.cvvTxt.tag = indexPath.section;
                cell.payBtn.addTarget(self, action: #selector(CCViewController.pay(_:)) , for: UIControlEvents.touchUpInside)
                cell.payBtn.tag = indexPath.section;
                return cell
            }else if identifier == "Net Banking" && arrayForBool[indexPath.section] as! Bool == true{
                
                payment = "Net Banking"
                let cell = tableView.dequeueReusableCell(withIdentifier: "NetBankingCell") as! CustomCell
                
                let payOpt = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "payOpt") as? String
                
                payOptionId = payOpt!
                
                cell.netBankingBtn.addTarget(self, action: #selector(CCViewController.showCardsList(_:)), for: UIControlEvents.touchUpInside)
                bankName = cell.netBankTxt.text!
                cell.netBankingBtn.tag = indexPath.section;
                return cell
            }else if identifier == "Cash card" && arrayForBool[indexPath.section] as! Bool == true{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CashCardCell") as! CustomCell
                payment = "Cash card"
                let payOpt = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "payOpt") as? String
                
                payOptionId = payOpt!
                
                cell.cashCardBtn.addTarget(self, action: #selector(CCViewController.showCardsList(_:)), for: UIControlEvents.touchUpInside)
                cell.cashCardBtn.tag = indexPath.section;
                bankName = cell.cashCardTxt.text!

                
                return cell
            }else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell") as! CustomCell
                cell.walletBtn.addTarget(self, action: #selector(CCViewController.showCardsList(_:)), for: UIControlEvents.touchUpInside)
                let payOpt = (payOptions.object(at: indexPath.section) as AnyObject).value(forKey: "payOpt") as? String
                
                payOptionId = payOpt!
                cell.walletBtn.tag = indexPath.section;
                bankName = cell.walletTxt.text!

                
                return cell
            }
            
        }*/
       // cell?.textLabel?.textColor = UIColor.black
        cell.accessoryType = UITableViewCellAccessoryType.none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SharedManager.showHUD(viewController: self)
        
        let name = (payOptions.object(at: indexPath.row) as AnyObject).value(forKey: "payOptDesc") as? String
        
        payment = name!
        
        print(((cardsList.object(at: indexPath.row) as AnyObject).value(forKey: "cardType") as AnyObject).object(at: 0))
        print(((cardsList.object(at: indexPath.row) as AnyObject).value(forKey: "dataAcceptedAt") as AnyObject).object(at: 0))
        
        cardTypeStr = ((cardsList.object(at: indexPath.row) as AnyObject).value(forKey: "cardType") as AnyObject).object(at: 0) as! String
        dataAcceptedAt = ((cardsList.object(at: indexPath.row) as AnyObject).value(forKey: "dataAcceptedAt") as AnyObject).object(at: 0) as! String
        
        let payOpt = (payOptions.object(at: indexPath.row) as AnyObject).value(forKey: "payOpt") as? String
        payOptionId = payOpt!
        
        continueToPay(cardType: name!)
    }

    func showCardsList( _ sender: UIButton)
    {
        let _:IndexPath = IndexPath(row: sender.tag, section: 0)
      //  let cell = self.tableView.cellForRow(at: index) as! CustomCell

        let array = cardsList.object(at: sender.tag) as! NSArray
        
        var cardNames = [String]()
        
         var accessTypecardType = [String]()
        var cardType = [String]()
        let accTyp:NSMutableArray = []
        let cardTy:NSMutableArray = []
        
         for i in 0 ..< array.count
         {
            cardNames.append((array.object(at: i) as AnyObject).value(forKey: "cardName") as! String)
            accessTypecardType.append((array.object(at: i) as AnyObject).value(forKey: "dataAcceptedAt") as! String)
            cardType.append((array.object(at: i) as AnyObject).value(forKey: "cardType") as! String)
            }
        accTyp.addObjects(from: accessTypecardType)
        cardTy.addObjects(from: cardType)

        let picker = BSModalPickerView.init(values: cardNames)
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice {
                self.bankName = (picker?.selectedValue)!
                let indexValue = (picker?.selectedIndex)

                 for i in 0 ..< accTyp.count
                 {
                 
                    let index:Int  = Int(indexValue!)
                    
                   

                    if (index == i)
                    {
                        self.dataAcceptedAt = accTyp.object(at: i) as! String
                        self.cardTypeStr = cardTy.object(at: i) as! String
                    }
                  }
                
                cardNames.removeAll()
            }
            else {
                print("You cancelled the picker")
            }
        })
    }

    func continueToPay(cardType: String)
    {
        let shipping_FirstName = shippingDict.object(forKey: "shipping_firstname") as! String
        // print(" shipping_FirstName: \(shipping_FirstName) ")
        
        let shipping_address_1 = shippingDict.object(forKey: "shipping_address_1") as! String
        
        let shipping_city =  shippingDict.object(forKey: "shipping_city") as! String
        
        let shipping_zone = shippingDict.object(forKey: "shipping_zone") as! String
        
        let shipping_country = shippingDict.object(forKey: "shipping_country") as! String
        
        let shipping_telephone =  shippingDict.object(forKey: "shipping_telephone") as! String
        
        let shipping_postcode =  shippingDict.object(forKey: "shipping_postcode") as! String
        
        let shipping_email =  shippingDict.object(forKey: "shipping_email") as! String
        
        let accesscode = "AVGO01BD32AF03OGFA"
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
        
        viewController.cardName =  ""
        viewController.payOptId =  payOptionId
        viewController.expiryMonth = ""
        viewController .expiryYear = ""
        viewController.cardCVV = ""
        viewController.issuingBank = ""
        viewController.accessCode = accesscode
        viewController.currency = "INR"
        viewController.redirectUrl = "http://www.n-gal.com/ccavResponseHandler.php"
        viewController.cancelUrl = "http://www.n-gal.com/ccavResponseHandler.php"
        viewController.rsaKeyUrl = "https://www.n-gal.com/GetRSA.php"
        viewController.mobileNo = shipping_telephone
        viewController.billingName = shipping_FirstName
        viewController.billingAddress = shipping_address_1
        viewController.billingCountry = shipping_country
        viewController.billingState = shipping_zone
        viewController.billingCountry = shipping_country
        viewController.billingCity = shipping_city
        viewController.billingTel = shipping_telephone
        viewController.deliveryName = shipping_FirstName
        viewController.deliveryAddress = shipping_address_1
        viewController.deliveryCountry = shipping_country
        viewController.deliveryState = shipping_zone
        viewController.deliveryCity =  shipping_city
        viewController.deliveryTel = shipping_telephone
        viewController.mobileNo = shipping_telephone
        viewController.orderId = orderID
        viewController.amount = price
        viewController.billingZipCode = shipping_postcode
        viewController.deliveryZipCode = shipping_postcode
      //  viewController.emiPlanId = shipping_email
      //  viewController.emiTenureId = shipping_email
        viewController.billingEmail = shipping_email
        viewController.dataAcceptedAt = "CCAvenue"
        viewController.cardType = cardTypeStr
        viewController.paymentType = payment
        
        SharedManager.dismissHUD(viewController: self)
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
