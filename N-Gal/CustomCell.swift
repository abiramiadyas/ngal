//
//  CustomCell.swift
//  N-Gal
//
//  Created by Adyas on 01/03/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell , UITextFieldDelegate{

    
    @IBOutlet var ccCardNumberTxt: UITextField!
    @IBOutlet var monthTxt: UITextField!
    @IBOutlet var yearTxt: UITextField!
    @IBOutlet var cvvTxt: UITextField!
    @IBOutlet var bankNameTxt: UITextField!
    
    @IBOutlet weak var customerName: UITextField!
    

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet var bankNameBtn: UIButton!
    
    @IBOutlet var payBtn: UIButton!
    
    @IBOutlet var netBankingBtn: UIButton!
    @IBOutlet var netBankTxt: UITextField!
    
    @IBOutlet var cashCardBtn: UIButton!
    @IBOutlet var cashCardTxt: UITextField!
    
    @IBOutlet var walletBtn: UIButton!
    @IBOutlet var walletTxt: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  /*  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if (ccCardNumberTxt.text == textField.text)
        {
            let maxLength = 16
            let currentString: NSString = ccCardNumberTxt.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else  if (monthTxt.text == textField.text)
        {
            let maxLength = 2
            let currentString: NSString = monthTxt.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else  if (yearTxt.text == textField.text)
        {
            
            let maxLength = 4
            let currentString: NSString = yearTxt.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else  if (cvvTxt.text == textField.text)
        {
            let maxLength = 3
            let currentString: NSString = cvvTxt.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
       
       return (textField.text != nil)
    }    */
}
