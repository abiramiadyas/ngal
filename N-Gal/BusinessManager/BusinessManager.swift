//
//  BusinessManager.swift
//  E-Commerce
//  Created by Apple on 24/06/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

import UIKit

var baseUrl:NSString = "https://www.n-gal.com/mobile-app/api.php?rquest="
class BusinessManager: NSObject {
    
    class func loginWith(_ userID:String , password:String , deviceToken: String, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"username=%@&password=%@&device_type=2&device_id=%@", userID,password, deviceToken) as String, urlString: "\(baseUrl)login") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    //registration
    class func registerUserWithValues(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)registration") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func updateProfileInformationWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)update_customer_address") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func forgotPasswod(_ emailID:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"mailID=%@", emailID) as String, urlString: "\(baseUrl)forgotpassword") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getSubCategoryList(_ languageID:String, subCategoryID: String,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getcategories&language_id=%@&category_id=%@",baseUrl,languageID,subCategoryID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
            
        }
    }

    class func getCategoryList(_ languageID:String, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getcategories&language_id=%@",baseUrl,languageID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
        
    class func getFeaturedCategoryList(_ languageID:String,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getfeatureproduct&language_id=%@",baseUrl,languageID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getLatestCategoryList(_ languageID:String,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getlatestproduct&language_id=%@", baseUrl,languageID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getSpecailCategoryList(_ languageID:String,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getspecialsproduct&language_id=%@", baseUrl,languageID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getBannerImages(_ languageID:String,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@get_slideshow&language_id=%@", baseUrl,languageID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }

    
    class func getProductList(_ categoryID:String, languageID:String, page:Int, filters:String, limit:String, sort: String, order: String, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getproducts&language_id=%@&category_id=%@&page=%d&filters=%@&limit=%@&sort=%@&order=%@", baseUrl,languageID,categoryID,page,filters,limit,sort,order)
              let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getSortedProductList(_ categoryID:String, languageID:String, page:Int, limit:String, sort:String, order:String, filters:String, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getproducts&language_id=%@&category_id=%@&page=%d&sort=%@&order=%@&limit=%@&filters=%@", baseUrl,languageID,categoryID,page,sort,order,limit,filters)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getRelatedProducts(_ productID:String, languageID:String, page:Int, limit:String, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getrelatedproduct&language_id=%@&product_id=%@&limit=%@&page=%d", baseUrl,languageID,productID,limit,page)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getProductDetails(_ productID:String ,userID:String, languageID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getproduct&language_id=%@&product_id=%@&user_id=%@",baseUrl,languageID,productID,userID)
                       
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getFilterProducts(_ categoryID:String, languageID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getfilters&language_id=1&category_id=%@",baseUrl,categoryID, languageID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    
    class func postReview(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)post_review") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    
    class func getSearchResult(_ filterName:String ,languageId:String ,userid:String,limit:String, page:Int, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getproducts&language_id=%@&filter_name=%@&user_id=%@&limit=%@&page=%d",baseUrl,languageId,filterName,userid,limit,page)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }

    class func getProfileInformation(_ userID:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@get_customer_address&customer_id=%@",baseUrl,userID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }

    class func getWishList(_ userID:String ,languageID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@get_wishlist&user_id=%@&language_id=%@",baseUrl,userID, languageID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func addToWishListWithValues(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)update_wishlist") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func removeFromWishListWithValues(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)remove_wishlist") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func GetShippingMethod(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)getshipping_method") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func GetPaymentMethod(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)getpayment_method") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getCartProducts(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)get_cart_products") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func validateCouponWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)validatecoupon") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func validateVoucherWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)validatevoucher") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func checkDelivery(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)checkpin") as NSDictionary

            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    
    class func confirmOrderWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)confirmorder") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
  
    class func returnProductWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let trimValues = (NSString(format:"%@", values) as String).replacingOccurrences(of: " \"", with: "", options: .literal, range: nil)
            
            let result = DownloadManager.downloadDataFromEncodedServer(NSString(format:"%@", trimValues) as String, urlString: "\(baseUrl)save_return") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getPaymentOptions(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "https://secure.ccavenue.com/transaction/transaction.do") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func placeOrderWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)place_order") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getMyRewardsList(_ userID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getcustomer_rewards&customer_id=%@",baseUrl,userID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getMyCreditsList(_ userID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getcustomer_credits&customer_id=%@",baseUrl,userID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getMyReturnsList(_ userID:String ,languageID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getreturns&customer_id=%@&language_id=%@",baseUrl,userID,languageID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getReturnReasonList(_ languageID:String,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getreturn_reason&language_id=%@",baseUrl,languageID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getMyOrderList(_ userID:String ,languageID:String ,completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getcustomer_order&customer_id=%@&language_id=%@",baseUrl,userID,languageID)
            
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getCountryStateList(_ completionHandler:@escaping (_ result:NSArray)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@country",baseUrl)
            let result = DownloadManager.downloadDataFromServerGetMethodArray(url as String) as NSArray
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    

    class func getStaticPagesCOntent(_ infoID:String,langID:String, completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "%@getinformation&info_id=%@&language_id=%@",baseUrl,infoID,langID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func getAddressList(_ userID:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "\(baseUrl)get_customer_address&customer_id=%@" as NSString,userID)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func addNewAddressWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)insert_customer_address") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func updateAddressWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)update_customer_address") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func removeAddressWith(_ userID:String, addressId:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let url = NSString(format: "\(baseUrl)delete_customer_address&customer_id=%@&address_id=%@" as NSString,userID,addressId)
            let result = DownloadManager.downloadDataFromServerGetMethod(url as String) as NSDictionary
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    class func updateAccountInformationWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)update_account_detail") as NSDictionary
            
            DispatchQueue.main.async
                {
                    completionHandler(result)
            }
        }
    }
    
    class func submitContactFormWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)contactus") as NSDictionary
            
            DispatchQueue.main.async
                {
                    completionHandler(result)
            }
        }
    }
    
    class func changePasswordWith(_ values:String , completionHandler:@escaping (_ result:NSDictionary)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let result = DownloadManager.downloadDataFromServer(NSString(format:"%@", values) as String, urlString: "\(baseUrl)change_password") as NSDictionary
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
}
