	
//  DownloadManager.swift
//  E-Commerce
//
//  Created by Apple on 24/06/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

import UIKit

class DownloadManager: NSObject {
  
    class func downloadDataFromServer(_ parameter:String , urlString: String) ->NSDictionary
    {
     
       // request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type");

        var request = NSMutableURLRequest()
        request.url = URL(string: urlString)
        
        request = NSMutableURLRequest(url: URL(string: urlString)!, cachePolicy:.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 25)
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField:"Content-Disposition")
        request.httpMethod = "POST"
        
        
        let data = (parameter as NSString).data(using: String.Encoding.utf8.rawValue)
        request.httpBody = data
        
        var Error:NSError? = nil

        var returnData: Data?
        do {
            returnData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        } catch let error as NSError {
            
            Error = error
            returnData = nil
        }
        
        if(Error == nil)
        {
            return self.validateResponds(returnData!)
        }
        let errorResult:NSDictionary = [
            "status" : "401",
            "message" : "Error message"
        ]

        return errorResult
    }
    
    class func downloadDataFromEncodedServer(_ parameter:String , urlString: String) ->NSDictionary
    {
        var request = NSMutableURLRequest()
        request.url = URL(string: urlString)
        
        request = NSMutableURLRequest(url: URL(string: urlString)!, cachePolicy:.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 25)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type");
       // request.setValue("application/json; charset=utf-8", forHTTPHeaderField:"Content-Disposition")
        request.httpMethod = "POST"
        
        
        let data = (parameter as NSString).data(using: String.Encoding.utf8.rawValue)
        request.httpBody = data
        
        var Error:NSError? = nil
        
        var returnData: Data?
        do {
            returnData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        } catch let error as NSError {
            
            Error = error
            returnData = nil
        }
        
        if(Error == nil)
        {
            return self.validateResponds(returnData!)
        }
        let errorResult:NSDictionary = [
            "status" : "401",
            "message" : "Error message"
        ]
        
        return errorResult
    }
    
    /*
    class func downloadDataFromServerGetMethod(_ urlString: String) ->NSDictionary
    {
        let urlString: String! = urlString
        let request: NSMutableURLRequest = NSMutableURLRequest()
        let escapedString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        request.url = URL(string: escapedString)
        request.httpMethod = "GET"
        var Error:NSError? = nil
        var returnData: Data?
        do {
            returnData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        } catch let error as NSError {
            Error = error
            returnData = nil
        }
        return self.validateResponds(returnData!)
    }
    
    */
    
    class func downloadDataFromServerGetMethod(_ urlString: String) ->NSDictionary
    {
        let urlString: String! = urlString
        var request: NSMutableURLRequest = NSMutableURLRequest()
        
        let escapedString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        request = NSMutableURLRequest(url: URL(string: escapedString)!, cachePolicy:.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 25)
        
        request.url = URL(string: escapedString)
        request.httpMethod = "GET"
        var Error:NSError? = nil
        var returnData: Data?
        do {
            returnData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        } catch let error as NSError {
            Error = error
            returnData = nil
        }
        return self.validateResponds(returnData!)
    }
    
    class func validateResponds(_ data:Data) ->NSDictionary
    {
        var dictData: NSDictionary?
        var jsonError:NSError? = nil
        do{
            if  let dict  = try  JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
            {
                // print(dict)
                
                dictData = dict
                return dictData!
            }
            else {
                let resultString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                print("Flawed JSON String: \(String(describing: resultString))")
                let errorResult:NSDictionary = [
                    "httpCode" : 401,
                    "message" : "Server Error, Please Try again"
                ]
                return errorResult
            }
        }
        catch let error as NSError
        {
            print(error.description)
            print(error.debugDescription)
            dictData = ["status":"401","message":"\(error.localizedDescription)"]
            
        }
        return dictData!
    }

    /*
    class func validateResponds(_ data:Data) ->NSDictionary
    {
        var dictData: NSDictionary?
        var jsonError:NSError? = nil
        do{
            if  let dict  = try  JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            {
                dictData = dict
                print("dictData: \(dictData)")
                return dictData!
            }
            else {
                let resultString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                print("Flawed JSON String: \(resultString)")
                let errorResult:NSDictionary = [
                    "httpCode" : 401,
                    "message" : "Error message"
                ]
                return errorResult
            }
        }
            

        catch let error as NSError
        {
            let dict = ["status":"400", "message":"\(error.description)"]
            dictData = dict as NSDictionary?
        }
        return dictData!
    }
    
    
    */
    
    class func downloadDataFromServerGetMethodArray(_ urlString: String) ->NSArray
    {
        let urlString: String! = urlString
        let url = NSURL(string: urlString)!
        var request: NSMutableURLRequest = NSMutableURLRequest()
        
        request = NSMutableURLRequest(url: url as URL, cachePolicy:.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 25)
        
        let escapedString:String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        request.url = URL(string: escapedString)
        request.httpMethod = "GET"
        var Error:NSError? = nil
        var returnData: Data?
        do {
            returnData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        } catch let error as NSError {
            Error = error
            returnData = nil
        }
        return self.validateRespondsArray(returnData!)
    }
    class func validateRespondsArray(_ data:Data) -> NSArray
    {
        var dictData: NSArray?
        //   var jsonError:NSError? = nil
        do{
            if  let dict  = try  JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray
            {
                dictData = dict
                  return dictData!
            }
                
            else {
                let resultString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                print("Flawed JSON String: \(resultString)")
                let errorResult:NSArray = ["Error message"]
                return errorResult
            }
        }
            
            
        catch let error as NSError
        {
            let dict = [error.description]
            dictData = dict as NSArray?
        }
        return dictData!
    }
    
}
