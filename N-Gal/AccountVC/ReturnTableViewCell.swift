//
//  ReturnTableViewCell.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 08/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class ReturnTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblorderId: UILabel!
    @IBOutlet weak var lblReturnId: UILabel!
    @IBOutlet weak var lblReturnType: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
