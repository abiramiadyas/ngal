//
//  ReturnViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 08/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class ReturnViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var lblEmptyMsg: UILabel!
    @IBOutlet weak var tblReturns: UITableView!
    
   // var creditTypeStr = ""
    var myReturnsArr:NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()

        SharedManager.showHUD(viewController: self)
        BusinessManager.getMyReturnsList(userIDStr, languageID: languageID, completionHandler: {(result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            
            self.myReturnsArr = result.object(forKey: "return_list") as! NSMutableArray
            
            
            if self.myReturnsArr.count != 0
            {
                self.tblReturns.isHidden = false
                self.lblEmptyMsg.isHidden = true
                
                self.tblReturns.reloadData()
            }
            else
            {
                self.tblReturns.isHidden = true
                self.lblEmptyMsg.isHidden = false
            }
        });
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return self.myReturnsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 176
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:ReturnTableViewCell = self.tblReturns.dequeueReusableCell(withIdentifier: "returnCell") as! ReturnTableViewCell
        
        cell.lblDate.text = (self.myReturnsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "date_added") as? String
        cell.lblStatus.text = (self.myReturnsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "status") as? String
        cell.lblorderId.text = (self.myReturnsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "order_id") as? String
        cell.lblReturnId.text = (self.myReturnsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "return_id") as? String
       // cell.lblReturnType.text = (self.myReturnsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "description") as? String
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }

}
