//
//  AccountVC.swift
//  Exlcart
//
//  Created by iPhone on 22/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class AccountVC: ParentViewController,UITabBarControllerDelegate,loginIntimation {

    @IBOutlet weak var contactMsgTxt: UITextField!
    @IBOutlet weak var contactMobileTxt: UITextField!
    @IBOutlet weak var contactEmailTxt: UITextField!
    @IBOutlet weak var contactNameTxt: UITextField!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var editAcView: UIView!
    @IBOutlet weak var extrasView: UIView!
    @IBOutlet weak var myOrderView: UIView!
    @IBOutlet weak var myAcView: UIView!
    @IBOutlet weak var changePassView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var phoneNumberTxt: UITextField!
    @IBOutlet weak var companyTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var streetTxt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet var stateBtn: UIButton!
    @IBOutlet weak var newPasswordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var notificationSwitch: UISwitch!
    var isFromDidLoad:Bool = false
    
    var profileDetailsAry:NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        self.setLeftNavigationButton()
        scrollView.autoresizesSubviews = true
        
        let saveBtn: UIButton = UIButton(type: UIButtonType.custom)
        saveBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 25)
        saveBtn.layer.borderWidth = 1.0
        saveBtn.layer.borderColor = UIColor.white.cgColor
        saveBtn.layer.cornerRadius = 14.0
        saveBtn.setTitle("SAVE", for: UIControlState())
        saveBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        saveBtn.addTarget(self, action: #selector(AccountVC.save), for: UIControlEvents.touchUpInside)
        
        //let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: saveBtn)
       // self.navigationItem.setRightBarButton(rightBarButtonItem, animated: false)

     //   NotificationCenter.default.addObserver(self, selector: #selector(AccountVC.refreshList(notification:)), name: NSNotification.Name(rawValue: "getName"), object: nil)

    }
    
    func loginSuccess() {
        
        isFromDidLoad = false
       self.showData()
    }
    
    func loginFailure() {
        
        isFromDidLoad = false
        self.tabBarController?.selectedIndex = 0
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let tabbarObj:UITabBarController = self.tabBarController!
        tabbarObj.delegate=self
        
        contactView.isHidden = true
        editAcView.isHidden = true
        emailTxt.isEnabled = false
        changePassView.isHidden = true
        
        let height = self.myAcView.frame.size.height + self.myOrderView.frame.size.height + self.extrasView.frame.size.height + 90
        
        self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: height);
        
        if(UserDefaults.standard.object(forKey: kUserDetails) == nil && isFromDidLoad == false)
        {
            isFromDidLoad = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            viewController.delegate = self
            
            self.present(viewController, animated: false, completion: { () -> Void in
            })
        }
        else
        {
            self.showData()
        }
        
        self.myAcView.layer.shadowColor = UIColor.black.cgColor
        self.myAcView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.myAcView.layer.shadowOpacity = 0.7
        self.myAcView.layer.shadowRadius = 4.0
        
        self.myOrderView.layer.shadowColor = UIColor.black.cgColor
        self.myOrderView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.myOrderView.layer.shadowOpacity = 0.7
        self.myOrderView.layer.shadowRadius = 4.0
        
        self.extrasView.layer.shadowColor = UIColor.black.cgColor
        self.extrasView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.extrasView.layer.shadowOpacity = 0.7
        self.extrasView.layer.shadowRadius = 4.0
        
    }
     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        _ = self.navigationController?.popToRootViewController(animated: false)
        
    }
 
    func showData()
    {
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let userDic = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            userIDStr = userDic.value(forKeyPath: "customer.customer_id") as! String
            isFromDidLoad = true
            
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.getProfileInformation(userIDStr, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                self.profileDetailsAry = result.value(forKey: "customer_address") as! NSMutableArray
                if(self.profileDetailsAry.count != 0)
                {
                    self.firstNameTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "firstname") as? String
                    self.lastNameTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "lastname") as? String
                    self.phoneNumberTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "telephone") as? String
                  //  self.companyTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "company") as? String
                 //   self.cityTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "city") as? String
                 //   self.streetTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_1") as? String
                 //   self.zipCodeTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "postcode") as? String
                    self.emailTxt.text = (self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "email") as? String
                  //  self.countryBtn.setTitle((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "country") as? String, for: .normal)
                 //   self.stateBtn.setTitle((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "state") as? String, for: .normal)
                 //   countryID = (self.profileDetailsAry.object(at: 0) as! NSDictionary).value(forKey: "country_id") as! NSString
                 //   stateID = (self.profileDetailsAry.object(at: 0) as! NSDictionary).value(forKey: "zone_id") as! NSString
                }
                
            })
            
        }

    }

    func save()
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.phoneNumberTxt.hasText && self.companyTxt.hasText && self.cityTxt.hasText && self.streetTxt.hasText && self.zipCodeTxt.hasText && self.stateBtn.currentTitle?.characters.count != 0 && self.countryBtn.currentTitle?.characters.count != 0)
        {
            var value:String = String()
            value = NSString(format: "address_id=%@&customer_id=%@&firstname=%@&lastname=%@&company=%@&address_1=%@&city=%@&postcode=%@&country=%@&state=%@",((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_id") as? String)!,((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "customer_id") as? String)!,self.firstNameTxt.text!,self.lastNameTxt.text!,self.companyTxt.text!,self.streetTxt.text!,self.cityTxt.text!,self.zipCodeTxt.text!,countryID,stateID) as String

            print("value: \(value)")
            SharedManager.showHUD(viewController: self)
            BusinessManager.updateProfileInformationWith(value, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "Message") as! String, viewController: self)
                }
                else{
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyTextFieldAlertMessage, viewController: self)
        }
    }
    
    @IBAction func myRewardsAction(_ sender: AnyObject)
    {
        creditTypeStr = "rewards"
    }
    
    @IBAction func myCreditsAction(_ sender: AnyObject)
    {
        creditTypeStr = "credits"
    }
    
    @IBAction func cancelChangePasswordAction(_ sender: AnyObject)
    {
        contactView.isHidden = true
        editAcView.isHidden = true
        changePassView.isHidden = true
        
        newPasswordTxt.text = ""
        confirmPasswordTxt.text = ""
        
        contactNameTxt.text = ""
        contactMsgTxt.text = ""
        contactEmailTxt.text = ""
        contactMobileTxt.text = ""
    }
    
    @IBAction func doneChangePasswordAction(_ sender: AnyObject)
    {
        if(newPasswordTxt.hasText && confirmPasswordTxt.hasText)
        {
            let values:String = NSString(format: "password=%@&confirm=%@&customer_id=%@",self.newPasswordTxt.text!,self.confirmPasswordTxt.text!,userIDStr) as String
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.changePasswordWith(values, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! NSString
                if Int(code as String) == 200
                {
                    let alt: UIAlertController = UIAlertController(title: "Information", message: result.value(forKey: "Message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alt.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        self.changePassView.isHidden = true
                    }))
                    self.present(alt, animated: true, completion: nil)
                    self.newPasswordTxt.text = ""
                    self.confirmPasswordTxt.text = ""
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKey: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
             SharedManager.showAlertWithMessage(title: "Information", alertMessage:  kEmptyTextFieldAlertMessage, viewController: self)
        }
    }
    
    @IBAction func changePasswordAction(_ sender: AnyObject)
    {
        changePassView.isHidden = false
        changePassView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickEditAcDone(_ sender: Any)
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.phoneNumberTxt.hasText)
        {
            
            let mainDict:NSMutableDictionary = NSMutableDictionary()
            
            mainDict.setObject(userIDStr, forKey: "customer_id" as NSCopying)
            mainDict.setObject(self.firstNameTxt.text!, forKey: "firstname" as NSCopying)
            mainDict.setObject(self.lastNameTxt.text!, forKey: "lastname" as NSCopying)
            mainDict.setObject(self.emailTxt.text!, forKey: "email" as NSCopying)
            mainDict.setObject(self.phoneNumberTxt.text!, forKey: "phone" as NSCopying)
            
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: mainDict ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData!,
                                       encoding: String.Encoding.utf8.rawValue)
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.updateAccountInformationWith(theJSONText as! String, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    let alt: UIAlertController = UIAlertController(title: "Information", message: result.value(forKey: "Message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alt.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        self.editAcView.isHidden = true
                    }))
                    self.present(alt, animated: true, completion: nil)
                }
                else{
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyTextFieldAlertMessage, viewController: self)
        }
    }
    
    @IBAction func clickEditAccount(_ sender: Any)
    {
        editAcView.isHidden = false
        editAcView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickContactUs(_ sender: Any)
    {
        contactView.isHidden = false
        contactView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickContactDone(_ sender: Any)
    {
        if(self.contactNameTxt.hasText && self.contactMsgTxt.hasText && self.contactEmailTxt.hasText && self.contactMobileTxt.hasText)
        {
            
            let mainDict:NSMutableDictionary = NSMutableDictionary()
            
            mainDict.setObject(self.contactNameTxt.text!, forKey: "firstname" as NSCopying)
            mainDict.setObject(self.contactMsgTxt.text!, forKey: "message" as NSCopying)
            mainDict.setObject(self.contactEmailTxt.text!, forKey: "email" as NSCopying)
            mainDict.setObject(self.contactMobileTxt.text!, forKey: "phone" as NSCopying)
            
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: mainDict ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData!,
                                       encoding: String.Encoding.utf8.rawValue)
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.submitContactFormWith(theJSONText as! String, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    let alt: UIAlertController = UIAlertController(title: "Information", message: result.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alt.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        self.contactView.isHidden = true
                    }))
                    self.present(alt, animated: true, completion: nil)
                }
                else{
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyTextFieldAlertMessage, viewController: self)
        }
    }
    
    @IBAction func clickChangeAddress(_ sender: Any)
    {
        
    }

    @IBAction func countryAction(_ sender: AnyObject) {
        
        stateName = ""
        self.showCountryList()
    }
    
    @IBAction func stateAction(_ sender: AnyObject)
    {
        self.showStateList()
    }
    
    
    func refreshList(notification: NSNotification)
    {
       // self.countryBtn.setTitle((countryName as NSString) as String, for: .normal)
       // self.stateBtn.setTitle((stateName as NSString) as String, for: .normal)
    }
        
    @IBAction func notificationSettingsSwitch(_ sender: AnyObject)
    {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func logoutAction(_ sender: AnyObject)
    {
        isFromDidLoad = false
        UserDefaults.standard.removeObject(forKey: kUserDetails)
        UserDefaults.standard.removeObject(forKey: kAddToCart)

        UserDefaults.standard.removeObject(forKey: "COUPON")
        UserDefaults.standard.removeObject(forKey: "VOUCHER")
        UserDefaults.standard.removeObject(forKey: "REWARDS")
        
        addressIDStr = ""
        userIDStr = ""
        
        self.gotoRootViewController()
        self.updateCartBadge()

        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        self.tabBarController?.selectedIndex = 0
    }
    

}
