//
//  AddressListVC.swift
//  Exlcart
//
//  Created by Adyas Iinfotech on 13/04/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class AddressListVC: ParentViewController,UITabBarControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet weak var editAcView: UIView!
    @IBOutlet weak var myAddressTable: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var streetTxt: UITextField!
    @IBOutlet weak var address2Txt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet var stateBtn: UIButton!
    
    var typeStr: String = ""
    var addressId: String = ""
    
    var myAddressArr:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var picker: UIPickerView!
    var selectedIndex = Int()
    var pickerType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPicker.isHidden = true
        self.title = "My Address"
        
        self.editAcView.isHidden = true
      //  self.myAddressTable.isHidden = true
      //  self.myAddressTable.tableFooterView = UIView()
        
        
        
        if (UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let dic = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            
            userIDStr = dic.value(forKeyPath: "customer.customer_id") as! String
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshList(notification:)), name: NSNotification.Name(rawValue: "getName"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        SharedManager.showHUD(viewController: self)
        BusinessManager.getAddressList(userIDStr, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            if let resultArr = result.object(forKey: "customer_address")
            {
                let code = result.value(forKey: "status") as! String
                
                if code == "200"
                {
                    self.myAddressArr = result.object(forKey: "customer_address") as! NSMutableArray
                    
                    print(self.myAddressArr)
                    
                    if self.myAddressArr.count != 0
                    {
                        self.myAddressTable.isHidden = false
                        self.myAddressTable.reloadData()
                    }
                    else
                    {
                        self.myAddressTable.isHidden = true
                        
                        SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "Your address list is Empty! \n Add a new one", viewController: self)
                    }
                    
                }
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.myAddressArr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:AddressTableViewCell = self.myAddressTable.dequeueReusableCell(withIdentifier: "cell") as! AddressTableViewCell
        
        let name = NSString(format: "%@ %@",((myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "firstname") as? String!)!, ((myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "lastname") as? String!)!)
        cell.lblName.text =  name as String
        
        cell.lblAddress.text = (myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "address_1") as? String
        
        let city = NSString(format: "%@ %@",((myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "city") as? String!)!, ((myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "postcode") as? String!)!)
        cell.lblCity.text = city as String
        
        cell.lblState.text = (myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "state") as? String
        cell.lblCountry.text = (myAddressArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "country") as? String
        
        cell.btnDelete.addTarget(self, action: #selector(self.deleteAction(_:)), for: UIControlEvents.touchUpInside)
        cell.btnDelete.tag = (indexPath as NSIndexPath).row
        cell.btnEdit.addTarget(self, action: #selector(self.editAction(_:)), for: UIControlEvents.touchUpInside)
        cell.btnEdit.tag = (indexPath as NSIndexPath).row
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 177
    }
    
    func deleteAction(_ sender : UIButton)
    {
        
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let defaultAddId =  ((self.myAddressArr.object(at: index.row) as AnyObject).value(forKeyPath: "default_address_id") as AnyObject) as! String
        
        let selectedAddId = ((self.myAddressArr.object(at: index.row) as AnyObject).value(forKeyPath: "address_id") as AnyObject) as! String
        
        if selectedAddId == defaultAddId
        {
            SharedManager.showAlertWithMessage(title: "Alert", alertMessage: "Default Address Cannot be deleted", viewController: self)
        }
        else
        {
            let alt: UIAlertController = UIAlertController(title: "Information", message: "Are you sure, Do want to delete this Address?", preferredStyle: UIAlertControllerStyle.alert)
            alt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                
                SharedManager.showHUD(viewController: self)
                BusinessManager.removeAddressWith(userIDStr, addressId: selectedAddId, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    let code = result.value(forKey: "status") as! String
                    
                    if code == "200"
                    {
                        SharedManager.showAlertWithMessage(title: "Alert", alertMessage: "Successfully Deleted", viewController: self)
                        
                        self.viewWillAppear(true)
                    }
                    
                    self.myAddressTable.reloadData()
                })
                
                
            }))
            
            alt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) -> Void in
                
                alt.dismiss(animated: true)
            }))
            self.present(alt, animated: true, completion: nil)
        }
    }
    
    func editAction(_ sender : UIButton)
    {
        typeStr = "edit"
        
        self.editAcView.isHidden = false
        
        if(self.myAddressArr.count != 0)
        {
            self.firstNameTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "firstname") as? String
            self.lastNameTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "lastname") as? String
              self.cityTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "city") as? String
              self.streetTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "address_1") as? String
            self.address2Txt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "address_2") as? String
              self.zipCodeTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "postcode") as? String
              self.countryTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "country") as? String
              self.stateTxt.text = (self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKey: "state") as? String
            addressId = ((self.myAddressArr.object(at: sender.tag) as! NSDictionary).value(forKey: "address_id") as! NSString) as String
             countryID = (self.myAddressArr.object(at: sender.tag) as! NSDictionary).value(forKey: "country_id") as! NSString
              stateID = (self.myAddressArr.object(at: sender.tag) as! NSDictionary).value(forKey: "zone_id") as! NSString
        }
    }
    
    @IBAction func clickEditAcDone(_ sender: Any)
    {
        self.editAcView.isHidden = true
        
        if typeStr == "new"
        {
            self.addNew()
        }
        else
        {
            self.edit()
        }
    }
    
    @IBAction func clickCancel(_ sender: Any)
    {
        self.editAcView.isHidden = true
    }
    
    @IBAction func clickAddAddress(_ sender: Any)
    {
        typeStr = "new"
        
        self.editAcView.isHidden = false
        
        self.firstNameTxt.text = ""
        self.lastNameTxt.text = ""
        self.streetTxt.text = ""
        self.address2Txt.text = ""
        self.cityTxt.text = ""
        self.zipCodeTxt.text = ""
        self.stateTxt.text = ""
        self.countryTxt.text = ""
    }
    
    @IBAction func countryAction(_ sender: AnyObject)
    {
        stateName = ""
        self.view.endEditing(true)
        
        var countryNames = [String]()
        let country_id = NSMutableArray()
        
        for i in 0 ..< self.countryListAry.count
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: i)
            countryNames.append(name as! String)
            country_id.add((self.countryListAry.value(forKey: "country_id") as! NSArray).object(at: i) as! String)
        }
        pickerType = "country"
        
        self.viewPicker.isHidden = false
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    @IBAction func stateAction(_ sender: AnyObject)
    {
        // self.showStateList()
        self.view.endEditing(true)
        
        for i : Int in 0 ..< self.countryListAry.count
        {
            let id:String = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "country_id") as? String)!
            
            if countryID.isEqual(to: id as String)
            {
                self.stateAryList = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "state")! as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        pickerType = "state"
        
        self.viewPicker.isHidden = false
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    
    func refreshList(notification: NSNotification)
    {
        self.countryTxt.text = (countryName as NSString) as String
        self.stateTxt.text = (stateName as NSString) as String
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func addNew()
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.cityTxt.hasText && self.streetTxt.hasText && self.zipCodeTxt.hasText && self.stateTxt.hasText && self.countryTxt.hasText)
        {
            var value:String = String()
            value = NSString(format: "customer_id=%@&firstname=%@&lastname=%@&address_1=%@&address_2=%@&city=%@&postcode=%@&country=%@&state=%@",userIDStr,self.firstNameTxt.text!,self.lastNameTxt.text!,self.streetTxt.text!,self.address2Txt.text!,self.cityTxt.text!,self.zipCodeTxt.text!,countryID,stateID) as String
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.addNewAddressWith(value, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "Message") as! String, viewController: self)
                    
                    self.viewWillAppear(true)
                }
                else{
                    SharedManager.showAlertWithMessage(title: "Information", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Information", alertMessage: kEmptyTextFieldAlertMessage, viewController: self)
        }
    }
    
    func edit()
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.cityTxt.hasText && self.streetTxt.hasText && self.zipCodeTxt.hasText && self.stateTxt.hasText && self.countryTxt.hasText)
        {
            var value:String = String()
            value = NSString(format: "address_id=%@&customer_id=%@&firstname=%@&lastname=%@&address_1=%@&address_2=%@&city=%@&postcode=%@&country=%@&state=%@",addressId,userIDStr,self.firstNameTxt.text!,self.lastNameTxt.text!,self.streetTxt.text!,self.address2Txt.text!,self.cityTxt.text!,self.zipCodeTxt.text!,countryID,stateID) as String

            SharedManager.showHUD(viewController: self)
            BusinessManager.updateAddressWith(value, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    SharedManager.showAlertWithMessage(title: "Success", alertMessage: result.value(forKeyPath: "Message") as! String, viewController: self)
                    
                    self.viewWillAppear(true)
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kEmptyTextFieldAlertMessage, viewController: self)
        }
    }
    
    //MARK: PickerView Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerType == "country"
        {
            return countryListAry.count
        }
        else
        {
            return stateAryList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerType == "country"
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        else
        {
            let name = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedIndex = row
    }
    
    
    @IBAction func clickPickerCancel(_ sender: Any)
    {
        self.viewPicker.isHidden = true
    }
    
    @IBAction func clickPickerDone(_ sender: Any)
    {
        if pickerType == "country"
        {
            countryTxt.text = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            countryName = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            countryID = (self.countryListAry.value(forKey: "country_id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        else
        {
            stateTxt.text = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            stateName = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            stateID = (self.stateAryList.value(forKey: "state_id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        
        self.viewPicker.isHidden = true
    }
}
