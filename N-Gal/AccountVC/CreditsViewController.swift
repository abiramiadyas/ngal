//
//  CreditsViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 08/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class CreditsViewController: ParentViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEmptyMsg: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblBalTitle: UILabel!
    @IBOutlet weak var tblCredits: UITableView!
    
   // var creditTypeStr = String()
    var myRewardsArr:NSMutableArray = []
    var myCreditsArr:NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if creditTypeStr == "rewards"
        {
            SharedManager.showHUD(viewController: self)
            BusinessManager.getMyRewardsList(userIDStr, completionHandler: {(result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                self.myRewardsArr = result.object(forKey: "orders") as! NSMutableArray
                
                if self.myRewardsArr.count != 0
                {
                    self.tblCredits.isHidden = false
                    self.lblEmptyMsg.isHidden = true
                    self.lblTitle.text = "YOUR REWARD POINTS"
                    self.lblBalance.text = "\(result.value(forKey: "total")!)"
                    self.lblBalTitle.text = "Your Total Reward Points are :"
                    
                    self.tblCredits.reloadData()
                }
                else
                {
                    self.tblCredits.isHidden = true
                    self.lblEmptyMsg.isHidden = false
                    self.lblEmptyMsg.text = "You do not have any Reward Points!"
                }
                self.lblBalTitle.sizeToFit()
                self.lblBalance.frame.origin.x = self.lblBalTitle.frame.origin.x + self.lblBalTitle.frame.size.width + 10
            });
        }
        else
        {
            SharedManager.showHUD(viewController: self)
            BusinessManager.getMyCreditsList(userIDStr, completionHandler: {(result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                self.myCreditsArr = result.object(forKey: "orders") as! NSMutableArray
                
                if self.myCreditsArr.count != 0
                {
                    self.tblCredits.isHidden = false
                    self.lblEmptyMsg.isHidden = true
                    self.lblTitle.text = "YOUR TRANSACTIONS"
                    self.lblBalance.text = result.value(forKey: "total") as? String
                    self.lblBalTitle.text = "Your current Balance is :"
                    
                    self.tblCredits.reloadData()
                }
                else
                {
                    self.tblCredits.isHidden = true
                    self.lblEmptyMsg.isHidden = false
                    self.lblEmptyMsg.text = "You do not have any transactions!"
                }
                
                self.lblBalTitle.sizeToFit()
                self.lblBalance.frame.origin.x = self.lblBalTitle.frame.origin.x + self.lblBalTitle.frame.size.width + 10
            });
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if creditTypeStr == "rewards"
        {
            return self.myRewardsArr.count
        }
        else
        {
            return self.myCreditsArr.count
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 108
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:CreditsTableViewCell = self.tblCredits.dequeueReusableCell(withIdentifier: "creditCell") as! CreditsTableViewCell
        
        if creditTypeStr == "rewards"
        {
            cell.lblDate.text = (self.myRewardsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "date_added") as? String
            cell.lblDescription.text = (self.myRewardsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "description") as? String
            cell.lblCreditTypeTitle.text = "Points"
            cell.lblCredits.text = (self.myRewardsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "points") as? String
        }
        else
        {
            cell.lblDate.text = (self.myCreditsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "date_added") as? String
            cell.lblDescription.text = (self.myCreditsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "description") as? String
            cell.lblCreditTypeTitle.text = "Amt(USD)"
            cell.lblCredits.text = (self.myCreditsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "amount") as? String
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }

}
