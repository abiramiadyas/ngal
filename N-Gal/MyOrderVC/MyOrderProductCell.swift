//
//  MyOrderProductCell.swift
//  Exlcart
//
//  Created by iPhone on 14/10/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class MyOrderProductCell: UITableViewCell {

    @IBOutlet weak var productTitleLbl: UILabel!
    
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    
    
    @IBOutlet weak var subTotalTitleLbl: UILabel!
    @IBOutlet weak var subtotalValueLbl: UILabel!
    
    @IBOutlet weak var flatShippingTitleLbl: UILabel!
    @IBOutlet weak var flatShippingLbl: UILabel!
    
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var btnReturn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
