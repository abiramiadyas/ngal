//
//  MyOrderVC.swift
//  Exlcart
//
//  Created by iPhone on 23/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class MyOrderVC: UIViewController {

    @IBOutlet weak var satusLbl: UILabel!
    @IBOutlet weak var myOrderTable: UITableView!
    var myOrderArr:NSMutableArray = NSMutableArray()
    var myOrderStatus:NSMutableArray = ["Processing","Shipped","Cancelled","Complete","Denied","Canceled Reversal","Failed","Refunded","Reversed","Chargeback","Pending","Voided","Processed","Expired"]
    var statusDic:NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Order"

        self.myOrderTable.isHidden = true
        self.myOrderTable.tableFooterView = UIView()
        
        if (UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let dic = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            
            userIDStr = dic.value(forKeyPath: "customer.customer_id") as! String
        }
        
            SharedManager.showHUD(viewController: self)
        BusinessManager.getMyOrderList(userIDStr, languageID: languageID as String) { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            let array = result.object(forKey: "orders") as! NSMutableArray
            for i : Int in 0 ..< array.count
            {
                let temp1 = (array.object(at: i) as AnyObject).value(forKey: "order_status") as! NSString
                
                for j : Int in 0 ..< self.myOrderStatus.count
                {
                    let temp2 = self.myOrderStatus.object(at: j) as! NSString
                    
                    if temp1.isEqual(to: temp2 as String)
                    {
                        let array1:NSMutableArray = []
                        array1.add(array.object(at: i))
                        
                        self.statusDic = ["Status":temp2, "OrderList":array1]
                        self.myOrderArr.add(self.statusDic)
                    }
                }
            }
            
            if self.myOrderArr.count != 0
            {
                self.myOrderTable.isHidden = false
                self.myOrderTable.reloadData()
            }
            else
            {
                self.myOrderTable.isHidden = true
            }
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return ((self.myOrderArr[section] as! NSObject).value(forKey: "OrderList")! as AnyObject).count

    }
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int
    {
        return myOrderArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:MyOrderTableViewCell = self.myOrderTable.dequeueReusableCell(withIdentifier: "cell") as! MyOrderTableViewCell
                
        cell.orderIDValueLbl.text = (((self.myOrderArr.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(forKey: "OrderList") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "order_id") as? String
        cell.orderIDValueLbl.textAlignment = NSTextAlignment.left
        
        cell.shippingAddressValueLbl.text = MyOrderVC.findAddressWith((((self.myOrderArr.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(forKey: "OrderList")! as! NSArray).object(at: (indexPath as NSIndexPath).row) as? NSDictionary)!) // shippingAddress
        
        
        cell.shippingAddressValueLbl.numberOfLines = 0
        
        let discribLblHeight = ProductDetailsVC.heightForView(cell.shippingAddressValueLbl.text!, font: cell.shippingAddressValueLbl.font, width: cell.shippingAddressValueLbl.frame.size.width)
        
        cell.shippingAddressValueLbl.frame = CGRect(x: cell.shippingAddressValueLbl.frame.origin.x, y: cell.shippingAddressValueLbl.frame.origin.y , width: cell.shippingAddressValueLbl.frame.size.width, height: discribLblHeight)
        
        var frame:CGRect = cell.orderStatusLbl.frame
        
        frame.origin.y = max(70, discribLblHeight + 35)
        
        cell.orderStatusLbl.frame = frame
        
        var valueFrame:CGRect = cell.orderStatusValueLbl.frame
        
        valueFrame.origin.y = max(70, discribLblHeight + 35)
        
        cell.orderStatusValueLbl.frame = valueFrame
        
       cell.orderStatusValueLbl.text = (((self.myOrderArr.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(forKey: "OrderList") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "order_status") as?String
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.clear

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {

        let height = ProductDetailsVC.heightForView(MyOrderVC.findAddressWith((((self.myOrderArr.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(forKey: "OrderList")! as! NSArray).object(at: (indexPath as NSIndexPath).row) as? NSDictionary)!), font: UIFont.systemFont(ofSize: 12.0), width: 168)
        
        return max(137, height+75)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "myOrderProductSegue", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        let indexPath:IndexPath = (sender as? IndexPath)!
        let obj:MyOrderProductVC = segue.destination as! MyOrderProductVC
        obj.orderDetails = (((self.myOrderArr.object(at: (indexPath as NSIndexPath).section) as AnyObject).object(forKey: "OrderList") as! NSArray).object(at: (indexPath as NSIndexPath).row) as? NSDictionary)!

    }
    
    class func findAddressWith(_ details:NSDictionary)->String
    {
        var shippingAddress:String;
        
        shippingAddress = String(format:"%@ %@",(details.object(forKey: "shipping_firstname") as?String)!,(details.object(forKey: "shipping_lastname") as?String)!)
        
        if !(details.object(forKey: "shipping_company") as! String).isEmpty
        {
            shippingAddress = String(format: "%@\n%@",shippingAddress, (details.object(forKey: "shipping_company") as?String)!)
            
        }
        shippingAddress = String(format: "%@\n%@",shippingAddress, (details.object(forKey: "shipping_address_1") as?String)!)
        
        if !(details.object(forKey: "shipping_address_2") as! String).isEmpty
        {
            shippingAddress = String(format: "%@\n%@",shippingAddress, (details.object(forKey: "shipping_address_2") as?String)!)
            
        }
        
        if (details.object(forKey: "shipping_postcode") as! String).isEmpty
        {
            shippingAddress = String(format: "%@\n%@",shippingAddress, (details.object(forKey: "shipping_city") as?String)!)
        }
        else
        {
            shippingAddress = String(format: "%@\n%@ %@",shippingAddress, (details.object(forKey: "shipping_city") as?String)!,(details.object(forKey: "shipping_city") as?String)!)
        }
        
        shippingAddress = String(format: "%@\n%@",shippingAddress, (details.object(forKey: "shipping_zone") as?String)!)
        
        shippingAddress = String(format: "%@\n%@",shippingAddress, (details.object(forKey: "shipping_country") as?String)!)
        
        return shippingAddress
    }    
}
