//
//  ReturnProductViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 10/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class ReturnProductViewController: ParentViewController
{
    var orderDetails:NSDictionary = [:]
    var returnProductDict:NSMutableDictionary = NSMutableDictionary()
    
    var isTermsClicked = false
    var isOpened = false
    var productId = ""
    var productName = ""
    var productModelName = ""
    
    @IBOutlet weak var reasonLbl: UILabel!
    @IBOutlet weak var imgTerms: UIImageView!
    @IBOutlet weak var tblReason: UITableView!
    @IBOutlet weak var reasonView: UIView!
    @IBOutlet weak var imgNo: UIImageView!
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var txtFaultyOtherDetails: UITextField!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    var opened = "0"
    
    var reasonId = ""
    var reasonArr = NSMutableArray ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.reasonView.isHidden = true
        var contentRect:CGRect = CGRect.zero
        var scrollHeight:CGFloat = 0.0
        for views in self.mainScrollView.subviews
        {
            let subView:UIView = views
            contentRect = contentRect.union(subView.frame)
            scrollHeight = scrollHeight + subView.frame.size.height
        }
        
        self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height:scrollHeight + 10)
        
        self.lblName.text = orderDetails.value(forKey: "firstname") as! String?
        self.lblOrderId.text = orderDetails.value(forKey: "order_id") as! String?
        self.lblEmail.text = orderDetails.value(forKey: "email") as! String?
        self.lblOrderDate.text = orderDetails.value(forKey: "date_added") as! String?
        self.lblMobile.text = orderDetails.value(forKey: "telephone") as! String?
        
        let productDetails = orderDetails.value(forKeyPath: "products") as! NSMutableArray
        self.lblProductName.text = productName
        self.lblModel.text = productModelName
        self.txtQuantity.text = (productDetails[0] as AnyObject).value(forKey: "quantity") as? String

        reasonView.layer.cornerRadius = 8
        reasonView.clipsToBounds = true
        reasonView.layer.masksToBounds = false
        reasonView.layer.shadowColor = UIColor.black.cgColor
        reasonView.layer.shadowOffset = CGSize(width: 0, height: 0)
        reasonView.layer.shadowOpacity = 0.7
        reasonView.layer.shadowRadius = 3.0
        
        
        BusinessManager.getReturnReasonList(languageID as String) { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            
            self.reasonArr = result.object(forKey: "reasons") as! NSMutableArray
            
            if self.reasonArr.count != 0
            {
                self.tblReason.reloadData()
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "Please Enter the Reason Below", viewController: self)
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.reasonArr.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.reasonLbl.text = ((self.reasonArr.object(at: indexPath.row) as AnyObject).value(forKey: "reason")) as? String
        self.reasonView.isHidden = true
        
        reasonId = (((self.reasonArr.object(at: indexPath.row) as AnyObject).value(forKey: "return_reason_id")) as? String)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ReturnReasonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reasonCell") as! ReturnReasonTableViewCell
        
        cell.reasonLbl.text = ((self.reasonArr.object(at: indexPath.row) as AnyObject).value(forKey: "reason")) as? String
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 30
    }
    
    @IBAction func clickCancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickReason(_ sender: Any)
    {
        self.reasonView.isHidden = false
    }
    
    @IBAction func clickTerms(_ sender: Any)
    {
        if isTermsClicked == false
        {
            imgTerms.image = UIImage (named: "ico-tick.png")
            isTermsClicked = true
        }
        else
        {
            imgTerms.image = UIImage (named: "ico-rnd1.png")
            isTermsClicked = false
        }
    }
    
    @IBAction func clickOpenedNo(_ sender: Any)
    {
        imgNo.image = UIImage (named: "ico-tick.png")
        imgYes.image = UIImage (named: "ico-rnd1.png")
        isOpened = false
        opened = "0"
    }
    
    @IBAction func clickOpenedYes(_ sender: Any)
    {
        imgYes.image = UIImage (named: "ico-tick.png")
        imgNo.image = UIImage (named: "ico-rnd1.png")
        isOpened = true
        opened = "1"
    }
    
    @IBAction func clickSubmitReturn(_ sender: Any)
    {
        if isTermsClicked != true
        {
            SharedManager.showAlertWithMessage(title: "Alert", alertMessage: "Please Accept our Delivery and Return Policy", viewController: self)
        }
        else
        {
            if reasonId == ""
            {
                SharedManager.showAlertWithMessage(title: "Alert", alertMessage: "Please select any one Reason for Return", viewController: self)
            }
            else
            {
                
                let lastName = orderDetails.value(forKey: "lastname") as! String!
                
                let values:NSString = NSString(format: "firstname=%@&lastname=%@&telephone=%@&quantity=%@&email=%@&comment=%@&customer_id=%@&order_id=%@&return_reason_id=%@&model=%@&product=%@&opened=%@",lblName.text!,lastName!,lblMobile.text!,txtQuantity.text!,lblEmail.text!,txtFaultyOtherDetails.text!,userIDStr,lblOrderId.text!,reasonId,lblModel.text!,lblProductName.text!,opened)
                
                SharedManager.showHUD(viewController: self)
                
                BusinessManager.returnProductWith(values as String, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    let code = result.value(forKey: "status") as! String
                    if code == "200"
                    {
                        SharedManager.showAlertWithMessage(title: "Alert", alertMessage: "Your Return Request submitted Successfully", viewController: self)
                        
                        
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "Unable to Place a Return request, Please Try again Later", viewController: self)
                    }
                })
            }
        }
    }
}
