//
//  MyOrderProductVC.swift
//  Exlcart
//
//  Created by iPhone on 13/10/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class MyOrderProductVC: ParentViewController {

    
    @IBOutlet weak var productListTbl: UITableView!
    @IBOutlet weak var orderValueLbl: UILabel!
    
    @IBOutlet weak var addressValueLbl: UILabel!
    @IBOutlet weak var statusValueLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var orderBGView: UIView!
    var orderDetails:NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.productListTbl.tableFooterView = UIView()
        orderValueLbl.text = orderDetails.object(forKey: "order_id") as? String
        addressValueLbl.text = MyOrderVC.findAddressWith(orderDetails)
        addressValueLbl.numberOfLines = 0
        let discribLblHeight = ProductDetailsVC.heightForView(addressValueLbl.text!, font: addressValueLbl.font, width: addressValueLbl.frame.size.width)
        
        addressValueLbl.frame = CGRect(x: addressValueLbl.frame.origin.x, y: addressValueLbl.frame.origin.y , width: addressValueLbl.frame.size.width, height: discribLblHeight)
        
        var frame:CGRect = statusLbl.frame
        
        frame.origin.y = max(70, discribLblHeight + 35)
        
        statusLbl.frame = frame
        
        var valueFrame:CGRect = statusValueLbl.frame
        
        valueFrame.origin.y = max(70, discribLblHeight + 35)
        
        statusValueLbl.frame = valueFrame
        
        statusValueLbl.text =  orderDetails.object(forKey: "order_status") as?String
        
        orderBGView.frame = CGRect(x: 0, y: orderBGView.frame.origin.y, width: orderBGView.frame.width, height: max(100, discribLblHeight+65))
        
        productListTbl.frame = CGRect(x: 0, y: orderBGView.frame.height + orderBGView.frame.origin.y, width: productListTbl.frame.width, height: self.view.frame.height - (orderBGView.frame.height + orderBGView.frame.origin.y) - 40)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return (orderDetails.object(forKey: "products")! as! NSArray).count + 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifier:NSString
        
        if (indexPath as NSIndexPath).row == (orderDetails.object(forKey: "products")! as AnyObject).count
        {
            identifier = "reuseIdentifier2"
        }
        else
        {
            identifier = "reuseIdentifier1"
        }
        
        let cell:MyOrderProductCell = tableView.dequeueReusableCell(withIdentifier: identifier as String) as! MyOrderProductCell
        
        if (indexPath as NSIndexPath).row == (orderDetails.object(forKey: "products")! as! NSArray).count
        {
            
            cell.subtotalValueLbl.text = ((orderDetails.object(forKey: "order_totals") as! NSArray).object(at: 0) as AnyObject).object(forKey: "value") as? String
            cell.flatShippingLbl.text = ((orderDetails.object(forKey: "order_totals") as! NSArray).object(at: 1) as AnyObject).object(forKey: "value") as? String
            cell.totalLbl.text = ((orderDetails.object(forKey: "order_totals") as! NSArray).object(at: 2) as AnyObject).object(forKey: "value") as? String
            
            cell.subTotalTitleLbl.text = ((orderDetails.object(forKey: "order_totals") as! NSArray).object(at: 0) as AnyObject).object(forKey: "title") as? String
            cell.flatShippingTitleLbl.text = ((orderDetails.object(forKey: "order_totals") as! NSArray).object(at: 1) as AnyObject).object(forKey: "title") as? String
            cell.totalTitleLbl.text = ((orderDetails.object(forKey: "order_totals") as! NSArray).object(at: 2) as AnyObject).object(forKey: "title") as? String
            
        }
        else
        {
            cell.btnReturn.tag = indexPath.row
            cell.btnReturn.addTarget(self, action: #selector(clickReturn), for: .touchUpInside)
            
            cell.productTitleLbl.text = ((orderDetails.object(forKey: "products") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            cell.quantityLbl.text = String(format: "%@ x %@",(((orderDetails.object(forKey: "products") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "quantity") as?String)!,(((orderDetails.object(forKey: "products") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "price") as?String)!)
            cell.totalPriceLbl.text = ((orderDetails.object(forKey: "products") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "total") as? String
            let imageUrl = (((orderDetails.object(forKey: "products") as! NSArray).object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as? String)!
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            cell.productImg.sd_setImage(with: URL(string: trimmedUrl))

        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    //MARK: Return
    func clickReturn(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ReturnProductViewController") as! ReturnProductViewController
        vc.orderDetails = self.orderDetails
      //  vc.productId = (((orderDetails.object(forKey: "products") as! NSArray).object(at: (sender.tag)) as AnyObject).value(forKey: "name") as? String)!
        vc.productName = (((orderDetails.object(forKey: "products") as! NSArray).object(at: (sender.tag)) as AnyObject).value(forKey: "name") as? String)!
        vc.productModelName = (((orderDetails.object(forKey: "products") as! NSArray).object(at: (sender.tag)) as AnyObject).value(forKey: "model") as? String)!
        
        navigationController?.pushViewController(vc, animated: true)
    }

}
