//
//  ReturnReasonTableViewCell.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 10/05/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class ReturnReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var reasonLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
