//
//  MyOrderTableViewCell.swift
//  Exlcart
//
//  Created by iPhone on 23/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class MyOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var orderIDValueLbl: UILabel!
    @IBOutlet weak var shippingAddressValueLbl: UILabel!
    @IBOutlet weak var orderStatusValueLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var separateLbl: UILabel!
    
    
    @IBAction func deleteAction(_ sender: AnyObject)
    {
        
    }
    
    @IBAction func editAction(_ sender: AnyObject)
    {
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
