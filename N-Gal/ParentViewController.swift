    //
//  ParentViewController.swift
//  E-Commerce
//
//  Created by Apple on 24/06/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

import UIKit
var languageID:String = "1"
var countryID: NSString = ""
var stateID:NSString = ""
var countryName:String = ""
var stateName:String = ""
var categoryID:String = ""
var subCategoryID:NSString = ""
var creditTypeStr:NSString = ""
var mainCategoryID:String = ""
var catID = NSMutableArray()
var cartTotalAmount:NSString = ""
var paymentInfo:NSMutableArray = []
var shippingInfo:NSMutableArray = []
var shipingAddressJsonStr:NSString = ""
    
@objc class ParentViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIPopoverPresentationControllerDelegate  {

    var rechability:Reachability?
    
    var categoryListTbl: UITableView  =   UITableView(frame: CGRect(x: -(UIScreen.main.bounds.size.width-100), y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height-108), style: .grouped)
    
    var informationTbl: UITableView  =   UITableView(frame: CGRect(x: -(UIScreen.main.bounds.size.width-100), y: 0, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height-108), style: .grouped)
    
    var servicesTbl: UITableView  =   UITableView(frame: CGRect(x: -(UIScreen.main.bounds.size.width-100), y: 0, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height-108), style: .grouped)
    
    var tempView : UIView = UIView()
    
    var categoryListAry:NSArray = []
    var countryListAry:NSArray = []
    var sortedListAry:NSMutableArray = []
    var arrayForBool:NSMutableArray = []
    var subListAry = NSMutableArray()
    var btn = UIButton()
    var collapsed = Bool()
    var stateAryList:NSMutableArray = []
    let scrollingView  = UIScrollView()
    let informationView = UIView()
    let servicesView = UIView()
    
    var informArr = NSArray()
    var servicesArr = NSArray()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.items![1].title =  "Search"
        if Reachability.forInternetConnection().isReachable()
        {
            if (UserDefaults.standard.object(forKey: "language_id") != nil)
            {
                languageID = UserDefaults.standard.object(forKey: "language_id") as! String
            }
            if (UserDefaults.standard.object(forKey: "country_array") != nil)
            {
                countryListAry = UserDefaults.standard.object(forKey: "country_array") as! NSArray
            }
            
            
            if self.countryListAry.count == 0
            {
                
                BusinessManager.getCountryStateList{ (result) -> () in
                    
                    self.countryListAry = result as! NSMutableArray
                    UserDefaults.standard.set(self.countryListAry, forKey: "country_array")
                }
            }

            tempView.frame = CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: 2000)
            tempView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            
            scrollingView.showsVerticalScrollIndicator = false
            scrollingView.contentSize = CGSize(width:200, height:2300)
            
            scrollingView.backgroundColor = UIColor.white
            scrollingView.isScrollEnabled = true
            
            tempView.addSubview(scrollingView)
            
            self.informationTbl.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
            self.servicesTbl.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
           // self.automaticallyAdjustsScrollViewInsets = false
            
            // Category list
            
            categoryListTbl.delegate    =   self
            categoryListTbl.dataSource  =   self
            categoryListTbl.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            categoryListTbl.tableFooterView = UIView()
            categoryListTbl.canCancelContentTouches = false
            categoryListTbl.backgroundColor = UIColor.white
            
            categoryListTbl.isScrollEnabled = false
            
            if self.categoryListAry.count == 0
            {
                BusinessManager.getCategoryList(languageID as String, completionHandler:  { (result) -> () in
                    self.categoryListAry    = result.value(forKey: "categories") as! NSArray
                    
                    let descriptor: NSSortDescriptor = NSSortDescriptor(key: "sort_order", ascending: true)
                    let sortedResults = self.categoryListAry.sortedArray(using: [descriptor])
                    self.sortedListAry = NSMutableArray(array: sortedResults)
                    
                    for i in 0 ..< self.sortedListAry.count
                    {
                        self.arrayForBool.insert(NSNumber(value: false as Bool), at: i)
                        self.subListAry.add((self.sortedListAry.object(at: i) as AnyObject).value(forKey: "child") as! NSMutableArray)
                    }
                })
            }
            
            let logoImg = UIImageView()
            logoImg.image = UIImage (named: "logo.png")
            logoImg.frame = CGRect(x: 10, y: 10, width: 50, height: 50)
            logoImg.contentMode = .scaleAspectFit
            self.scrollingView.addSubview(logoImg)
            
            let font = UIFont.boldSystemFont(ofSize: 16.0)
            let subTitle = UILabel()
            subTitle.text = "CATEGORIES"
            subTitle.textAlignment = .center
            subTitle.font = font
            subTitle.frame = CGRect(x: 0, y: logoImg.frame.size.height + 10, width: UIScreen.main.bounds.size.width-60, height: 40)
            self.scrollingView.addSubview(subTitle)
            
            let lineTop = UIView()
            lineTop.frame = CGRect(x: 0, y: subTitle.frame.origin.y + subTitle.frame.size.height , width: UIScreen.main.bounds.size.width-60, height: 1)
            lineTop.backgroundColor = UIColor.gray
           // scrollingView.addSubview(lineTop)
            
            self.categoryListTbl.frame = CGRect(x: 0, y: lineTop.frame.origin.y + lineTop.frame.size.height + 5, width: UIScreen.main.bounds.size.width-60, height: self.categoryListTbl.contentSize.height)
            categoryListTbl.tableFooterView = UIView()
            categoryListTbl.canCancelContentTouches = false
            
            self.scrollingView.addSubview(self.categoryListTbl)
            self.autoAdjustFrame()
        
        //Infromation
            
            informationView.backgroundColor = UIColor.white
            
            informArr = ["Reward Points and Coupons", "Delivery and Returns", "Cancellation and Refunds", "Caring for clothes", "Contact Us"]
            servicesArr = ["Terms and conditions", "Privacy Policy"]
            
            informationTbl.reloadData()
            servicesTbl.reloadData()
            
            var frameY:CGFloat = 5.0
            
            let line1 = UIView()
            line1.frame = CGRect(x: 0, y: frameY , width: UIScreen.main.bounds.size.width-60, height: 1)
            line1.backgroundColor = UIColor.gray
            informationView.addSubview(line1)
            
            frameY = frameY+10
            
        let titleInfoLbl = UILabel()
            titleInfoLbl.text = "INFORMATION"
            titleInfoLbl.textAlignment = .center
            titleInfoLbl.frame = CGRect(x: 0, y: frameY, width: UIScreen.main.bounds.size.width-60, height: 25)
            titleInfoLbl.font = font
            informationView.addSubview(titleInfoLbl)
            
            frameY = frameY+titleInfoLbl.frame.size.height
            
            informationTbl.delegate    =   self
            informationTbl.dataSource  =   self
            informationTbl.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            informationTbl.tableFooterView = UIView()
            informationTbl.canCancelContentTouches = false
            informationTbl.backgroundColor = UIColor.white
            informationTbl.separatorStyle = .none
            informationTbl.separatorInset = .zero
            
            var frame = informationTbl.frame
            frame.origin.x = 0
            frame.origin.y = frameY + 5
            frame.size.width = UIScreen.main.bounds.size.width-60
            frame.size.height = informationTbl.contentSize.height
            self.informationTbl.frame = frame
            
            self.informationView.addSubview(informationTbl)
            self.scrollingView.addSubview(self.informationView)
            
            self.informationView.frame.size.height =  self.informationTbl.contentSize.height
            
            // Services
            
            servicesTbl.delegate    =   self
            servicesTbl.dataSource  =   self
            servicesTbl.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            servicesTbl.tableFooterView = UIView()
            servicesTbl.canCancelContentTouches = false
            servicesTbl.backgroundColor = UIColor.white
            servicesTbl.separatorStyle = .none
            servicesTbl.separatorInset = .zero
            
            frameY = 5.0
            
            let line2 = UIView()
            line2.frame = CGRect(x: 0, y: frameY , width: UIScreen.main.bounds.size.width-60, height: 1)
            line2.backgroundColor = UIColor.gray
            
            frameY = frameY+10
            
            let titleServicesLbl = UILabel()
            titleServicesLbl.text = "SERVICES"
            titleServicesLbl.textAlignment = .center
            titleServicesLbl.frame = CGRect(x: 0, y: frameY, width: UIScreen.main.bounds.size.width-60, height: 25)
            titleServicesLbl.font = font
            servicesView.addSubview(titleServicesLbl)
            
            frameY = frameY+titleServicesLbl.frame.size.height
            
            frame = servicesTbl.frame
            frame.origin.x = 0
            frame.origin.y = frameY + 5
            frame.size.width = UIScreen.main.bounds.size.width-60
            frame.size.height = servicesTbl.contentSize.height
            self.servicesTbl.frame = frame
            
            self.servicesView.addSubview(titleServicesLbl)
            servicesView.addSubview(line2)
            self.servicesView.addSubview(servicesTbl)
            
            frame = servicesView.frame
            frame.origin.x = 0
            frame.origin.y = self.informationView.frame.origin.y + self.informationView.frame.size.height - 50
            frame.size.width = UIScreen.main.bounds.size.width
            frame.size.height = servicesTbl.contentSize.height + titleServicesLbl.frame.size.height + 5
            self.servicesView.frame = frame
            
            self.scrollingView.addSubview(self.servicesView)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(ParentViewController.addMenuView))
            tap.delegate = self
            tempView.addGestureRecognizer(tap)
            tempView.addSubview(scrollingView)
            
            autoAdjustFrame()
        }
        else
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "NoConnectionVC") as! ErrorVC
            let navController = UINavigationController.init(rootViewController: viewController)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func autoAdjustFrame()
    {
        var frame: CGRect = self.categoryListTbl.frame
        frame.size.height = CGFloat(self.sortedListAry.count * 55)
        self.categoryListTbl.frame = frame
        
        frame = self.informationTbl.frame
        frame.size.height = self.informationTbl.contentSize.height
        self.informationTbl.frame = frame
        
        frame = self.servicesTbl.frame
        frame.size.height = self.servicesTbl.contentSize.height
        self.servicesTbl.frame = frame
        
        var height:CGFloat = 0.0
        
        var contentRect:CGRect = CGRect.zero

        self.informationView.frame = CGRect(x: 0, y: self.categoryListTbl.frame.origin.y + self.categoryListTbl.frame.size.height + 10, width: UIScreen.main.bounds.size.width-60, height: self.informationTbl.contentSize.height)
        
        for views in self.servicesView.subviews
        {
            let subView:UIView = views
            contentRect = contentRect.union(subView.frame)
            height = height + subView.frame.size.height + 5
        }
        
        self.servicesView.frame = CGRect(x: 0, y: self.informationView.frame.origin.y + self.informationView.frame.size.height - 40, width: UIScreen.main.bounds.size.width-60, height: height)
        
        for views in self.scrollingView.subviews
        {
            let subView:UIView = views
            contentRect = contentRect.union(subView.frame)
            height = height + subView.frame.size.height
        }
        
      //  self.scrollingView.frame = CGRect(x: 0, y: self.scrollingView.frame.origin.y, width: self.scrollingView.frame.size.width, height: self.categoryListTbl.contentSize.height + self.informationView.frame.size.height + self.servicesView.frame.size.height)
        
        if SharedManager.DeviceType.IS_IPHONE_6
        {
            self.scrollingView.contentSize = CGSize(width: self.scrollingView.frame.size.width, height: self.categoryListTbl.frame.size.height + self.informationView.frame.size.height + self.servicesView.frame.size.height + 1450)
        }
        else if SharedManager.DeviceType.IS_IPHONE_6P
        {
            self.scrollingView.contentSize = CGSize(width: self.scrollingView.frame.size.width, height: self.categoryListTbl.frame.size.height + self.informationView.frame.size.height + self.servicesView.frame.size.height + 1450)
        }
        else if SharedManager.DeviceType.IS_IPHONE_5
        {
            self.scrollingView.contentSize = CGSize(width: self.scrollingView.frame.size.width, height: self.categoryListTbl.contentSize.height + self.informationView.frame.size.height + self.servicesView.frame.size.height + 1650)
        }
        else
        {
            self.scrollingView.contentSize = CGSize(width: self.scrollingView.frame.size.width, height: self.categoryListTbl.frame.size.height + self.informationView.frame.size.height + self.servicesView.frame.size.height + 1400)
        }
        
        for views in self.tempView.subviews
        {
            let subView:UIView = views
            contentRect = contentRect.union(subView.frame)
            height = self.tempView.frame.size.height + subView.frame.size.height
        }
        
        self.tempView.frame = CGRect(x: 0, y: self.tempView.frame.origin.y, width: self.tempView.frame.size.width, height: height)
        
        if SharedManager.DeviceType.IS_IPHONE_5
        {
            self.tempView.frame = CGRect(x: 0, y: self.tempView.frame.origin.y, width: self.tempView.frame.size.width, height: height )
        }
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
        if touch.view == tempView
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    func gotoRootViewController()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarcontroller") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = viewController
        appDelegate.window?.makeKeyAndVisible()
    }

    func updateCartBadge()
    {
        if let data = UserDefaults.standard.object(forKey: kAddToCart) as? Data
        {
            let cartListAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            let tabItem = self.tabBarController?.tabBar.items![3] as UITabBarItem!// as Array!
            
            var count:Int = 0
            
            if (cartListAry.count > 0)
            {
                for i in 0 ..< cartListAry.count
                {
                let quantity = (cartListAry.object(at: i) as AnyObject).object(forKey: "userQuantity") as! String
                count = count + Int(quantity)!
                tabItem?.badgeValue = NSString(format: "%d", count) as String
                }
            }
            else
            {
                tabItem?.badgeValue = nil                
            }
            
        }
        
    }
    
    var url : URL!
    
    @objc func clickOtherCategory(tag: Int)
    {
        switch tag
        {
        case 0:
            url = URL(string: "https://www.n-gal.com/reward-points-and-coupons")!
            
        case 1:
            url = URL(string: "https://www.n-gal.com/delivery-and-returns")!
            
        case 2:
            url = URL(string: "https://www.n-gal.com/cancellations-and-refunds")!
            
        case 3:
            url = URL(string: "https://www.n-gal.com/caring-for-your-clothes")!
            
        case 4:
            url = URL(string: "https://www.n-gal.com/contact")!
            
        case 5:
            url = URL(string: "https://www.n-gal.com/terms-and-conditions")!
            
        case 6:
            url = URL(string: "https://www.n-gal.com/privacy-policy")!
            
        default:
            break
        }
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "OtherCategoriesViewController") as! OtherCategoriesViewController
        viewController.urlString = "\(url!)"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
    
    func setLeftNavigationButton()
    {
        
        var image = UIImage(named: "ico-menu.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(ParentViewController.addMenuView))
    }
    func showCountryList()
    {
        self.view.endEditing(true)

        var countryNames = [String]()
        let country_id = NSMutableArray()
        
        for i in 0 ..< self.countryListAry.count
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: i)
            countryNames.append(name as! String)
            country_id.add((self.countryListAry.value(forKey: "country_id") as! NSArray).object(at: i) as! String)
        }
        
        let picker = BSModalPickerView.init(values: countryNames)
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice
            {
                print("You chose index \(picker?.selectedIndex), which was the value \(picker?.selectedValue)")
                countryID = country_id.object(at: Int(bitPattern: (picker?.selectedIndex)!)) as! NSString
                countryName = (picker?.selectedValue)!
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getName"), object: nil)
            }
            else
            {
                print("You cancelled the picker")
            }
        })
    }
    
    func showStateList()
    {
        self.view.endEditing(true)

        var stateNames = [String]()
        var state_id = NSMutableArray()
        
        for i : Int in 0 ..< self.countryListAry.count
        {
            let id:String = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "country_id") as? String)!
            
            
            if countryID.isEqual(to: id as String)
            {
                self.stateAryList = ((self.countryListAry.object(at: i) as AnyObject).value(forKey: "state")! as! NSArray).mutableCopy() as! NSMutableArray
                
                if self.stateAryList.count > 0{
                for i in 0 ..< self.stateAryList.count{
                    
                        let name = (self.stateAryList.value(forKey: "name") as AnyObject).object(at: i)
                        stateNames.append(name as! String)
                        state_id.add((self.stateAryList.value(forKey: "state_id") as! NSArray).object(at: i) as! String)
                    }
                }
                else{
                    stateNames = ["None"]
                    state_id = ["0"]
                }
            }
        }
        
        let picker = BSModalPickerView.init(values: stateNames)
        
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice {
                print("You chose index \(picker?.selectedIndex), which was the value \(picker?.selectedValue)")
                stateID = state_id.object(at: Int(bitPattern: (picker?.selectedIndex)!)) as! NSString
                stateName = (picker?.selectedValue)!
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getName"), object: nil)

            }
            else {
                print("You cancelled the picker")
            }
        })

    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
    }
      
    func showFavoriteList()
    {
        self.tabBarController?.selectedIndex = 2
    }
    
    func addMenuView()
    {
        if !(categoryListTbl.isDescendant(of: tempView))
        {
            tempView.addSubview(scrollingView)
            self.view.addSubview(tempView)
            
            self.categoryListTbl.frame.size.height = self.categoryListTbl.contentSize.height
            
            for i in 0 ..< self.arrayForBool.count
            {
                self.arrayForBool.replaceObject(at: i, with: false as Bool)
            }
            
            self.categoryListTbl.reloadData()
            
            UIView.animate(withDuration: 0.50, animations: {
                //self.categoryListTbl.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-100, height: self.tempView.frame.height)
                self.scrollingView.frame =  CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: 2000)
                
                self.autoAdjustFrame()
            })
        }
        else
        {
            self.categoryListTbl.frame.size.height = self.categoryListTbl.contentSize.height
            
            for i in 0 ..< self.arrayForBool.count
            {
                self.arrayForBool.replaceObject(at: i, with: false as Bool)
            }
            
            self.categoryListTbl.reloadData()
            
            UIView.animate(withDuration: 0.50, animations: { () -> Void in
                
                self.scrollingView.frame =  CGRect(x: -(UIScreen.main.bounds.size.width-100), y: 0, width: UIScreen.main.bounds.size.width-60, height: self.categoryListTbl.contentSize.height + self.informationView.frame.size.height + self.servicesView.frame.size.height + 2000)
                
                if SharedManager.DeviceType.IS_IPHONE_5
                {
                    self.scrollingView.frame =  CGRect(x: -(UIScreen.main.bounds.size.width-100), y: 0, width: UIScreen.main.bounds.size.width-60, height: self.categoryListTbl.contentSize.height + self.informationView.frame.size.height + self.servicesView.frame.size.height + 1800)
                }
                
                self.autoAdjustFrame()
                
            }, completion: { (bol) -> Void in
                self.tempView.removeFromSuperview()
                self.scrollingView.removeFromSuperview()
            })
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == self.categoryListTbl
        {
            return self.sortedListAry.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView ==  self.categoryListTbl && arrayForBool.count > 0
        {
            if CBool(arrayForBool[section] as! NSNumber)
            {
                return (self.subListAry[section] as AnyObject).count
            }
            else
            {
                return 0
            }
        }
        else if tableView == informationTbl
        {
            return informArr.count
        }
        else if tableView == servicesTbl
        {
            return servicesArr.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cell = self.categoryListTbl.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        
        if tableView == self.categoryListTbl
        {
            if (cell == nil)
            {
                cell = UITableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
            }
            
            let manyCells: Bool = CBool(arrayForBool[(indexPath as NSIndexPath).section] as! NSNumber)
            
            if !manyCells
            {
                cell?.backgroundColor = UIColor.clear
                cell?.textLabel?.text = ""
            }
            else
            {
                let subCatId = ((self.subListAry.object(at: (indexPath as NSIndexPath).section) as! NSArray).value(forKeyPath: "category_id") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                
                if mainCategoryID == subCatId
                {
                    cell?.textLabel?.text = "All Products"
                }
                else
                {
                    cell?.textLabel?.text = ((self.subListAry.object(at: (indexPath as NSIndexPath).section) as! NSArray).value(forKey: "name") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                }
                
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 15)
                cell?.backgroundColor = UIColor.white
                cell?.indentationLevel = 2
            }
            cell?.textLabel?.textColor = UIColor.black
            cell?.accessoryType = UITableViewCellAccessoryType.none
            
        }
        else if tableView == self.informationTbl
        {
            if (cell == nil)
            {
                cell = UITableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
            }
            
            cell?.textLabel?.text = informArr[indexPath.row] as? String
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell?.backgroundColor = UIColor.white
            cell?.textLabel?.textColor = UIColor.black
            cell?.indentationLevel = 2
            
            cell?.selectionStyle = .none
        }
        else if tableView == self.servicesTbl
        {
            if (cell == nil){
                cell = UITableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
            }
            
            cell?.textLabel?.text = servicesArr[indexPath.row] as? String
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell?.backgroundColor = UIColor.white
            cell?.indentationLevel = 2
            
            cell?.selectionStyle = .none
        }
        
        return cell!
        
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
        
        
        categoryID = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "category_id") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
        VC.catTitle = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "name") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
        self.navigationController?.pushViewController(VC, animated: true)
        
        */
        
        if tableView == categoryListTbl
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            var ary = NSMutableArray()
            subCategoryID = ((subListAry.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "category_id") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String as NSString
            
            BusinessManager.getSubCategoryList(languageID as String, subCategoryID: subCategoryID as String,completionHandler:  { (result) -> () in
                
                let imageUrl = ((result.value(forKey: "categories") as! NSArray).object(at: 0) as AnyObject).value(forKey: "image") as! String
                
                let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                
                ary = ((result.value(forKey: "categories") as! NSArray).object(at: 0) as AnyObject).value(forKey: "child") as! NSMutableArray
                
                if ary.count > 0
                {
                    let viewController = storyBoard.instantiateViewController(withIdentifier: "SubCategory") as! SubCategory
                    viewController.subCategoryAry2 = ary
                    
                    viewController.imageUrl = URL(string: trimmedUrl)!
                    viewController.imageUrlStr = trimmedUrl
                    
                    categoryID = (((result.value(forKey: "categories") as! NSArray).object(at: 0) as AnyObject).value(forKey: "category_id") as! String as NSString) as String
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else
                {
                    let VC = self.storyboard!.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
                    categoryID = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "category_id") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
                    VC.catTitle = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "name") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            })
        }
        else if tableView == informationTbl
        {
            clickOtherCategory(tag: indexPath.row)
        }
        else if tableView == servicesTbl
        {
            if indexPath.row == 0
            {
                clickOtherCategory(tag: 5)
            }
            else
            {
                clickOtherCategory(tag: 6)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == self.categoryListTbl
        {
            return 40
        }
        else if tableView == self.informationTbl
        {
            return 0.000001
        }
        else if tableView == self.servicesTbl
        {
            return 0.000001
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == self.categoryListTbl
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 40))
            sectionView.tag = section
            
            let imageIcon = UIImageView()
            imageIcon.frame = CGRect(x: 15, y: 10, width: 17, height: 23)
            
            let imageURL = (sortedListAry.object(at: section) as AnyObject).value(forKey: "mobile_app_icon") as! String
            let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            imageIcon.sd_setImage(with: URL(string: trimmedUrl))
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = imageIcon.center
            activityLoader.startAnimating()
            imageIcon.addSubview(activityLoader)
            
            imageIcon.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                
                if image != nil
                {
                    
                    activityLoader.stopAnimating()
                }else
                {
                    print("image not found")
                    activityLoader.stopAnimating()
                }
            })
            
            sectionView.addSubview(imageIcon)
            
            sectionView.tag = section
            let viewLabel: UILabel = UILabel(frame: CGRect(x: 45, y: 0, width: categoryListTbl.frame.size.width - 10, height: 40))
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.black
            viewLabel.font = UIFont.systemFont(ofSize: 15)
            viewLabel.text =  (self.sortedListAry.object(at: section) as AnyObject).value(forKey: "name") as? String
            sectionView.addSubview(viewLabel)
            
            catID.add(((self.sortedListAry.object(at: section) as AnyObject).value(forKey: "category_id") as? String)!)
            btn = UIButton(frame: CGRect(x: categoryListTbl.frame.size.width - 25, y: 15, width: 20, height: 20))
            if arrayForBool[section] as! Bool == true
            {
                btn.setImage(UIImage(named: "ic_remove.png"), for: UIControlState())
            }

            else
            {
                btn.setImage(UIImage(named: "ic_add.png"), for: UIControlState())
            }
            
            sectionView.addSubview(btn)
            /********** Add a custom Separator with Section view *******************/
            let separatorLineView: UIView = UIView(frame: CGRect(x: 0, y: 40, width: categoryListTbl.frame.size.width, height: 0.3))
            separatorLineView.backgroundColor = UIColor.lightGray
            //separatorLineView.layer.borderWidth = 0.5
           // sectionView.addSubview(separatorLineView)
            /********** Add UITapGestureRecognizer to SectionView   **************/
            let headerTapped: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(ParentViewController.sectionHeaderTapped(_:)))
            sectionView.addGestureRecognizer(headerTapped)
            
            sectionView.frame.size.height = 40
            
            return sectionView
        }
        else if tableView == self.informationTbl
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 0.01))
           // sectionView.frame.size.height = 0
            
            return sectionView
            
        }
        else if tableView == self.servicesTbl
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 0.01))
           // sectionView.frame.size.height = 0
            
            return sectionView
        }
        else
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 0))
          //  sectionView.frame.size.height = 0
            
            return sectionView
        }
    }
    
    
    func sectionHeaderTapped(_ gestureRecognizer: UITapGestureRecognizer)
    {
        let indexPath: IndexPath = IndexPath(row: 0, section: gestureRecognizer.view!.tag)
        if (indexPath as NSIndexPath).row == 0
        {
            collapsed = CBool(arrayForBool[(indexPath as NSIndexPath).section] as! NSNumber)
            for i in 0 ..< categoryListAry.count {
                if (indexPath as NSIndexPath).section == i {
                    arrayForBool[i] = !collapsed
                    if (arrayForBool[i] as! Bool) == true
                    {
                        categoryListTbl.isScrollEnabled = true
                        mainCategoryID = catID[indexPath.section] as! String
                        collapsed = true
                        categoryListTbl.reloadSections(IndexSet(integer: gestureRecognizer.view!.tag), with: .automatic)
                        categoryListTbl.scrollToRow(at: IndexPath(row: 0, section: indexPath.section), at: .top, animated: true)
                    }
                    else
                    {
                        categoryListTbl.isScrollEnabled = false
                        collapsed = false
                        categoryListTbl.reloadSections(IndexSet(integer: gestureRecognizer.view!.tag), with: .automatic)
                    }
                }
            }
        }
    }

    func isValidEmail(_ testStr:String) -> Bool {

        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}
