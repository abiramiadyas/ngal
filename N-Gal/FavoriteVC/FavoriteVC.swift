 //
//  FavoriteVC.swift
//  Exlcart
//
//  Created by iPhone on 22/08/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class FavoriteVC: ParentViewController,UITabBarControllerDelegate,loginIntimation {
    var favoriteAry:NSMutableArray = []
    
    @IBOutlet weak var favoriteTableView: UITableView!
    var isDidLoad:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
     //   self.title = NSLocalizedString("Wishlist", comment: "")
        self.favoriteTableView.tableFooterView = UIView()
        
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
        {
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let userDic:NSMutableDictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableDictionary
            
            userIDStr = userDic.value(forKeyPath: "customer.customer_id") as! String
            
                SharedManager.showHUD(viewController: self)
            BusinessManager.getWishList(userIDStr, languageID: languageID as String) { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let count = result.value(forKey: "count") as! Int
                
                if count > 0
                {
                    self.favoriteTableView.reloadData()
                    self.favoriteAry = result.value(forKey: "products") as! NSMutableArray
                    
                    let testArr = NSMutableArray()
                    testArr.addObjects(from: self.favoriteAry as! [Any])
                    
                    for i in 0..<testArr.count
                    {
                        let name = (testArr.object(at: (i)) as AnyObject).value(forKey: "name") as! String
                        
                        if name == ""
                        {
                            self.favoriteAry.removeObject(at: i)
                        }
                    }
                    
                    self.favoriteTableView.reloadData()
                    let tempData = NSKeyedArchiver.archivedData(withRootObject: self.favoriteAry)
                    UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kProductWishlistAlertMessage, viewController: self)
                }
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
                //
            })
        }
    }
        func loginSuccess()
        {
            isDidLoad = false
            if(UserDefaults.standard.object(forKey: kUserDetails) != nil)
            {
                
                SharedManager.showHUD(viewController: self)
                BusinessManager.getWishList(userIDStr, languageID: languageID as String) { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    let count = result.value(forKey: "count") as! Int
                    
                    if count > 0
                    {
                        self.favoriteAry = result.value(forKey: "products") as! NSMutableArray
                        
                        let testArr = NSMutableArray()
                        testArr.addObjects(from: self.favoriteAry as! [Any])
                        
                        for i in 0..<testArr.count
                        {
                            let name = (testArr.object(at: (i)) as AnyObject).value(forKey: "name") as! String
                            
                            if name == ""
                            {
                                self.favoriteAry.removeObject(at: i)
                            }
                        }
                        
                        
                        self.favoriteTableView.reloadData()
                        let tempData = NSKeyedArchiver.archivedData(withRootObject: self.favoriteAry)
                        UserDefaults.standard.set(tempData, forKey: kAddToWishList)
                        
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kProductWishlistAlertMessage, viewController: self)
                    }
                }
            }

    }
        func loginFailure()
        {
            isDidLoad = false
            self.tabBarController?.selectedIndex = 0
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.setLeftNavigationButton()
        let tabbarObj:UITabBarController = self.tabBarController!
        tabbarObj.delegate=self
        
        if(UserDefaults.standard.object(forKey: kUserDetails) == nil && isDidLoad == false)
        {
            isDidLoad = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginIdentifierSegue") as! SignUpViewController
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
                //
            })
        }
        if let data = UserDefaults.standard.object(forKey: kAddToWishList) as? Data
        {
            favoriteAry = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            let testArr = NSMutableArray()
            testArr.addObjects(from: self.favoriteAry as! [Any])
            
            if favoriteAry.count == 0
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: kProductWishlistAlertMessage, viewController: self)
            }
            else
            {
                for i in 0..<testArr.count
                {
                    let name = (testArr.object(at: (i)) as AnyObject).value(forKey: "name") as! String
                    
                    if name == ""
                    {
                        self.favoriteAry.removeObject(at: i)
                    }
                }
                
                self.favoriteTableView.reloadData()
            }
        }

       

        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        self.navigationController?.popToRootViewController(animated: false)
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == self.categoryListTbl
        {
            return self.categoryListAry.count
        }
        else
        {
            return 1
        }
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView ==  self.categoryListTbl && arrayForBool.count > 0 {
            if CBool(arrayForBool[section] as! NSNumber)
            {
                return (self.subListAry[section] as AnyObject).count
            }
            else
            {
                return 0
            }
        }
        else if tableView == self.favoriteTableView
        {
            return self.favoriteAry.count
        }
        else
        {
            return 0
        }

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.categoryListTbl
        {
            var cell = self.categoryListTbl.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
            
            if (cell == nil){
                cell = UITableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
            }
            
            let manyCells: Bool = CBool(arrayForBool[(indexPath as NSIndexPath).section] as! NSNumber)
            
            if !manyCells
            {
                cell?.backgroundColor = UIColor.clear
                cell?.textLabel?.text = ""
            }
            else
            {
                
                cell?.textLabel?.text = ((self.subListAry.object(at: (indexPath as NSIndexPath).section) as! NSArray).value(forKey: "name") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                cell?.textLabel?.font = UIFont(name: "Arial", size: 14)
                cell?.backgroundColor = UIColor.white
                cell?.indentationLevel = 2
                
            }
            cell?.textLabel?.textColor = UIColor.black
            cell?.accessoryType = UITableViewCellAccessoryType.none
            
            return cell!
        }
        else
        {
            let cell:FavoriteTableViewCell = self.favoriteTableView.dequeueReusableCell(withIdentifier: "cell") as! FavoriteTableViewCell
            cell.titleLbl.text =  (favoriteAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            cell.priceLbl.text = (favoriteAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "price") as? String
            
            let imageUrl = (self.favoriteAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "image") as! String
             let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            cell.productImage.sd_setImage(with: URL(string: trimmedUrl))
            cell.deleteBtn.addTarget(self, action: #selector(FavoriteVC.deleteAction(_:)), for: UIControlEvents.touchUpInside)
            cell.deleteBtn.tag = (indexPath as NSIndexPath).row
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.categoryListTbl
        {
            return 44
        }
        else
        {
            return 119
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.categoryListTbl
        {
            self.addMenuView()
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
            categoryID = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "category_id") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
            VC.catTitle = ((self.subListAry.object(at: indexPath.section) as AnyObject).value(forKey: "name") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String
            self.navigationController?.pushViewController(VC, animated: true)

        }
        else
        {
            self.performSegue(withIdentifier: "favToDetail", sender: indexPath)
        }
    }
    
    func deleteAction(_ sender : UIButton)
    {
        
        let index:IndexPath = IndexPath(row: sender.tag, section: 0)
        let values:String = NSString(format: "product_id=%@&user_id=%@",((self.favoriteAry.object(at: index.row) as AnyObject).value(forKeyPath: "product_id") as AnyObject) as! String,userIDStr) as String
        
            SharedManager.showHUD(viewController: self)
        BusinessManager.removeFromWishListWithValues(values, completionHandler: { (result) -> () in
            SharedManager.dismissHUD(viewController: self)
            
            let code = result.value(forKey: "status") as! String
            
            if code == "200"
            {
                self.favoriteAry.removeObject(at: (index as NSIndexPath).row)
                let data1 = NSKeyedArchiver.archivedData(withRootObject: self.favoriteAry)
                UserDefaults.standard.set(data1, forKey: kAddToWishList)
                self.viewWillAppear(true)
            }
         
            self.favoriteTableView.reloadData()
        })
     
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "favToDetail"
        {
            let productDetailObj = segue.destination as! ProductDetailsVC
            productDetailObj.productID = (self.favoriteAry.object(at: (sender! as AnyObject).row) as AnyObject).value(forKey: "product_id") as! String
        }

    }


}
