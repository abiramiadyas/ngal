//
//  AddressViewController.swift
//  N-Gal
//
//  Created by Adyas on 01/03/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class AddressViewController: UIViewController {

    @IBOutlet var scrollView: UIScrollView!
    
    
    @IBOutlet var billingAddLbl: UILabel!
    @IBOutlet var shippingAddLbl: UILabel!
    
    var amount:Float = 0.0
    var accessCode = "AVGO01BD32AF03OGFA"
    var shippingDict:NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(shippingDict, forKey: "SHIPPING_ADDRESS")
        billingAddLbl.text = NSString(format: "%@ %@ %@ %@ %@ \n%@", shippingDict.object(forKey: "shipping_firstname") as! String, shippingDict.object(forKey: "shipping_address_1") as! String, shippingDict.object(forKey: "shipping_city") as! String, shippingDict.object(forKey: "shipping_zone") as! String,shippingDict.object(forKey: "shipping_country") as! String, shippingDict.object(forKey: "shipping_telephone") as! String) as String
        
        let shipping_FirstName = shippingDict.object(forKey: "shipping_firstname") as! String
        UserDefaults.standard.set(shipping_FirstName, forKey: "shipping_FirstName")
        
        let shipping_address_1 = shippingDict.object(forKey: "shipping_address_1") as! String
        UserDefaults.standard.set(shipping_address_1, forKey: "shipping_address_1")

        let shipping_city =  shippingDict.object(forKey: "shipping_city") as! String
        UserDefaults.standard.set(shipping_city, forKey: "shipping_city")
        
        let shipping_zone = shippingDict.object(forKey: "shipping_zone") as! String
        UserDefaults.standard.set(shipping_zone, forKey: "shipping_zone")
        
        let shipping_country = shippingDict.object(forKey: "shipping_country") as! String
        UserDefaults.standard.set(shipping_country, forKey: "shipping_country")
        
        let shipping_telephone =  shippingDict.object(forKey: "shipping_telephone") as! String
        UserDefaults.standard.set(shipping_telephone, forKey: "shipping_telephone")

        shippingAddLbl.text = NSString(format: "%@ %@ %@ %@ %@ \n%@", shippingDict.object(forKey: "shipping_firstname") as! String, shippingDict.object(forKey: "shipping_address_1") as! String, shippingDict.object(forKey: "shipping_city") as! String, shippingDict.object(forKey: "shipping_zone") as! String,shippingDict.object(forKey: "shipping_country") as! String, shippingDict.object(forKey: "shipping_telephone") as! String) as String
    }

    
    @IBAction func payAction(_ sender: Any) {
        
        let urlString = "https://secure.ccavenue.com/transaction/transaction.do"

        let myRequestString = NSString(format:"command=%@&currency=%@&amount=%@&access_code=%@&customer_identifier=%@", "getJsonDataVault","INR", "\(amount)",accessCode,"stg")
        let myRequestData = Data(bytes: myRequestString.utf8String!, count: myRequestString.length)
        let request = NSMutableURLRequest(url: URL(string: urlString)!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
        request.setValue(urlString, forHTTPHeaderField: "Referer")
        request.setValue("Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30", forHTTPHeaderField: "User-Agent")
        request.httpMethod = "POST"
        request.httpBody = myRequestData
        
        do{
        let returnData: Data? = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        do{
            let dict  = try  JSONSerialization.jsonObject(with: returnData!, options: []) as? NSDictionary
            
            let array = dict?.value(forKey: "payOptions") as! NSArray
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CCVC") as! CCViewController
            viewController.payOptions = NSMutableArray(array: array)
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        catch let error as NSError
        {
            print(error.description)
        }
        
        }
        catch
        {
            
        }
        
    }

    

}
