//
//  CCPOViewController.m
//  CCIntegrationKit
//
//  Created by test on 5/12/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import "CCWebViewController.h"
#import "CCResultViewController.h"
//#import "CCViewController.h"
#import "CCTool.h"
#import "N_Gal-Swift.h"

@interface CCWebViewController ()

@end

@implementation CCWebViewController

@synthesize rsaKeyUrl;@synthesize accessCode;@synthesize mobileNo;@synthesize merchantId;@synthesize orderId;
@synthesize amount;@synthesize currency;@synthesize redirectUrl;@synthesize cancelUrl;@synthesize billingName;
@synthesize billingAddress;@synthesize billingCountry;@synthesize billingState;@synthesize billingCity;
@synthesize billingZipCode;@synthesize billingTel;@synthesize billingEmail;@synthesize deliveryName;
@synthesize deliveryAddress;@synthesize deliveryCountry;@synthesize deliveryState;@synthesize deliveryCity;
@synthesize deliveryZipCode;@synthesize deliveryTel;@synthesize cardNumber;@synthesize cardCVV;
@synthesize expiryYear;@synthesize expiryMonth;@synthesize issuingBank;@synthesize payOptId;@synthesize cardName;
@synthesize cardType;@synthesize dataAcceptedAt;@synthesize saveCard;@synthesize customerIdentifier;
@synthesize emiPlanId;@synthesize emiTenureId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewWeb.delegate = self;
    
    [SharedManager showHUDWithViewController:self];
    //let tabledata = NSUserDefaults.standardUserDefaults().stringForKey("SHIPPING_ADDRESS")
     merchantId = @"14807";
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back:)];
    newBackButton.tintColor = [UIColor blueColor];
    self.navigationItem.leftBarButtonItem = newBackButton;

    
    //Getting RSA Key
    NSString *rsaKeyDataStr = [NSString stringWithFormat:@"access_code=%@&order_id=%@",accessCode,orderId];
    
    
    NSData *requestData = [NSData dataWithBytes: [rsaKeyDataStr UTF8String] length: [rsaKeyDataStr length]];

    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: @"https://www.n-gal.com/GetRSA.php"]];

    
    [rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [rsaRequest setHTTPMethod: @"POST"];

    [rsaRequest setHTTPBody: requestData];
    
    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    
    NSString *rsaKey = [[NSString alloc] initWithData:rsaKeyData encoding:NSASCIIStringEncoding];
    
    rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
    
    //Encrypting Card Details
    NSString *myRequestString = @"";
    
    if ([_paymentType  isEqual: @"Credit Card"]) {
        
    myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@&card_number=%@&expiry_month=%@&expiry_year=%@&cvv_number=%@&customer_identifier=stg",amount,currency,cardNumber,expiryMonth,expiryYear,cardCVV];
    }
    else if([_paymentType  isEqual: @"Debit Card"])
    {
    myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@&card_number=%@&expiry_month=%@&expiry_year=%@&cvv_number=%@&customer_identifier=stg",amount,currency,cardNumber,expiryMonth,expiryYear,cardCVV];

    }
    else if([_paymentType  isEqual: @"Cash card"])
    {
        myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@&customer_identifier=stg",amount,currency];
   
    }
    else if([_paymentType  isEqual: @"Net Banking"])

    {
        myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@&customer_identifier=",amount,currency];

    }
    else
    {
        myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@&customer_identifier=stg",amount,currency];
    }
    
    
    CCTool *ccTool = [[CCTool alloc] init];
    NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
    encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                   (CFStringRef)encVal,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
   
   
    //Preparing for a webview call
    NSString * encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&language=EN&billing_name=%@&billing_address=%@&billing_city=%@&billing_state=%@&billing_zip=%@&billing_country=%@&billing_tel=%@&billing_email=%@&delivery_name=%@&delivery_address=%@&delivery_city=%@&delivery_state=%@&delivery_zip=%@&delivery_country=%@&delivery_tel=%@&merchant_param1=additional Info.&merchant_param2=additional Info.&merchant_param3=additional Info.&merchant_param4=additional Info.&payment_option=%@&card_type=%@&card_name=%@&data_accept=%@&enc_val=%@&access_code=%@&mobile_no=%@",merchantId,orderId,redirectUrl,cancelUrl,billingName,billingAddress,billingCity,billingState,billingZipCode,billingCountry,billingTel,billingEmail,deliveryName,deliveryAddress,deliveryCity,deliveryState,deliveryZipCode,deliveryCountry,deliveryTel,payOptId,cardType,cardName,dataAcceptedAt, encVal,accessCode,mobileNo];
        //issuing_bank=%@&,, issuingBank
    if (saveCard) {
        encryptedStr = [encryptedStr stringByAppendingString:@"&saveCard=Y"];
    }
    
    NSString *urlAsString = [NSString stringWithFormat:@"https://secure.ccavenue.com/transaction/initTrans"];
    

    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: myRequestData];
    [_viewWeb loadRequest:request];
}


- (void) back:(UIBarButtonItem *)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Do you really want to cancel the Transaction"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action)
                                   {
                                       
                                       
                                   }];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
          handler:^(UIAlertAction * action) {
              
        CheckOutStepFourVC * view = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckOutStepFourVC"];
              [self.navigationController pushViewController:view animated:YES];
                                                              
                                                          }];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SharedManager dismissHUDWithViewController:self];
    
    NSString *string = webView.request.URL.absoluteString;
    
    NSLog(@"URL STRING NEW %@ ",string);
    NSString *transStatus = @"Not Known";
    
    if ([string rangeOfString:@"/ccavResponseHandler.php"].location != NSNotFound)
    {
        NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        
        NSLog(@"HTML STRING : %@", html);
        
        if (([html rangeOfString:@"Aborted"].location != NSNotFound) ||
            ([html rangeOfString:@"Cancel"].location != NSNotFound))
        {
            transStatus = @"Transaction Cancelled";
        }
        else if (([html rangeOfString:@"Success"].location != NSNotFound))
        {
            transStatus = @"Transaction Successful";
        }
        else if (([html rangeOfString:@"Fail"].location != NSNotFound))
        {
            transStatus = @"Transaction Failed";
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject: transStatus forKey:@"TRANS_STATUS"];
        
        ResultSuccessViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultSuccessViewController"];
        [self.navigationController pushViewController:view animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
  /*  UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Oops!"
                                                                   message:@"Your transaction Falied"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action)
                                   {
                                       //  CheckOutStepFourVC * view = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckOutStepFourVC"];
                                       //  [self.navigationController pushViewController:view animated:YES];
                                       
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];*/
}

@end
