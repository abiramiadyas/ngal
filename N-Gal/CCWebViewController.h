//
//  CCPOViewController.h
//  CCIntegrationKit
//
//  Created by test on 5/12/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CCWebViewController : UIViewController <UIWebViewDelegate>
    @property (strong, nonatomic) IBOutlet UIWebView *viewWeb;
    @property (strong, nonatomic) NSString *payOptId;
    @property (strong, nonatomic) NSString *cardName;
    @property (strong, nonatomic) NSString *cardType;
    @property (strong, nonatomic) NSString *dataAcceptedAt;

    @property (strong, nonatomic) NSString *accessCode;
    @property (strong, nonatomic) NSString *merchantId;
    @property (strong, nonatomic) NSString *orderId;
    @property (strong, nonatomic) NSString *amount;
    @property (strong, nonatomic) NSString *currency;
    @property (strong, nonatomic) NSString *redirectUrl;
    @property (strong, nonatomic) NSString *cancelUrl;
    @property (strong, nonatomic) NSString *rsaKeyUrl;
    @property (strong, nonatomic) NSString *mobileNo;
    @property (strong, nonatomic) NSString *billingName;
    @property (strong, nonatomic) NSString *billingAddress;
    @property (strong, nonatomic) NSString *billingCountry;
    @property (strong, nonatomic) NSString *billingState;
    @property (strong, nonatomic) NSString *billingCity;
    @property (strong, nonatomic) NSString *billingZipCode;
    @property (strong, nonatomic) NSString *billingTel;
    @property (strong, nonatomic) NSString *billingEmail;
    @property (strong, nonatomic) NSString *deliveryName;
    @property (strong, nonatomic) NSString *deliveryAddress;
    @property (strong, nonatomic) NSString *deliveryCountry;
    @property (strong, nonatomic) NSString *deliveryState;
    @property (strong, nonatomic) NSString *deliveryCity;
    @property (strong, nonatomic) NSString *deliveryZipCode;
    @property (strong, nonatomic) NSString *deliveryTel;
    @property (strong, nonatomic) NSString *cardNumber;
    @property (strong, nonatomic) NSString *expiryMonth;
    @property (strong, nonatomic) NSString *expiryYear;
    @property (strong, nonatomic) NSString *cardCVV;
    @property (strong, nonatomic) NSString *issuingBank;
    @property (strong, nonatomic) NSString *customerIdentifier;
    @property (strong, nonatomic) NSString *emiPlanId;
    @property (strong, nonatomic) NSString *emiTenureId;
    @property (strong, nonatomic) NSString *paymentType;



    @property (nonatomic, assign) BOOL *saveCard;
@end
