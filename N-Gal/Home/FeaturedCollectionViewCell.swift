//
//  FeaturedCollectionViewCell.swift
//  DigitsHome
//
//  Created by Adyas on 08/08/16.
//  Copyright © 2016 adyas. All rights reserved.
//

import UIKit

class FeaturedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var featuredImageView: UIImageView!
    @IBOutlet var featuredTitle: UILabel!
    @IBOutlet var featuredPriceLbl: UILabel!
    @IBOutlet var featSplPriceLbl: UILabel!
}
