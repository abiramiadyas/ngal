//
//  SpecialCollectionViewCell.swift
//  DigitsHome
//
//  Created by Adyas on 08/08/16.
//  Copyright © 2016 adyas. All rights reserved.
//

import UIKit

class SpecialCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var specialImageView: UIImageView!
    @IBOutlet var specialTitle: UILabel!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var specialPriceLbl: UILabel!
    @IBOutlet var discountLbl: UILabel!
}
