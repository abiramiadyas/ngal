//
//  HomeViewController.swift
//  Exlcart
//
//  Created by iPhone on 14/07/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit

class HomeViewController: ParentViewController,UITabBarControllerDelegate, KIImagePagerDelegate {

    var featuredCategoryAry:NSMutableArray = []
    var latestCategoryAry:NSMutableArray = []
    var specialCategoryAry:NSMutableArray = []
    var bannerImages:NSMutableArray = []
    @IBOutlet weak var copyRightLbl: UILabel!
    var latestSortedAry:NSMutableArray = []
    var specialSortedAry:NSMutableArray = []
    
    let pageCount:Int = 1
    
    var exclusiveArr:NSArray = []
    var exclusiveIdArr:NSArray = []
    var exclusiveNameArr:NSArray = []
    var exclusiveCountArr = NSMutableArray()
    var exclusiveCIdArr = NSMutableArray()
    var exclusiveCNameArr:NSMutableArray = []
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var exclusiveView: UIView!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var featuredAllBtn: UIButton!
    @IBOutlet var latestAllBtn: UIButton!
    @IBOutlet var specialAllBtn: UIButton!
    
    @IBOutlet var specialView: UIView!
    @IBOutlet var latestView: UIView!
    
    var height1 = CGFloat()
    var height2 = CGFloat()
    
    @IBOutlet var featuredCollectionView: UICollectionView!
    @IBOutlet var specialCollectionView: UICollectionView!
    @IBOutlet var latestCollectionView: UICollectionView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var adImagePager: KIImagePager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        latestCollectionView.layer.shadowColor = UIColor.black.cgColor
        latestCollectionView.layer.shadowOffset = CGSize(width: 0, height: 0)
        latestCollectionView.layer.shadowOpacity = 0.2
        latestCollectionView.layer.shadowRadius = 2.0
        
        specialCollectionView.layer.shadowColor = UIColor.black.cgColor
        specialCollectionView.layer.shadowOffset = CGSize(width: 0, height: 0)
        specialCollectionView.layer.shadowOpacity = 0.2
        specialCollectionView.layer.shadowRadius = 2.0
        
        bottomView.layer.shadowColor = UIColor.black.cgColor
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bottomView.layer.shadowOpacity = 0.2
        bottomView.layer.shadowRadius = 2.0
        
        self.adImagePager.frame.size.height = 0
        
        if let data = UserDefaults.standard.object(forKey: kUserDetails)
        {
            let data1 = data as! Data
            
            if let userDic = NSKeyedUnarchiver.unarchiveObject(with: data1)
            {
                let userDict = userDic as! NSDictionary
                
                if let str = userDict.value(forKeyPath: "customer.customer_id")
                {
                    userIDStr = str as! String
                }                
            }
        }
        
        if Reachability.forInternetConnection().isReachable()
        {
            SharedManager.showHUD(viewController: self)
            mainScrollView.autoresizesSubviews = true
            self.navigationItem.title = "N-Gal"
   
            if self.bannerImages.count == 0
            {
                BusinessManager.getBannerImages(languageID as String, completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if let array:NSMutableArray = result.value(forKey: "slider") as! NSMutableArray?
                    {                        
                        for i in 0 ..< array.count
                        {
                            for j in 0 ..< array.count
                            {
                                let sort = (array.object(at: j) as AnyObject).value(forKey: "sort_order") as! NSString
                                let int = Int(sort as String)
                                
                                if int! - 1 == i
                                {
                                    let url = (array.object(at: j) as AnyObject).value(forKey: "image") as! String
                                    
                                    if (url as AnyObject).lowercased.range(of: "no_image") != nil
                                    {
                                        print("no_image")
                                    }
                                    else
                                    {
                                        let trimmedUrl = url.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                                      //  self.bannerImages.insert(trimmedUrl, at: i)
                                        self.bannerImages.add(trimmedUrl)
                                        
                                    }
                                    
                                   // let trimmedUrl = url.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                                  //  self.bannerImages.insert(trimmedUrl, at: i)
                                }
                            }
                        }
                        
                        if self.bannerImages.count == 0
                        {
                            self.exclusiveView.frame.origin.y = self.adImagePager.frame.origin.y + 10
                        }

                        self.adImagePager.reloadData()
                    }
                    
                })
            }
            
            if self.exclusiveArr.count == 0
            {
                BusinessManager.getCategoryList(languageID as String, completionHandler:  { (result) -> () in
                    
                    if let array = result.value(forKey: "categories")
                    {
                        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "sort_order", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
                        let sortedResults = (array as! NSArray).sortedArray(using: [descriptor])
                        self.sortedListAry = NSMutableArray(array: sortedResults)
                        self.categoryListAry = NSMutableArray(array: sortedResults)
                        
                        let testImgArr = NSMutableArray()
                        let testIdArr = NSMutableArray()
                        let testNameArr = NSMutableArray()
                        
                        for i in 0 ..< self.sortedListAry.count
                        {
                            self.arrayForBool.insert(NSNumber(value: false as Bool), at: i)
                            self.subListAry.add((self.sortedListAry.object(at: i) as AnyObject).value(forKey: "child") as! NSMutableArray)
                            
                            let categoryId = (self.sortedListAry[i] as AnyObject).value(forKey: "category_id") as! String
                            
                            testImgArr.add((self.sortedListAry[i] as AnyObject).value(forKey: "mobile_icon")!)
                            testIdArr.add((self.sortedListAry[i] as AnyObject).value(forKey: "category_id")!)
                            testNameArr.add((self.sortedListAry[i] as AnyObject).value(forKey: "name")!)
                            
                            let tempArr = (self.subListAry[i] as AnyObject).value(forKey: "mobile_icon")! as AnyObject
                            let tempArr1 = (self.subListAry[i] as AnyObject).value(forKey: "category_id")! as AnyObject
                            let tempArr2 = (self.subListAry[i] as AnyObject).value(forKey: "name")! as AnyObject
                            
                            for j in 0 ..< tempArr.count
                            {
                                if categoryId != tempArr1.object(at: j) as! String
                                {
                                    testImgArr.add(tempArr.object(at: j))
                                    testIdArr.add(tempArr1.object(at: j))
                                    testNameArr.add(tempArr2.object(at: j))
                                }
                            }
                        }
                                                
                        self.exclusiveArr = testImgArr as NSArray
                        self.exclusiveIdArr = testIdArr as NSArray
                        self.exclusiveNameArr = testNameArr as NSArray
                        
                        for i in 0..<self.exclusiveArr.count
                        {
                            if self.exclusiveArr[i] as! String == ""
                            {
                                
                            }
                            else
                            {
                                let imgUrl = self.exclusiveArr[i]
                                if (imgUrl as AnyObject).lowercased.range(of: "no_image") != nil
                                {
                                    
                                }
                                else
                                {
                                    self.exclusiveCountArr.add(self.exclusiveArr[i])
                                    self.exclusiveCIdArr.add(self.exclusiveIdArr[i])
                                    self.exclusiveCNameArr.add(self.exclusiveNameArr[i])
                                }
                            }
                        }
                    }
                    else
                    {
                        var frame = self.exclusiveView.frame
                        frame.size.height = 0
                        self.exclusiveView.frame = frame
                    }
                })
            }
            
            if self.latestCategoryAry.count == 0
            {
                BusinessManager.getLatestCategoryList(languageID as String,completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                   // print(result)
                    
                   if let resultArr = result.value(forKey: "product")
                   {
                    
                    for i in 0 ..< (resultArr as AnyObject).count
                    {
                        let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                        
                        if ((nameSTr)?.lowercased().contains("series"))!
                        {
                            
                        }
                        else
                        {
                            self.latestCategoryAry.add((resultArr as AnyObject).object(at: i))
                        }
                    }
                    
                    if self.latestCategoryAry.count > 4
                    {
                        for i in 0 ..< 4
                        {
                            self.latestSortedAry.add(self.latestCategoryAry[i])
                        }
                    }
                    else if self.latestCategoryAry.count < 4
                    {
                        if self.latestCategoryAry.count >= 2
                        {
                            for i in 0 ..< 2
                            {
                                self.latestSortedAry.add(self.latestCategoryAry[i])
                            }
                        }
                    }
                    else if self.latestCategoryAry.count == 4
                    {
                        for i in 0 ..< self.latestCategoryAry.count
                        {
                            self.latestSortedAry.add(self.latestCategoryAry[i])
                        }
                    }
                    self.latestCollectionView.reloadData()
                   }
                   else
                   {
                    var frame = self.latestView.frame
                    frame.size.height = 0
                    self.latestView.frame = frame
                        self.latestView.isHidden = true
                       // self.latestCollectionView.frame.size.height = 0
                    }
                    
                })
            }
            if self.specialCategoryAry.count == 0
            {
                BusinessManager.getSpecailCategoryList(languageID as String,completionHandler: { (result) -> () in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if let resultArr = result.value(forKey: "product")
                    {
                        
                        for i in 0 ..< (resultArr as AnyObject).count
                        {
                            let nameSTr = ((resultArr as AnyObject).object(at: i) as AnyObject).value(forKey: "name") as? String
                            
                            if ((nameSTr)?.lowercased().contains("series"))!
                            {
                                
                            }
                            else
                            {
                                self.specialCategoryAry.add((resultArr as AnyObject).object(at: i))
                            }
                        }
                        
                        for i in 0 ..< 3
                        {
                            self.specialSortedAry.add(self.specialCategoryAry[i])
                        }
                        
                        self.specialCollectionView.reloadData()
                    }
                    else
                    {
                        self.specialView.isHidden = true

                        var frame = self.specialView.frame
                        frame.size.height = 0
                        self.specialView.frame = frame
                       // self.specailCollectionView.frame.size.height = 0
                    }
                    
                })
            }
            
        }
        
        SharedManager.showHUD(viewController: self)
        self.perform(#selector(setFrames), with: nil, afterDelay: 4.0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        self.setLeftNavigationButton()
        self.updateCartBadge()
        self.sortProducts()
    }



override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    adImagePager.pageControl.currentPageIndicatorTintColor = UIColor(red: 252/255, green: 17/255, blue: 99/255, alpha: 1.0)
    adImagePager.pageControl.pageIndicatorTintColor = UIColor.darkGray
    adImagePager.slideshowTimeInterval = 5
    adImagePager.slideshowShouldCallScrollToDelegate = true
    adImagePager.bounces = false
}
    
    func setFrames()
    {
        var y : CGFloat = 0
        
        if bannerImages.count == 0
        {
            adImagePager.isHidden = true
            self.adImagePager.frame.size.height = 0
            y = 0
        }
        else
        {
            adImagePager.isHidden = false
            y = adImagePager.frame.origin.y + adImagePager.frame.size.height + 10
            
            if !SharedManager.DeviceType.IS_IPHONE_5
            {
                self.adImagePager.frame.size.height = 175
            }
            else
            {
                self.adImagePager.frame.size.height = 209
            }
        }
        
        if exclusiveCountArr.count <= 1
        {
            exclusiveView.isHidden = true
            exclusiveView.frame.size.height = 0
        }
        else
        {
            exclusiveView.isHidden = false
            
            print(self.exclusiveArr.count)
            
            if exclusiveCountArr.count%2 != 0
            {
                exclusiveCountArr.removeLastObject()
                exclusiveCIdArr.removeLastObject()
                exclusiveCNameArr.removeLastObject()
                
                featuredCollectionView.reloadData()
            }
            else
            {
                exclusiveView.isHidden = false
                featuredCollectionView.reloadData()
            }
            
            adImagePager.frame.origin.y = 0
            
            //Exclusive View
            
            var height = Int()
            
            if SharedManager.DeviceType.IS_IPHONE_5 || SharedManager.DeviceType.IS_IPHONE_4_OR_LESS
            {
                height = ((exclusiveCountArr.count/2) * 151) + ((exclusiveCountArr.count/2) * 2)
            }
            else if SharedManager.DeviceType.IS_IPHONE_6
            {
                height = ((exclusiveCountArr.count/2) * 178) + ((exclusiveCountArr.count/2) * 2)
            }
            else if SharedManager.DeviceType.IS_IPHONE_6P
            {
                height = ((exclusiveCountArr.count/2) * 197) + ((exclusiveCountArr.count/2) * 2)
            }
            else
            {
                height = ((exclusiveCountArr.count/2) * 197) + ((exclusiveCountArr.count/2) * 2)
            }
            // #f44336
            var frame = featuredCollectionView.frame
            frame.size.height = CGFloat(height + 2)
            frame.origin.y = 30
            featuredCollectionView.frame = frame
            
            frame = exclusiveView.frame
            frame.size.height = self.featuredCollectionView.frame.size.height + 39
            frame.origin.y = y
            exclusiveView.frame = frame
        }
        
        if latestSortedAry.count <= 1
        {
            latestView.isHidden = true
            latestView.frame.size.height = 0
        }
        else
        {
            //Latest View
            
            adImagePager.frame.origin.y = 0
            
            let height1 = ((latestSortedAry.count/2) * 294) + ((latestSortedAry.count/2) * 1)
            
            var frame = latestCollectionView.frame
            frame.size.height = CGFloat(height1 + 1)
            frame.origin.y = 50
            latestCollectionView.frame = frame
            
            frame = latestView.frame
            frame.size.height = self.latestCollectionView.frame.size.height + 56
            frame.origin.y = exclusiveView.frame.origin.y + exclusiveView.frame.size.height + 5
            latestView.frame = frame
        }
        if specialSortedAry.count == 0
        {
            specialView.isHidden = true
            specialView.frame.size.height = 0
        }
        else
        {
            //Special View
            
            let height2 = ((specialSortedAry.count) * 134) + ((specialSortedAry.count) * 1)
            
            var frame = specialCollectionView.frame
            frame.size.height = CGFloat(height2 + 1)
            frame.origin.y = 44
            specialCollectionView.frame = frame
            
            frame = specialView.frame
            frame.size.height = self.specialCollectionView.frame.size.height + 40
            frame.origin.y = latestView.frame.origin.y + latestView.frame.size.height + 1
            specialView.frame = frame
        }
        
        // Others
        var frame = bottomView.frame
        frame.origin.y = specialView.frame.origin.y + specialView.frame.size.height + 1
        bottomView.frame = frame
        
        frame = copyRightLbl.frame
        frame.origin.y = bottomView.frame.origin.y + bottomView.frame.size.height + 2
        copyRightLbl.frame = frame
        
        if bannerImages.count == 0
        {
            self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width,   height: exclusiveView.frame.size.height + latestView.frame.size.height + specialView.frame.size.height + bottomView.frame.size.height + copyRightLbl.frame.size.height + 26)
        }
        else
        {
            self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width,   height: adImagePager.frame.size.height + exclusiveView.frame.size.height + latestView.frame.size.height + specialView.frame.size.height + bottomView.frame.size.height + copyRightLbl.frame.size.height + 26)
        }
        
        self.baseView.frame.size.height = self.mainScrollView.contentSize.height
        
        SharedManager.dismissHUD(viewController: self)
    }

func arrayWithImages(_ pager: KIImagePager) -> [AnyObject]
{
    return self.bannerImages as [AnyObject]
}

/*func placeHolderImageForImagePager(_ image: UIImage) -> UIImage
{
    let image  =  UIImage.init(named: "logo.png")!
    return image as UIImage
}*/
    
    
    func placeHolderImageForImagePager(_ imag: UIImage) -> UIImage
    {
        return UIImage(named: "logo.png")!
    }
 
    
func contentModeForPlaceHolder(_ pager: KIImagePager) -> UIViewContentMode
{
    return .scaleToFill
}
    
func contentModeForImage(_ image: Int, inPager pager: KIImagePager) -> UIViewContentMode
{
    return .scaleToFill
}

//func contentModeForImage(_ image: Int, inPager pager: KIImagePager) -> UIViewContentMode
//{
//    return .scaleToFill
//}



func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
{
    if collectionView == featuredCollectionView
    {
        return exclusiveCountArr.count
        // return 6
    }
    else if collectionView == latestCollectionView
    {
        return latestSortedAry.count
    }
    else
    {
        return specialSortedAry.count
    }
}


func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell
{
    if collectionView == featuredCollectionView
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featuredCell", for: indexPath) as! FeaturedCollectionViewCell
        
        var imageURL = String()
        
        if exclusiveCountArr.count != 0
        {
            if ((exclusiveCountArr[indexPath.row]) as AnyObject).contains("https://")
            {
                imageURL = "\(exclusiveCountArr[indexPath.row])"
                
                
            }
            else
            {
                imageURL = "https://www.n-gal.com/image/\(exclusiveCountArr[indexPath.row])"
            }
            
            let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = cell.featuredImageView.center
            activityLoader.startAnimating()
            cell.featuredImageView.addSubview(activityLoader)
            
            cell.featuredImageView.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                
                if image != nil
                {
                    
                    activityLoader.stopAnimating()
                }else
                {
                    print("image not found")
                    activityLoader.stopAnimating()
                }
            })
            
        }
            
        else
        {
            
        }
        
        
        return cell
    }
    else if collectionView == latestCollectionView
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "latestCell", for: indexPath) as! LatestCollectionViewCell
        
        let imageURL = (latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "image") as! String
        if (imageURL as AnyObject).lowercased.range(of: "no_image") != nil
        {
            
        }
        else
        {
            let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            SDImageCache.shared().removeImage(forKey: trimmedUrl, fromDisk: true)
            
            cell.latestImageView.sd_setImage(with: URL(string: trimmedUrl))
            cell.latestCatTitle.text = (latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            
            cell.latestSplPriceLbl.text = (latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
            
            if !cell.latestSplPriceLbl.text!.isEmpty
            {
                cell.latestSplPriceLbl.text = (latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                
                cell.latestPriceLbl.text = (latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
                
                cell.latestSplPriceLbl.isHidden = false
                let attributeString:NSMutableAttributedString =  NSMutableAttributedString(string: cell.latestSplPriceLbl.text!)
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
                cell.latestSplPriceLbl.attributedText = attributeString
            }
            else
            {
                cell.latestPriceLbl.text = (latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                
                cell.latestSplPriceLbl.isHidden = true
            }
            
            cell.latestPriceLbl.sizeToFit()
            cell.latestSplPriceLbl.frame.origin.x = cell.latestPriceLbl.frame.origin.x + cell.latestPriceLbl.frame.width + 5
            cell.latestSplPriceLbl.sizeToFit()
            
            if (self.latestCategoryAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String != ""
            {
                let discoutStr:String = ((self.latestCategoryAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String)!
                cell.discountLbl.text = discoutStr
                
                cell.discountLbl.isHidden = false
            }
            else
            {
                cell.discountLbl.isHidden = true
            }
        }
        return cell
    }
    else
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "specialCell", for: indexPath) as! SpecialCollectionViewCell
        
        let imageURL = (specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "image") as! String
        
        if (imageURL as AnyObject).lowercased.range(of: "no_image") != nil
        {
            
        }
        else
        {
            let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            cell.specialImageView.sd_setImage(with: URL(string: trimmedUrl))
            SDImageCache.shared().removeImage(forKey: trimmedUrl, fromDisk: true)
            cell.specialTitle.text = (specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            
            cell.specialPriceLbl.text = (specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
            
            if !cell.specialPriceLbl.text!.isEmpty
            {
                cell.specialPriceLbl.text = (specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                
                cell.priceLbl.text = (specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "special") as? String
                
                cell.specialPriceLbl.isHidden = false
                let attributeString:NSMutableAttributedString =  NSMutableAttributedString(string: cell.specialPriceLbl.text!)
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
                cell.specialPriceLbl.attributedText = attributeString
            }
            else
            {
                cell.priceLbl.text = (specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
                
                cell.specialPriceLbl.isHidden = true
            }
            
            cell.priceLbl.sizeToFit()
            cell.specialPriceLbl.frame.origin.x = cell.priceLbl.frame.origin.x + cell.priceLbl.frame.width + 5
            cell.specialPriceLbl.sizeToFit()
            
            if (self.specialCategoryAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String != ""
            {
                let discoutStr:String = ((self.specialCategoryAry.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "special_discount") as? String)!
                cell.discountLbl.text = discoutStr
                cell.discountLbl.isHidden = false
            }
            else
            {
                cell.discountLbl.isHidden = true
            }
        }
        return cell
    }
}

func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
    
    if collectionView == featuredCollectionView
    {
       /* let VC = self.storyboard!.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
        categoryID = exclusiveCIdArr[indexPath.row] as! String
        VC.catTitle = exclusiveCNameArr[indexPath.row] as! String
        self.navigationController?.pushViewController(VC, animated: true)*/
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        var ary = NSMutableArray()
      //  subCategoryID = ((subListAry.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "category_id") as! NSArray).object(at: (indexPath as NSIndexPath).row) as! String as NSString
        
        subCategoryID = self.exclusiveCIdArr[indexPath.row] as! NSString
        
        BusinessManager.getSubCategoryList(languageID as String, subCategoryID: subCategoryID as String,completionHandler:  { (result) -> () in
            print("result: \(result)")
            
            ary = ((result.value(forKey: "categories") as! NSArray).object(at: 0) as AnyObject).value(forKey: "child") as! NSMutableArray
            let imageUrl = ((result.value(forKey: "categories") as! NSArray).object(at: 0) as AnyObject).value(forKey: "image") as! String
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            
            if ary.count > 0
            {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "SubCategory") as! SubCategory
                viewController.subCategoryAry2 = ary
                
                viewController.imageUrl = URL(string: trimmedUrl)!
                viewController.imageUrlStr = trimmedUrl
                
                categoryID = (((result.value(forKey: "categories") as! NSArray).object(at: 0) as AnyObject).value(forKey: "category_id") as! String as NSString) as String
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else
            {
                let VC = self.storyboard!.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
                categoryID = (self.exclusiveCIdArr[indexPath.row] as! NSString) as String
                VC.catTitle = (self.exclusiveCNameArr[indexPath.row] as! NSString) as String
                self.navigationController?.pushViewController(VC, animated: true)
            }
        })
        
    }
    else if collectionView == latestCollectionView
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        viewController.productID = (self.latestCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "product_id") as! String
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    else
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        viewController.productID = (self.specialCategoryAry.object(at: indexPath.row) as AnyObject).value(forKey: "product_id") as! String
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

@IBAction func featuredProductList(_ sender: AnyObject) {
    
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
    viewController.productListAry = self.featuredCategoryAry
    viewController.catTitle = "Featured"
    viewController.isFromHome = true
    self.navigationController?.pushViewController(viewController, animated: true)
    
}

@IBAction func viewProductList(_ sender: AnyObject) {
    
    
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
    viewController.productListAry = self.latestCategoryAry
    viewController.catTitle = "New Arrivals"
    viewController.isFromHome = true
    self.navigationController?.pushViewController(viewController, animated: true)
    
}

@IBAction func specialProductList(_ sender: AnyObject)
{
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "productlist") as! ProductListVC
     viewController.productListAry = self.specialCategoryAry
     viewController.catTitle = "Special"
    viewController.isFromHome = true
    self.navigationController?.pushViewController(viewController, animated: true)
    
    
}
    func sortProducts()
    {
        // Exclusive Cell
        let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout.minimumInteritemSpacing = 1.0
        flowlayout.minimumLineSpacing = 1.0
        flowlayout.sectionInset.top = 1.0;
        flowlayout.sectionInset.left = 1.0;
        flowlayout.sectionInset.right = 1.0;
        flowlayout.itemSize.width = (self.featuredCollectionView.frame.width - 4) / 2
        
        if SharedManager.DeviceType.IS_IPHONE_5 || SharedManager.DeviceType.IS_IPHONE_4_OR_LESS
        {
            flowlayout.itemSize.height = 151
        }
        else if SharedManager.DeviceType.IS_IPHONE_6
        {
            flowlayout.itemSize.height = 178
        }
        else if SharedManager.DeviceType.IS_IPHONE_6P
        {
           flowlayout.itemSize.height = 197
        }
        else
        {
            flowlayout.itemSize.height = 197
        }
        
         //(self.featuredCollectionView.frame.width - 4) / 2; //146
        self.featuredCollectionView .reloadData()
        self.featuredCollectionView.collectionViewLayout = flowlayout
        
        // Latest Cell
        let flowlayout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout1.minimumInteritemSpacing = 1.0
        flowlayout1.minimumLineSpacing = 1.0
        flowlayout1.sectionInset.top = 0
        flowlayout1.sectionInset.left = 1.0;
        flowlayout1.sectionInset.right = 1.0;
        flowlayout1.itemSize.width = (self.latestCollectionView.frame.width - 3) / 2
        flowlayout1.itemSize.height = 294;
        self.latestCollectionView .reloadData()
        self.latestCollectionView.collectionViewLayout = flowlayout1
        
        // Special Cell
        let flowlayout2:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout2.minimumInteritemSpacing = 1.0
        flowlayout2.minimumLineSpacing = 1.0
        flowlayout2.sectionInset.top = 1.0;
        flowlayout2.sectionInset.left = 2.0;
        flowlayout2.sectionInset.right = 2.0;
        flowlayout2.itemSize.width = (self.specialCollectionView.frame.width - 2)
        flowlayout2.itemSize.height = 134;
        self.specialCollectionView .reloadData()
        self.specialCollectionView.collectionViewLayout = flowlayout2
    }

}
