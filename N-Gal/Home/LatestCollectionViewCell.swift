//
//  LatestCollectionViewCell.swift
//  DigitsHome
//
//  Created by Adyas on 08/08/16.
//  Copyright © 2016 adyas. All rights reserved.
//

import UIKit

class LatestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var latestImageView: UIImageView!
    @IBOutlet var latestCatTitle: UILabel!
    @IBOutlet var latestPriceLbl: UILabel!
    @IBOutlet var latestSplPriceLbl: UILabel!
    @IBOutlet var discountLbl: UILabel!
}
